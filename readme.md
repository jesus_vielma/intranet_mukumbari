# Intranet Mukumbarí  

## Frontend 

El frontend de la aplicación esta diseñado para mostrar artículos, noticias e informaciones importante de la empresa, de la misma manera tiene un gestor de tickets que permite a los usuarios realizar solicitudes de forma expedita e inmediata al resto de los departamentos y coordinaciones de la empresa. 

![Detalle_de_tickets.png](https://bitbucket.org/repo/goMj5L/images/1945354462-Detalle_de_tickets.png)

## Backend
El backend esta diseñado para realizar las funciones del administrador del sitio, donde se puede crear usuarios, gestionar configuraciones entre otroa funciones del mismo

## Desarrollo
Jesús Vielma 2017 

## Herramientas usadas 
* Codeigniter 3.1