<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = $route['inicio'] = $route['inicio/(:num)'] = 'frontend';
$route['404_override'] = 'frontend/error_404';
$route['translate_uri_dashes'] = FALSE;

/*
Backend predefinidas
*/

$route['admin'] = 'backend/login';
$route['admin/(:any)/(:any)/(:any)/(:any)'] = '$1/$2/$3/$4';
$route['admin/(:any)/(:any)/(:any)'] = '$1/$2/$3';
$route['admin/(:any)/(:any)'] = '$1/$2';
$route['admin/(:any)'] = '$1/index';

/*
Frontend predefinidas
*/

$route['salir'] = 'frontend/salir';

$route['mis_tickets/(:num)'] = 'ticket';
$route['mis_tickets'] = 'ticket';
$route['mis_tickets/([a-z_]+)'] = 'ticket/generados/';
$route['nuevo_ticket'] = 'ticket/nuevo';

$route['noticias/(:num)'] = 'articulo/categoria/noticias';
$route['noticias'] = 'articulo/categoria/noticias';
$route['noticias/([a-z_]+)'] = 'articulo/completo/$1';
//$route['empleado_del_mes'] = 'articulo/categoria/empleado_del_mes';
//$route['empleado_del_mes/([a-z_]+)'] = 'articulo/completo/$1';

$route['nuevo_articulo'] = 'articulo/nuevo';
$route['articulo/buscar_departamento'] = 'articulo/buscar_departamento';
$route['articulo/(:any)'] = 'articulo/detalle/$1';

$route['eventos'] = 'evento';
$route['nuevo_evento'] = 'evento/nuevo';
$route['evento/(:num)'] = 'evento/detalle/$1';

$route['estructura_organizacional'] = 'articulo/detalle/estructura_organizacional';

/*Definidas por el usuario*/


$route['empleado_del_mes'] = 'articulo/categoria/empleado_del_mes';
$route['empleado_del_mes/([a-z_0-9]+)'] = 'articulo/detalle/$1';
$route['google'] = 'articulo/detalle/';
