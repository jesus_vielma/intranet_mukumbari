<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departamento_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function insertar($_data){

        if($this->db->insert('departamento',$_data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function obtener_todos(){
        $query = $this->db->get('departamento');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    

    function obtener_por_id($_id){
        $query = $this->db->where('id_departamento',$_id)
                          ->get('departamento');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function buscar_departamento_por_nombre($nombre){
        $query = $this->db->where('nombre',$nombre)
                          ->get('departamento');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function actualizar($_data)
    {
        
        if($this->db->where('id_departamento',$_data['id_departamento'])->update('departamento',$_data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function obtener_usuarios_activos($dep)
    {
        $query = $this->db->select('usuario.nombre as persona, usuario.id_usuario as id, usuario_departamento.estado as estado')
                          ->join('usuario_departamento','usuario.id_usuario = usuario_departamento.id_usuario','INNER')
                          ->where('usuario_departamento.id_departamento',$dep)
                          ->get('usuario');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_usuarios(){
        $query = $this->db->select('id_usuario, nombre')
                          ->get('usuario');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function vincular($_data)
    {
        if($this->db->insert_batch('usuario_departamento',$_data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function obtener_todos_activos()
    {
        $query = $this->db->select('id_usuario')
                          ->where('usuario_departamento.estado','1')
                          ->get('usuario_departamento');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_info_usuario($id){
        $query = $this->db->select('nombre')
                          ->where('id_usuario',$id)
                          ->get('usuario');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function desvincular($u,$d,$e)
    {
        if($this->db->set('estado',$e)->where('id_usuario',$u)->where('id_departamento',$d)->update('usuario_departamento'))
            return true;
        else
            return false;
    }
}
