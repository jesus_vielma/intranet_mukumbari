<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articulo_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function insertar($_data){

        if($this->db->insert('articulo',$_data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function obtener_todos(){
        $query = $this->db->get('articulo');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_por_alias($_alias){
        $query = $this->db->where('alias',$_alias)
                          ->get('articulo');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_por_alias_completo($_alias){
        $query = $this->db->select('articulo.id_articulo, articulo.titulo, articulo.cuerpo, articulo.fecha, articulo.alias, categoria.nombre as categoria, categoria.alias as catalias, usuario.nombre as autor')
                          ->join('categoria','categoria.id_categoria = articulo.id_categoria','INNER')
                          ->join('usuario','articulo.id_usuario = usuario.id_usuario','INNER')
                          ->where('articulo.alias',$_alias)
                          ->get('articulo');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function buscar_alias_igual($alias){
        $query = $this->db->where('alias',$alias)->get('articulo');
        if($query->num_rows()>0){
            return true;
        }
        else{
            return false;
        }
    }

    function actualizar($_data,$id)
    {
        
        if($this->db->where('id_articulo',$id)->update('articulo',$_data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function cambiar_default($_data,$a)
    {
        
        if($this->db->set('default',$a)->where('id_categoria',$_data)->update('categoria')){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function contar_registros_categoria($cat)
    {
        $query = $this->db->select('count(id_articulo) as registros')
                        ->where('id_categoria',$cat)
                        ->get('articulo');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return 0;
    }

     function obtener_articulos_cat($limite,$inicio,$cat)
    {
        $query =$this->db->select('articulo.id_articulo, articulo.titulo, articulo.cuerpo, articulo.fecha, articulo.alias, usuario.nombre as autor')
                         ->join('categoria','categoria.id_categoria = articulo.id_categoria','INNER')
                          ->join('usuario','articulo.id_usuario = usuario.id_usuario','INNER')
                          ->order_by('articulo.fecha','DESC')
                         ->where('articulo.id_categoria',$cat)
                         ->limit($limite, $inicio)
                         ->get("articulo");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return 0;
    }

    function obtener_articulos_por_autor_backend($_id){
        $query =$this->db->select('articulo.id_articulo, articulo.titulo, articulo.cuerpo, articulo.fecha, articulo.alias, categoria.nombre as cat')
                         ->join('categoria','categoria.id_categoria = articulo.id_categoria','INNER')
                         ->order_by('articulo.fecha','DESC')
                         ->where('articulo.id_usuario',$_id)
                         ->limit(5)
                         ->get("articulo");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return 0;

    }
}
