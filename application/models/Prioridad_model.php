<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prioridad_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function insertar($_data){

        if($this->db->insert('ticket_prioridad',$_data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function obtener_todos(){
        $query = $this->db->get('ticket_prioridad');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    

    function obtener_por_id($_id){
        $query = $this->db->where('id_prioridad',$_id)
                          ->get('ticket_prioridad');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_activas(){
        $query = $this->db->where('estado','1')
                          ->get('ticket_prioridad');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function buscar_default(){
        $query = $this->db->select('id_prioridad')->where('predeterminado','1')
                          ->get('ticket_prioridad');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function actualizar($_data)
    {
        
        if($this->db->where('id_prioridad',$_data['id_prioridad'])->update('ticket_prioridad',$_data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function cambiar_default($_data,$a)
    {
        
        if($this->db->set('predeterminado',$a)->where('id_prioridad',$_data)->update('ticket_prioridad')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}
