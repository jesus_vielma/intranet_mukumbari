<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function iniciar($_data){
        $query_user = $this->db->select('usuario')
                          ->where('usuario',$_data->post('usuario'))
                          ->get('usuario');


        if($query_user->num_rows() > 0){
            $query_pass = $this->db->where('usuario',$_data->post('usuario'))
                          ->where('clave',$_data->post('clave'))
                          ->get('usuario');
            if($query_pass->num_rows() > 0){
                foreach($query_pass->result() as $row){
                    $data[] = $row;
                }
            return $data;
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }

    function obtener_id_prof_tec($_id){
        $query = $this->db->select('id_persona')
                          ->where('id_usuario',$_id)
                          ->get('usuario');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function insertar($_data){

        if($this->db->insert('menu',$_data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function cerrar_sesion($_data){
        $data = array(
            'fecha_hora_cierre'=> date('Y-m-d H:i:s')
        );

        if($this->db->where('id_sesion',$_data)->update('sesion',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function actualizar_orden($_data)
    {
        if($this->db->update_batch('menu',$_data,'id_menu'))
        {
            return true;
        }
        else{
            return false;
        }
    }

    function obtener_padres()
    {
        $query = $this->db->where('padre','0')->where('logueado','0')->where('estado','1')->order_by('posicion','ASC')->get('menu');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    function obtener_hijos($padre)
    {
        $query = $this->db->where('padre',$padre)->where('logueado','0')->order_by('posicion','ASC')->where('estado','1')->get('menu');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_padres_l()
    {
        $query = $this->db->select('nombre,id_menu,posicion')->where('padre','0')->order_by('posicion','ASC')->get('menu');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    function obtener_hijos_l($padre)
    {
        $query = $this->db->select('nombre,id_menu,posicion')->order_by('posicion','ASC')->where('padre',$padre)->get('menu');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_ultimo_padre()
    {
        $query = $this->db->select('posicion')->where('padre','0')->order_by('posicion','DESC')->limit(1)->get('menu');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function buscar_alias_igual($alias){
        $query = $this->db->where('alias',$alias)->get('menu');
        if($query->num_rows()>0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
}
