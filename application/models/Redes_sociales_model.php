<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Redes_sociales_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function insertar($_data){

        if($this->db->insert('redes_sociales',$_data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function obtener_todos(){
        $query = $this->db->get('redes_sociales');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    

    function obtener_por_id($_id){
        $query = $this->db->where('id_red',$_id)
                          ->get('redes_sociales');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function buscar_default(){
        $query = $this->db->select('id_categoria')->where('default','1')
                          ->get('categoria');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function actualizar($_data)
    {
        
        if($this->db->where('id_red',$_data['id_red'])->update('redes_sociales',$_data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function cambiar_default($_data,$a)
    {
        
        if($this->db->set('default',$a)->where('id_categoria',$_data)->update('categoria')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}
