<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function insertar($_data){

        if($this->db->insert('categoria',$_data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function obtener_todos(){
        $query = $this->db->get('categoria');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    

    function obtener_por_id($_id){
        $query = $this->db->where('id_categoria',$_id)
                          ->get('categoria');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_id_cat($cat){
        $query = $this->db->select('id_categoria, nombre')->where('alias',$cat)
                          ->get('categoria');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }



    function buscar_default(){
        $query = $this->db->select('id_categoria')->where('default','1')
                          ->get('categoria');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function actualizar($_data)
    {
        
        if($this->db->where('id_categoria',$_data['id_categoria'])->update('categoria',$_data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function cambiar_default($_data,$a)
    {
        
        if($this->db->set('default',$a)->where('id_categoria',$_data)->update('categoria')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}
