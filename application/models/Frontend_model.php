<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function iniciar($_data){
        $query_user = $this->db->select('usuario')
                          ->where('usuario',$_data->post('usuario'))
                          ->get('usuario');


        if($query_user->num_rows() > 0){
            $query_pass = $this->db->where('usuario',$_data->post('usuario'))
                          ->where('clave',$_data->post('clave'))
                          ->get('usuario');
            if($query_pass->num_rows() > 0){
                foreach($query_pass->result() as $row){
                    $data[] = $row;
                }
            return $data;
            }
            else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }

    function obtener_id_prof_tec($_id){
        $query = $this->db->select('id_persona')
                          ->where('id_usuario',$_id)
                          ->get('usuario');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function insertar_sesion($_data){
        $data = array(
            'id_usuario' => $_data['id_usuario'],
            'fecha_hora_inicio'=> $_data['datetime'],
            'fecha_hora_cierre'=> $_data['datetime'],
            'direccion_ip' => $this->input->ip_address(),
            'id_sesion' => $_data['sess_id'],
        );

        if($this->db->insert('sesion',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function cerrar_sesion($_data){
        $data = array(
            'fecha_hora_cierre'=> date('Y-m-d H:i:s')
        );

        if($this->db->where('id_sesion',$_data)->update('sesion',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function obtener_ultima_sesion($id_usr){
        $select = "fecha_hora_inicio, to_char(fecha_hora_inicio, 'DD/MM/YYYY HH12:MI:SS pm') as fecha2, fecha_hora_cierre, direccion_ip, id_sesion";
        $query = $this->db->select($select)
                          ->where('id_usuario',$id_usr)
                          ->order_by('fecha_hora_inicio','DESC')
                          ->limit(1)
                          ->get('sesion');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_por_id($_id){
        $query = $this->db->select('*')
                          ->where('id_sesion',$_id)
                          ->limit(1)
                          ->get('sesion');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_dbrole($user){
        $query = $this->db->select('count(*)')
                          ->where('rolname',$user)
                          ->get('pg_catalog.pg_roles');

        $res = $query->result();

        return $res[0]->count;
    }

    function verif_pass($_data){
        $query = $this->db->where('id_usuario',$_data->post('id_usuario'))
                          ->where('clave',$_data->post('password'))
                          ->get('usuario');

        if($query->num_rows() > 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    function obtener_padres()
    {
        $query = $this->db->where('padre','0')->where('logueado','0')->where('estado','1')->order_by('posicion','ASC')->get('menu');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    function obtener_hijos($padre)
    {
        $query = $this->db->where('padre',$padre)->where('logueado','0')->where('estado','1')->order_by('posicion','ASC')->get('menu');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_padres_l()
    {
        $query = $this->db->where('padre','0')->where('estado','1')->order_by('posicion','ASC')->get('menu');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    function obtener_hijos_l($padre)
    {
        $query = $this->db->where('padre',$padre)->where('estado','1')->order_by('posicion','ASC')->get('menu');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_todos_con_autor($limite,$inicio){
        $query = $this->db->select('articulo.id_articulo, articulo.titulo, articulo.cuerpo, articulo.fecha, articulo.alias, categoria.nombre as categoria, categoria.alias as catalias, usuario.nombre as autor')
                          ->join('categoria','categoria.id_categoria = articulo.id_categoria','INNER')
                          ->join('usuario','articulo.id_usuario = usuario.id_usuario','INNER')
                          ->order_by('articulo.fecha','DESC')
                          ->limit($limite, $inicio)
                          ->get('articulo');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function contar_articulos(){
        return $this->db->count_all('articulo');
    }
}
