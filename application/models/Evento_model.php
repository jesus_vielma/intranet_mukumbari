<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evento_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function insertar($_data){

        if($this->db->insert('evento',$_data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function obtener_todos(){
        $query = $this->db->get('evento');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_por_id($_id){
        $query = $this->db->where('id_evento',$_id)->get('evento');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_por_alias($_alias){
        $query = $this->db->where('alias',$_alias)
                          ->get('evento');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_por_alias_completo($_alias){
        $query = $this->db->select('evento.id_evento, evento.titulo, evento.cuerpo, evento.fecha, evento.alias, categoria.nombre as categoria, usuario.nombre as autor')
                          ->join('categoria','categoria.id_categoria = evento.id_categoria','INNER')
                          ->join('usuario','evento.id_usuario = usuario.id_usuario','INNER')
                          ->where('evento.alias',$_alias)
                          ->get('evento');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function buscar_alias_igual($alias){
        if($this->db->where('alias',$alias)->get('evento')){
            return true;
        }
        else{
            return false;
        }
    }

    function actualizar($_data,$id)
    {
        
        if($this->db->where('id_evento',$id)->update('evento',$_data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function cambiar_default($_data,$a)
    {
        
        if($this->db->set('default',$a)->where('id_categoria',$_data)->update('categoria')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}
