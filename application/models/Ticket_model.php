<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ticket_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function insertar($ticket,$dptos){

        $this->db->trans_begin();

        if($this->db->insert('ticket',$ticket))
        {
            if($this->db->insert_batch('ticket_departamento',$dptos)){
                $this->db->trans_commit();
                return true;
            }
            else{
                $this->db->trans_rollback();
                return false;
            }
        }
        else
        {
            $this->db->trans_rollback();
            return false;
        }
    }

    function respuesta($data)
    {
         if($this->db->insert('ticket_respuesta',$data)){
                return true;
            }
            else{
                return false;
            }
    }

    function insertar_h($data)
    {
         if($this->db->insert('ticket_historial',$data)){
                return true;
            }
            else{
                return false;
            }
    }


    function obtener_todos(){
        $query = $this->db->get('ticket');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function cerrar_ticket($id,$estado)
    {
        if($this->db->set('estado',$estado)->where('id_ticket',$id)->update('ticket')){
            return true;
        }else{
            return false;
        }
    }

    function obtener_por_id($_id){
        $query =$this->db->select('ticket.asunto, ticket.estado, ticket.fecha_creacion,ticket.cuerpo, ticket.id_ticket, ticket_prioridad.nombre as nombre_prioridad, ticket_prioridad.color, usuario.id_usuario as generador')
                         ->join('ticket_prioridad','ticket_prioridad.id_prioridad = ticket.prioridad','INNER')
                         ->join('usuario','ticket.id_usuario = usuario.id_usuario','INNER')
                         ->join('usuario_departamento','usuario.id_usuario = usuario_departamento.id_usuario','INNER')
                         ->where('id_ticket',$_id)
                         ->get("ticket");


        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

	function obtener_generador($_id){
        $query = $this->db->select('usuario.nombre as generador, usuario.telefono, usuario.correo, departamento.nombre as dep, usuario.id_usuario as id_usuario')
                          ->join('usuario_departamento','usuario.id_usuario = usuario_departamento.id_usuario','INNER')
                          ->join('departamento','departamento.id_departamento = usuario_departamento.id_departamento','INNER')
                          ->where('usuario.id_usuario',$_id)
                          ->where('usuario_departamento.estado','1')
                          ->get('usuario');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_respuestas($id)
    {
        $query = $this->db->select('ticket_respuesta.mensaje as mensaje, ticket_respuesta.fecha as fecha, usuario.nombre as usuario, departamento.nombre as dep')
                          ->join('usuario','ticket_respuesta.id_usuario = usuario.id_usuario','INNER')
                          ->join('usuario_departamento','usuario.id_usuario = usuario_departamento.id_usuario','INNER')
                          ->join('departamento','departamento.id_departamento = usuario_departamento.id_departamento','INNER')
                          ->where('ticket_respuesta.id_ticket',$id)
                          ->where('usuario_departamento.estado','1')
                          ->order_by('ticket_respuesta.fecha','DESC')
                          ->get('ticket_respuesta');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function actualizar_persona($_data){

        $data = array(
            'cedula'           => $_data->post('cedula'),
            'nombre_completo'  => $_data->post('nombre'),
            'id_departamento'       => $_data->post('departamento_id'),
			'correo'	       => $_data->post('email'),
			'telefono'         => $_data->post('movil'),
            'tipo'             => $_data->post('tipo')
        );

        if($this->db->where('id_persona',$_data->post('id_persona'))->update('persona',$data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function borrar_persona($id){

        if($this->db->where('id_persona',$id)->delete('persona')){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function contar_registros($dep)
    {
        $query = $this->db->select('count(ticket_departamento.id_ticket) as registros')
                        ->where('ticket_departamento.id_departamento',$dep)
                        ->get('ticket_departamento');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return 0;
    }

    function obtener_tickets($limite,$inicio,$dep)
    {
        $query =$this->db->select('ticket.asunto, ticket.estado, ticket.fecha_creacion, ticket.id_ticket, ticket_prioridad.nombre as nombre_prioridad, ticket_prioridad.color, usuario.nombre as generador, departamento.nombre as dpto')
                         ->join('ticket_prioridad','ticket_prioridad.id_prioridad = ticket.prioridad','INNER')
                         ->join('usuario','ticket.id_usuario = usuario.id_usuario','INNER')
                         ->join('usuario_departamento','usuario.id_usuario = usuario_departamento.id_usuario','INNER')
                         ->join('departamento','usuario_departamento.id_departamento = departamento.id_departamento','INNER')
                         ->join('ticket_departamento','ticket_departamento.id_ticket = ticket.id_ticket','INNER')
                         ->where('ticket_departamento.id_departamento',$dep)
                         //->where('ticket.estado !=','Cerrado')
                         ->order_by('fecha_creacion','DESC')
                         ->limit($limite, $inicio)
                         ->get("ticket");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return 0;
    }

    function obtener_dpto_involucrados($_id)
    {
        $query =$this->db->select('departamento.nombre as dpto, ticket_departamento.estado')
                         ->join('ticket_departamento','ticket_departamento.id_departamento = departamento.id_departamento','INNER')
                         ->where('ticket_departamento.id_ticket',$_id)
                         ->get("departamento");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return 0;
    }

    function obtener_ultima_vez($_id)
    {
        $query =$this->db->select('fecha,accion')
                         ->where('ticket_historial.id_ticket',$_id)
                         ->limit(1)
                         ->order_by('fecha','DESC')
                         ->get("ticket_historial");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return 0;
    }
    function obtener_dep_act($_id)
    {

        $query =$this->db->select('id_departamento')
                         ->where('id_usuario',$_id)
                         ->where('estado',1)
                         ->get("usuario_departamento");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return 0;
    }

}
