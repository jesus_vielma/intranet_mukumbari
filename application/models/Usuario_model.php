<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function insertar($_data){
        $data = [
            'usuario' =>$_data->post('usuario'),
            'nombre'  =>$_data->post('nombre'),
            'foto'    =>'perfil.jpg',
            'telefono'=>$_data->post('telefono'),
            'correo'  =>$_data->post('correo'),
            'clave'   =>$_data->post('clave'),
            'permisos'=>$_data->post('permiso'),
            'config'  =>'0'
        ];

        if($this->db->insert('usuario',$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function obtener_todos(){
        $query = $this->db->get('usuario');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }
    

    function obtener_por_id($_id){
        $query = $this->db->where('id_usuario',$_id)
                          ->get('usuario');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function obtener_por_id_con_dpto($_id){
        $query = $this->db->select('usuario.*, departamento.nombre as dpto')
                          ->join('usuario_departamento','usuario.id_usuario = usuario_departamento.id_usuario','INNER')
                          ->join('departamento','usuario_departamento.id_departamento = departamento.id_departamento','INNER')
                          ->where('usuario.id_usuario',$_id)
                          ->where('usuario_departamento.estado','1')
                          ->get('usuario');

        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
            return $data;
        }
        else{
            return 0;
        }
    }

    function check_clave($clave,$id_usuario){
        $query = $this->db->where('id_usuario',$id_usuario)
                          ->where('clave',$clave)
                          ->get('usuario');

        if($query->num_rows() > 0){
            
            return true;
        }
        else{
            return false;
        }
    }


    function actualizar($_data,$_id)
    {
        
        if($this->db->where('id_usuario',$_id)->update('usuario',$_data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function actualizar_por_frontend($_data,$_id)
    {
        
        if($this->db->where('id_usuario',$_id)->update('usuario',$_data)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}
