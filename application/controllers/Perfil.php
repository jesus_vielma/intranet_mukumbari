<?php
include_once 'Frontend.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends Frontend {

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		$this->load->model('Usuario_model');
		$this->load->helper(array('text','typography'));
	}

	public function index()
	{
		$this->logueado();
		$data['titulo'] = 'Usuario';
		$data['controlador'] = 'perfil';
		$data['fila'] = $this->Usuario_model->obtener_por_id($this->session->userdata('front')['id_usuario']);
		$data['menu'] = $this->menu;
		$data['redes'] = $this->redes;
		$this->load->view('frontend/base/cabecera',$data);
		$this->load->view('frontend/perfil/index');
		$this->load->view('frontend/base/pie');
	}

	public function actualizar()
	{
			$this->logueado();
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('usuario','Nivel','trim|required');
			$this->form_validation->set_rules('correo','Correo','trim|required');
			$this->form_validation->set_rules('telefono','Teléfono','trim|required');
			$this->form_validation->set_rules('clave_actual','Clave actual','trim|required|MD5|callback_check_clave');

			$clave_nueva = $this->input->post('clave_nueva');
			$clave_actual = $this->input->post('clave_actual');
			if($clave_nueva!=''){
				$this->form_validation->set_rules('clave_nueva','Nueva clave','trim|required|MD5|callback_check_pass_equal['.MD5($clave_actual).']');
			}
			//$this->form_validation->set_rules('organigrama','Organigrama','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->index();
			}
			else {
					if($this->input->post('foto_nueva')!= '' && $this->input->post('foto_nueva') != $this->input->post('foto_actual') ){
						$foto = $this->input->post('foto_nueva');
						$fotoConf = 1;
					}
					elseif($this->input->post('foto_actual') != 'perfil.jpg'){
						$foto = $this->input->post('foto_actual');
						$fotoConf = 1;
					}
					else{
						$foto = $this->input->post('foto_actual');
						$fotoConf = 0;
					}
					if($clave_nueva != '' && $clave_actual != $clave_nueva ){
						$clave = $this->input->post('clave_nueva');
						$claveConf = 2;
					}
					elseif($this->input->post('conf_actual') == 1 ){
						$clave = $this->input->post('clave_actual');
						$claveConf = 2;
					}
					else{
						$clave = $this->input->post('clave_actual');
						$claveConf = 2;
					}

					$data = [
						'nombre' => $this->input->post('nombre'),
						'correo' => $this->input->post('correo'),
						'telefono' => $this->input->post('telefono'),
						'clave' => $clave,
						'config' => $fotoConf+$claveConf,
						'foto' => $foto,
					];
					/*
					echo "<pre>";
					print_r($data);
					echo "</pre>";
					echo $fotoConf+$claveConf;
					exit(1);*/

					if($this->Usuario_model->actualizar_por_frontend($data,$this->input->post('id_usuario'))){
						redirect('salir','refresh');
					}else {
					  redirect('perfil','refresh');
					}
				}
	}

	public function perfil($id)
	{
		$this->logueado();
		$data['titulo'] = 'Usuario';
		$data['controlador'] = 'detalle';
		$data['fila'] = $this->Usuario_model->obtener_por_id($id);
		$this->load->view('frontend/base/cabecera',$data);
		$this->load->view('frontend/usuario/perfil');
		$this->load->view('frontend/base/pie');
	}

	public function generarClave()
	{
		$this->load->helper('string');

		$msg = array('clave'=> random_string('alnum',8));
		$this->output->set_content_type('application/json')
                         ->set_output(json_encode($msg));
	}

	public function check_pass_equal($cv,$ca)
	{
		if($cv == $ca ){

			$this->form_validation->set_message('check_pass_equal', 'La nueva clave no puede ser igual su clave actual');
            return FALSE;
		}
		else{
			return TRUE;
		}
	}

	public function check_clave($str)
	{
		
		if(!$this->Usuario_model->check_clave($str,$this->session->userdata('front')['id_usuario'])){
			$this->form_validation->set_message('check_clave', 'La {field} no coincide coincide con la clave registrada');
            return FALSE;
		}
		else{
			return TRUE;
		}
	}

}
