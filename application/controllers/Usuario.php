<?php
include_once 'Backend.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends Backend {

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		$this->load->model('Usuario_model');
		$this->load->helper(array('text','typography'));
	}

	public function index()
	{
		$this->check_seg();
		$data['titulo'] = 'Usuario';
		$data['controlador'] = 'listar';
		$data['filas'] = $this->Usuario_model->obtener_todos();
		$this->load->view('backend/base/cabecera',$data);
		$this->load->view('backend/usuario/index');
		$this->load->view('backend/base/pie');
	}

	public function nuevo($fase=1)
	{
		$this->check_seg();
		$data['titulo'] = 'Usuario';
		$data['controlador'] = 'insertar';

		if($fase ==1){

			$this->load->view('backend/base/cabecera',$data);
			$this->load->view('backend/usuario/nuevo');
			$this->load->view('backend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('usuario','Nivel','trim|required');
			$this->form_validation->set_rules('clave','Clave','trim|required|MD5');
			$this->form_validation->set_rules('permiso','permiso','trim|required');
			$this->form_validation->set_rules('correo','Correo','trim|required');
			$this->form_validation->set_rules('telefono','Teléfono','trim|required');
			//$this->form_validation->set_rules('organigrama','Organigrama','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{

				if($this->Usuario_model->insertar($this->input)){
					redirect('admin/usuario','refresh');
				}else {
					redirect('admin/usuario/nuevo','refresh');
				}
			}
		}
	}

	public function editar($id)
	{
		$this->check_seg();
			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('usuario','Nivel','trim|required');
			$this->form_validation->set_rules('permiso','permiso','trim|required');
			$this->form_validation->set_rules('correo','Correo','trim|required');
			$this->form_validation->set_rules('telefono','Teléfono','trim|required');

			$clave = $this->input->post('clave');
			if($clave!='')
			$this->form_validation->set_rules('clave','Clave','trim|required|MD5');


			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->perfil($id);
			}
			else {
				if($clave!=''){
					$data = [
						'nombre' => $this->input->post('nombre'),
						'permisos' => $this->input->post('permiso'),
						'correo' => $this->input->post('correo'),
						'telefono' => $this->input->post('telefono'),
						'clave' => $this->input->post('clave')
					];
				}
				else{
					$data = [
						'nombre' => $this->input->post('nombre'),
						'permisos' => $this->input->post('permiso'),
						'correo' => $this->input->post('correo'),
						'telefono' => $this->input->post('telefono'),
					];
				}
					if($this->Usuario_model->actualizar($data,$id)){
						redirect('admin/usuario/perfil/'.$id,'refresh');
					}else {
					  redirect('admin/usuario/perfil/'.$id,'refresh');
					}
				}
	}

	public function perfil($id)
	{
		$this->check_seg();
		$this->load->model('Articulo_model');
		$data['titulo'] = 'Usuario';
		$data['controlador'] = 'detalle';
		$data['fila'] = $this->Usuario_model->obtener_por_id_con_dpto($id);
		if($data['fila']==0)
			$data['fila'] = $this->Usuario_model->obtener_por_id($id);
		$data['articulos'] = $this->Articulo_model->obtener_articulos_por_autor_backend($id);
		$this->load->view('backend/base/cabecera',$data);
		$this->load->view('backend/usuario/perfil');
		$this->load->view('backend/base/pie');
	}

	public function generarClave()
	{
		$this->load->helper('string');

		$msg = array('clave'=> random_string('alnum',8));
		$this->output->set_content_type('application/json')
                         ->set_output(json_encode($msg));
	}

}
