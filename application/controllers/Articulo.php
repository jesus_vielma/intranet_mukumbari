<?php
include_once 'Frontend.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Articulo extends Frontend {

private $acentuadas = array('à','á','â','ã','ä','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ú','û','ü','ý','ÿ','À','Á','Â','Ã','Ä','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','Ù','Ú','Û','Ü','Ý','%','#',',','.',';','-');
private $sinacento =  array('a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','u','y','y','A','A','A','A','A','C','E','E','E','E','I','I','I','I','N','O','O','O','O','O','U','U','U','U','Y','','','','','','');
private $espacios = array('          ','         ','        ','        ','      ','     ','    ','   ','  ');
private $esp = array(' ',' ',' ',' ',' ',' ',' ',' ',' ');

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		setlocale(LC_ALL,'es_VE.UTF-8','es_VE','Windows-1252','esp','es_ES.UTF-8','es_ES');
		$this->load->model(array('Categoria_model','Articulo_model'));
	}

	public function index()
	{

		$data['menu'] = $this->menu;
		$data['redes'] = $this->redes;
		$data['articulo'] = $this->Articulo_model->obtener_por_alias($alias);

		$this->load->view('frontend/base/cabecera',$data);
		$this->load->view('frontend/articulo/detalle');
		$this->load->view('frontend/base/pie');
	}

	public function detalle($alias)
	{

		$data['menu'] = $this->menu;
		$data['redes'] = $this->redes;
		$data['articulo'] = $this->Articulo_model->obtener_por_alias_completo($alias);
		$data['controlador']='';
		
		$this->load->view('frontend/base/cabecera',$data);
		$this->load->view('frontend/articulo/index');
		$this->load->view('frontend/base/pie');
	}

	public function nuevo($fase=1)
	{
		$this->logueado();
		if($fase ==1){
			$data['controlador']= 'insertar-t';
			$data['menu'] = $this->menu;
			$data['redes'] = $this->redes;
			$data['categorias'] = $this->Categoria_model->obtener_todos();

			$this->load->view('frontend/base/cabecera',$data);
			$this->load->view('frontend/articulo/nuevo');
			$this->load->view('frontend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('titulo','Titulo','trim|required|max_length[150]|min_length[30]');
			if($this->input->post('alias')!=''){
				$this->form_validation->set_rules('Alias','alias','trim|required|max_length[100]|min_length[30]');
			}
			$this->form_validation->set_rules('categoria','Categoría','trim|required');
			$this->form_validation->set_rules('cuerpo','Mensaje','trim|required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			$this->form_validation->set_message('max_length', 'El campo {field}  no puede tener mas de {param} caracteres');
			$this->form_validation->set_message('min_length', 'El campo {field}  debe tener al menos {param} caracteres');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{
				if ($this->input->post('alias')=='')
				{
					$nombre = str_replace($this->acentuadas,$this->sinacento,$this->input->post('titulo'));
				}
				else{
					$nombre = str_replace($this->acentuadas,$this->sinacento,$this->input->post('alias'));
				}

				$nombre = rtrim(str_replace($this->espacios, $this->esp, $nombre));

					$nombre= strtolower($nombre);		
					$alias = str_replace(' ','_', $nombre);

					if($this->Articulo_model->buscar_alias_igual($alias)){
						$this->load->helper('string');
						$alias.='_'.random_string('numeric','4');
					}

				if($this->input->post('modal')=='1'){
					$modal = '<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="myModal">';
					  $modal .= '<div class="modal-dialog modal-lg" role="document">';
					    $modal .= '<div class="modal-content">';
					      $modal .= '<div class="modal-header">';
					        $modal .= '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
					        $modal .= '<h4 class="modal-title" id="titulo"></h4>';
					      $modal .= '</div>';
					      $modal .= '<div class="modal-body">';
					        $modal .= '<img id="imagen" /><br>';
					        $modal .= '<div id="cuerpo"></div>';
					      $modal .= '</div>';
					      $modal .= '<div class="modal-footer">';
					        $modal .= '<button type="button" class="btn btn-default" id="quitarModal" data-dismiss="modal">Salir</button>';
					      $modal .= '</div>';
					    $modal .= '</div>';
					  $modal .= '</div>';
					$modal .= '</div>';
				}
				else{
					$modal = '';
				}

				$data = [
						'titulo'=> $this->input->post('titulo'),
						'alias'=>$alias,
						'id_categoria' => $this->input->post('categoria'),
						'cuerpo' =>$this->input->post('cuerpo').''.$modal,
						'id_usuario'=>$this->session->userdata('front')['id_usuario'],
						'fecha' => date('Y-m-d H:i:s'),
				 	];

				 	if($this->Articulo_model->insertar($data)){
				 		redirect('inicio','refresh');
				 	}
				 	else{
				 		redirect('nuevo_articulo','refresh');
				 	}

			}
		}
	}

	public function editar($fase=1,$alias='')
	{
		$this->logueado();
		if($fase ==1){
			$data['controlador']= 'insertar-t';
			$data['menu'] = $this->menu;
			$data['redes'] = $this->redes;
			$data['categorias'] = $this->Categoria_model->obtener_todos();
			$data['articulo'] = $this->Articulo_model->obtener_por_alias($alias);

			$this->load->view('frontend/base/cabecera',$data);
			$this->load->view('frontend/articulo/editar');
			$this->load->view('frontend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('titulo','Titulo','trim|required|max_length[150]|min_length[30]');
			$this->form_validation->set_rules('categoria','Categoría','trim|required');
			$this->form_validation->set_rules('cuerpo','Mensaje','trim|required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			$this->form_validation->set_message('max_length', 'El campo {field}  no puede tener mas de {param} caracteres');
			$this->form_validation->set_message('min_length', 'El campo {field}  debe tener al menos {param} caracteres');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{

				$data = [
						'titulo'=> $this->input->post('titulo'),
						'alias'=>$this->input->post('alias'),
						'id_categoria' => $this->input->post('categoria'),
						'cuerpo' =>$this->input->post('cuerpo'),
						'id_usuario'=>$this->session->userdata('front')['id_usuario'],
						'fecha' => $this->input->post('fecha'),
				 	];

				 	if($this->Articulo_model->actualizar($data,$this->input->post('id_articulo'))){
				 		redirect('inicio','refresh');
				 	}
				 	else{
				 		redirect('nuevo_articulo','refresh');
				 	}

			}
		}
	}

	public function categoria($cat){
		$this->load->library('pagination');
		$cat_id = $this->Categoria_model->obtener_id_cat($cat);
		$total_rows = $this->Articulo_model->contar_registros_categoria($cat_id[0]->id_categoria);
		$config['base_url'] = site_url($this->uri->segment(1));
		$config['total_rows'] = $total_rows[0]->registros;
		$config['per_page'] = 3;
		$config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['first_link'] = 'First';
        $config['next_link'] = '<i class="fa fa-chevron-right"></i>';
        $config['prev_link'] = '<i class="fa fa-chevron-left"></i>';
         
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		$articulos = $this->Articulo_model->obtener_articulos_cat($config['per_page'],$page,$cat_id[0]->id_categoria);

		$data['articulos'] =$articulos;
		$data['cat'] = $cat_id[0]->nombre;
		$data['pagination'] = $this->pagination->create_links();
		$data['controlador'] ='';
		$data['titulo'] = 'Mis tickets';

		$data['menu'] = $this->menu;
		$data['redes']= $this->redes;
		$this->load->view('frontend/base/cabecera',$data);
		$this->load->view('frontend/articulo/categoria');
		$this->load->view('frontend/base/pie');
	}

	public function buscar_departamento()
	{
		$nombre = $this->input->post('nombre');
		$this->load->model('Departamento_model');

		$dep = $this->Departamento_model->buscar_departamento_por_nombre($nombre);

		$this->output->set_content_type('application/json')
                         ->set_output(json_encode($dep[0]));
	}
}