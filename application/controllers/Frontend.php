<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {

	public $menu;

	public $redes;

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		setlocale(LC_ALL,'es_VE.UTF-8','es_VE','Windows-1252','esp','es_ES.UTF-8','es_ES');
		$this->load->model(array('Frontend_model','Redes_sociales_model'));
		$this->load->helper('text');

		if($this->session->userdata('front')){
			$padres = $this->Frontend_model->obtener_padres_l();

			foreach ($padres as $padre) {
				$this->menu[$padre->nombre] = array('0'=>$this->Frontend_model->obtener_hijos_l($padre->id_menu),'l'=>$padre->logueado,'alias'=>$padre->alias,'tipo'=>$padre->tipo);

			}
		}else{
			$padres = $this->Frontend_model->obtener_padres();

			foreach ($padres as $padre) {
				$this->menu[$padre->nombre] = array('0'=>$this->Frontend_model->obtener_hijos($padre->id_menu),'l'=>$padre->logueado,'alias'=>$padre->alias,'tipo'=>$padre->tipo);

			}
		}
		$this->redes = $this->Redes_sociales_model->obtener_todos();
	}

	public function login()
	{
		if(!$this->session->userdata('front')){
			$this->form_validation->set_error_delimiters('<div class="alert alert-warning">','</div>');

			$this->form_validation->set_rules('usuario','Usuario','trim|required');
			$this->form_validation->set_rules('clave','Contraseña','trim|required|MD5');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->index();
			}
			elseif($fila= $this->Frontend_model->iniciar($this->input)){
					$user_pass = array();

					$user_pass = array(
						'usuario'    => $this->input->post('usuario'),
						'id_usuario' => $fila[0]->id_usuario,
						'user_ip'	 => $_SERVER['REMOTE_ADDR'],
						'datetime'		 => date('Y-m-d H:i:s'),
						'nombre' 	 => $fila[0]->nombre,
						'config'     => $fila[0]->config,
						'sess_id' 	 => session_id()
					);
					$this->session->set_userdata('front',$user_pass);
					if($this->input->post('pag_ant'))
						redirect($this->input->post('pag_ant'),'refresh');
					else
						redirect('inicio','refresh');
			}
			else {
				$error= 'Los datos suministrados son incorrectos. <br> Verifíquelos y vuelva a intentar';
				$this->session->set_flashdata('error',$error);
				$this->index();
			}
		}
		else{
			redirect('','refresh');
		}
	}

		public function salir(){
		$this->session->unset_userdata('front');
		if(!$this->session->userdata('usuario'))
		$this->session->sess_destroy();
		redirect('', 'refresh');
	}

	public function logueado(){
		if (!$this->session->userdata('front')) {
			redirect('inicio/?ref='.$this->uri->uri_string(),'refresh');
		}
	}

	public function index()
	{
		/*if($this->session->userdata('front')){
			$padres = $this->Frontend_model->obtener_padres_l();

			foreach ($padres as $padre) {
				$menu[$padre->nombre] = array('0'=>$this->Frontend_model->obtener_hijos_l($padre->id_menu),'l'=>$padre->logueado,'alias'=>$padre->alias);

			}
		}else{
			$padres = $this->Frontend_model->obtener_padres();

			foreach ($padres as $padre) {
				$menu[$padre->nombre] = array('0'=>$this->Frontend_model->obtener_hijos($padre->id_menu),'l'=>$padre->logueado,'alias'=>$padre->alias);

			}
		}*/
		
		/*echo "<pre>";
		print_r($menu);
		echo "</pre>";*/
		/*echo "<ul>";
		$var='';
		foreach ($menu as $m => $value) {
			if($var!=$m){
				echo "<li>".$m; 
				if($value[0]>0){
					echo "<ul>";
						foreach ($value[0] as $key) {
							echo "<li>".$key->nombre."</li>"; 
						}
					echo "</ul>";
				}
				echo "</li>";
			}
		}
		echo "</ul>";*/

		$this->load->library('pagination');
		$total_rows = $this->Frontend_model->contar_articulos();

		$config['base_url'] = site_url('inicio');
		$config['total_rows'] = $total_rows;
		$config['per_page'] = 4;
		$config['num_links'] = 2;
		$config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['first_link'] = '<i class="fa fa-chevron-circle-left"></i> Primera';
		$config['last_link'] = 'Ultimo <i class="fa fa-chevron-circle-right"></i>';
        $config['next_link'] = '<i class="fa fa-chevron-right"></i>';
        $config['prev_link'] = '<i class="fa fa-chevron-left"></i>';
         
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		$articulos = $this->Frontend_model->obtener_todos_con_autor($config['per_page'],$page);
		$data['controlador'] = 'inicio';
		$data['titulo'] = 'Inicio';
		$data['menu'] = $this->menu;
		$data['redes'] = $this->redes;
		$data['articulos'] = $articulos;
		$data['pagination'] = $this->pagination->create_links();
		$this->load->view('frontend/base/cabecera',$data);
		$this->load->view('frontend/base/index');
		$this->load->view('frontend/base/pie');
	}

	public function error_404()
	{
		$data['controlador'] = 'inicio';
		$data['titulo'] = 'Inicio';
		$data['menu'] = $this->menu;
		$data['redes'] = $this->redes;
		$this->load->view('frontend/base/cabecera',$data);
		$this->load->view('frontend/errors/404');
		$this->load->view('frontend/base/pie');	
	}
}
