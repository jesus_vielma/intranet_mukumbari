<?php
include_once 'Frontend.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Evento extends Frontend {

private $acentuadas = array('à','á','â','ã','ä','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ú','û','ü','ý','ÿ','À','Á','Â','Ã','Ä','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','Ù','Ú','Û','Ü','Ý','%','#',',','.',';','-');
private $sinacento =  array('a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','u','y','y','A','A','A','A','A','C','E','E','E','E','I','I','I','I','N','O','O','O','O','O','U','U','U','U','Y','','','','','','');
private $espacios = array('          ','         ','        ','        ','      ','     ','    ','   ','  ');
private $esp = array(' ',' ',' ',' ',' ',' ',' ',' ',' ');

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		setlocale(LC_ALL,'es_VE.UTF-8','es_VE','Windows-1252','esp','es_ES.UTF-8','es_ES');
		$this->load->model(array('Evento_model'));
	}

	public function index()
	{

		$data['menu'] = $this->menu;
		$data['redes'] = $this->redes;
		//$data['Evento'] = $this->Evento_model->obtener_por_alias($alias);
		$data['controlador'] = 'calendario';

		$this->load->view('frontend/base/cabecera',$data);
		$this->load->view('frontend/evento/index');
		$this->load->view('frontend/base/pie');
	}

	public function detalle($id)
	{

		$data['menu'] = $this->menu;
		$data['redes'] = $this->redes;
		$data['evento'] = $this->Evento_model->obtener_por_id($id);
		$data['controlador']='';
		
		$this->load->view('frontend/base/cabecera',$data);
		$this->load->view('frontend/evento/detalle');
		$this->load->view('frontend/base/pie');
	}

	public function nuevo($fase=1)
	{
		$this->logueado();
		if($fase ==1){
			$data['controlador']= 'insertar-t';
			$data['menu'] = $this->menu;
			$data['redes'] = $this->redes;

			$this->load->view('frontend/base/cabecera',$data);
			$this->load->view('frontend/evento/nuevo');
			$this->load->view('frontend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('titulo','Titulo','trim|required|max_length[150]|min_length[30]');
			$this->form_validation->set_rules('tipo','Tipo','trim|required');
			$this->form_validation->set_rules('descripcion','Descripcion','trim|required');
			$this->form_validation->set_rules('inicio','Fecha Inicio','trim|required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			$this->form_validation->set_message('max_length', 'El campo {field}  no puede tener mas de {param} caracteres');
			$this->form_validation->set_message('min_length', 'El campo {field}  debe tener al menos {param} caracteres');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{

				if($this->input->post('fin'))
					$fin = $this->input->post('fin');
				else
					$fin = $this->input->post('inicio');


				$data = [
						'titulo'=> $this->input->post('titulo'),
						'descripcion' =>$this->input->post('descripcion'),
						'id_usuario'=>$this->session->userdata('front')['id_usuario'],
						'fecha_inicio' => $this->input->post('inicio'),
						'fecha_fin' => $fin,
						'tipo' => $this->input->post('tipo'),
						'estado' => '0'
				 	];

				 	if($this->Evento_model->insertar($data)){
				 		redirect('eventos','refresh');
				 	}
				 	else{
				 		redirect('nuevo_evento','refresh');
				 	}

			}
		}
	}

	public function editar($fase=1,$alias)
	{
		$this->logueado();
		if($fase ==1){
			$data['controlador']= 'insertar-t';
			$data['menu'] = $this->menu;
			$data['redes'] = $this->redes;
			$data['categorias'] = $this->Categoria_model->obtener_todos();
			$data['Evento'] = $this->Evento_model->obtener_por_alias($alias);

			$this->load->view('frontend/base/cabecera',$data);
			$this->load->view('frontend/Evento/editar');
			$this->load->view('frontend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('titulo','Titulo','trim|required|max_length[150]|min_length[30]');
			$this->form_validation->set_rules('categoria','Categoría','trim|required');
			$this->form_validation->set_rules('cuerpo','Mensaje','trim|required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			$this->form_validation->set_message('max_length', 'El campo {field}  no puede tener mas de {param} caracteres');
			$this->form_validation->set_message('min_length', 'El campo {field}  debe tener al menos {param} caracteres');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{

				$data = [
						'titulo'=> $this->input->post('titulo'),
						'alias'=>$this->input->post('alias'),
						'id_categoria' => $this->input->post('categoria'),
						'cuerpo' =>$this->input->post('cuerpo'),
						'id_usuario'=>$this->session->userdata('front')['id_usuario'],
						'fecha' => $this->input->post('fecha'),
				 	];

				 	if($this->Evento_model->actualizar($data,$this->input->post('id_Evento'))){
				 		redirect('','refresh');
				 	}
				 	else{
				 		redirect('nueva_noticias','refresh');
				 	}

			}
		}
	}


	public function obtener_eventos(){

		$eventosSQL = $this->Evento_model->obtener_todos();
		if($eventosSQL!='0'){
			foreach ($eventosSQL as $e){
			if($e->tipo=='cumpleanos')
			{
				$color = '#5bc178';
				$allDay = true;
			}
			else
			{
				$color = '#3b5998';
				$allDay = false;
				}
			$i = new DateTime($e->fecha_inicio);
			$f = new DateTime($e->fecha_fin);
			$eventos[] = [
				'id' => $e->id_evento,
				'start' => $i->format('Y-m-d').'T'.$i->format('H:i:s'),
				'end' => $f->format('Y-m-d').'T'.$f->format('H:i:s'),
				'url' => site_url('evento/'.$e->id_evento),
				'color' => $color,
				'allDay'=> $allDay,
				'title'=> $e->titulo
			];
		}
		$this->output->set_content_type('application/json')
                         ->set_output(json_encode($eventos));
		}
	}
}
