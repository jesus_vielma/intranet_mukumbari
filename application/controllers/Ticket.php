<?php
include_once 'Frontend.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends Frontend {

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		setlocale(LC_ALL,'es_VE.UTF-8','es_VE','Windows-1252','esp','es_ES.UTF-8','es_ES');
		$this->load->model(array('Ticket_model','Prioridad_model','Departamento_model'));
	}

	public function index()
	{
		$this->logueado();

		$this->load->library('pagination');
		$dep = $this->Ticket_model->obtener_dep_act($this->session->userdata('front')['id_usuario']);
		$total_rows = $this->Ticket_model->contar_registros($dep[0]->id_departamento);
		$config['base_url'] = site_url($this->uri->segment(1));
		$config['total_rows'] = $total_rows[0]->registros;
		$config['per_page'] = 3;
		$config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
        $config['first_link'] = 'First';
        $config['next_link'] = '<i class="fa fa-chevron-right"></i>';
        $config['perv_link'] = '<i class="fa fa-chevron-left"></i>';
         
        $config['cur_tag_open'] = "<li><span><b>";
        $config['cur_tag_close'] = "</b></span></li>";

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		$tickets = $this->Ticket_model->obtener_tickets($config['per_page'],$page,$dep[0]->id_departamento);

		foreach ($tickets as $ticket ) {
			$info_tickets[$ticket->id_ticket] = array('asunto' => $ticket->asunto,
													  'id_ticket'=>$ticket->id_ticket,
													  'estado'=>$ticket->estado,
													  'fc'=>$ticket->fecha_creacion,
													  'color'=>$ticket->color,
													  'nombre_prioridad'=>$ticket->nombre_prioridad,
													  'usuario' => $ticket->generador,
													  'depa' => $ticket->dpto,
													  'dptos'=>$this->Ticket_model->obtener_dpto_involucrados($ticket->id_ticket),
													  'ultima'=>$this->Ticket_model->obtener_ultima_vez($ticket->id_ticket));
		}
		$data['tickets'] =$info_tickets;
		$data['pagination'] = $this->pagination->create_links();
		$data['controlador'] ='';
		$data['boton_activo'] = 'departamento';
		$data['titulo'] = 'Mis tickets';

		$data['menu'] = $this->menu;
		$data['redes']= $this->redes;
		$this->load->view('frontend/base/cabecera',$data);
		$this->load->view('frontend/ticket/index');
		$this->load->view('frontend/base/pie');
	}

	public function nuevo($fase=1)
	{
		$this->logueado();
		$data['titulo'] = 'Nuevo ticket';
		$data['controlador'] = 'insertar-t';
		$data['menu'] = $this->menu;
		$data['redes']= $this->redes;

		$data['prioridades'] = $this->Prioridad_model->obtener_activas();
		$data['departamentos'] = $this->Departamento_model->obtener_todos();

		$data['dep_user'] = $this->Ticket_model->obtener_dep_act($this->session->userdata('front')['id_usuario']);

		if($fase ==1){

			$this->load->view('frontend/base/cabecera',$data);
			$this->load->view('frontend/ticket/nuevo');
			$this->load->view('frontend/base/pie');
		}
		else{
			
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('asunto','Nombre','trim|required');
			$this->form_validation->set_rules('cuerpo','Mensaje','trim|required');
			$this->form_validation->set_rules('prioridad','Prioridad','trim|required');
			$this->form_validation->set_rules('departamentos[]','Coordinaciones','trim|required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{
				$this->load->helper('string');

				$id_ticket = random_string('alnum',8);

				$ticket = [
							'id_ticket' 		=> $id_ticket,
							'asunto'    		=> $this->input->post('asunto'),
							'cuerpo'			=> $this->input->post('cuerpo'),
							'fecha_creacion'	=> date('Y-m-d H:i:s'),
							'estado'			=> 'Nuevo',
							'prioridad' 		=> $this->input->post('prioridad'),
							'id_usuario'		=> $this->session->userdata('front')['id_usuario']
						  ];
				$deptos = $this->input->post('departamentos');
				$a=0;
				foreach ($deptos as $dep ) {
					$dptos[$a] = [	
									'id_ticket' 		=>$id_ticket,
									'id_departamento'	=>$dep,
									'estado'			=>'Sin leer',
									'fecha'				=>date('Y-m-d H:i:s'),
						 ];
						 $a++;
				}

					if($this->Ticket_model->insertar($ticket,$dptos)){
							redirect('mis_tickets','refresh');
				 	}else {
				  		redirect('nuevo_ticket','refresh');
				 	}
				}
		}
	}

	public function respuesta()
	{
		$this->logueado();

			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('cuerpo','Nombre','trim|required');
			//$this->form_validation->set_rules('default','Predeterminada','trim|required');
			//$this->form_validation->set_rules('organigrama','Organigrama','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->detalle($this->input->post('id_ticket'));
			}
			else{
				  	$data = [
						'id_ticket'=> $this->input->post('id_ticket'),
						'mensaje'=>$this->input->post('cuerpo'),
						'fecha' => date('Y-m-d H:i:s'),
						'id_usuario' =>$this->session->userdata('front')['id_usuario'],
				 	];

				 	$data1 = [
				 				'id_ticket' => $this->input->post('id_ticket'),
				 				'id_usuario' =>$this->session->userdata('front')['id_usuario'],
				 				'fecha' => date('Y-m-d H:i:s'),
				 				'accion' => 'Responder',
				 			];

					if($this->Ticket_model->respuesta($data)){
						$this->Ticket_model->insertar_h($data1);
						redirect('ticket','refresh');
				 	}else {
				  		redirect('ticker/detalle/'.$this->input->post('id_ticket'),'refresh');
				 	}
		}
	}

	public function detalle($id)
	{
		$this->logueado();
		$data['titulo'] = 'Detalle del ticket';
		$data['controlador'] = 'respuesta';
		$tickets= $this->Ticket_model->obtener_por_id($id);
			$info_tickets[0] = array('asunto' => $tickets[0]->asunto,
													  'id_ticket'=>$tickets[0]->id_ticket,
													  'estado'=>$tickets[0]->estado,
													  'fc'=>$tickets[0]->fecha_creacion,
													  'color'=>$tickets[0]->color,
													  'nombre_prioridad'=>$tickets[0]->nombre_prioridad,
													  'usuario' => $this->Ticket_model->obtener_generador($tickets[0]->generador),
													  'cuerpo'=>$tickets[0]->cuerpo,
													  'dptos'=>$this->Ticket_model->obtener_dpto_involucrados($tickets[0]->id_ticket),
													  'ultima'=>$this->Ticket_model->obtener_ultima_vez($tickets[0]->id_ticket));

		$data['tickets'] = $info_tickets;
		$data['respuestas'] = $this->Ticket_model->obtener_respuestas($id);

		$data['menu'] = $this->menu;
		$data['redes']= $this->redes;
		$this->load->view('frontend/base/cabecera',$data);
		$this->load->view('frontend/ticket/detalle');
		$this->load->view('frontend/base/pie');
	}

	public function generados()
	{
		echo "generados por mi";
	}

	public function cambiar_estado($id,$estado)
	{
		if($estado =='Cerrado')
			$accion = 'Cerrar';
		elseif($estado =='Abierto')
			$accion = 'Reabrir';

		$data1 = [
	 				'id_ticket' => $id,
	 				'id_usuario' =>$this->session->userdata('front')['id_usuario'],
	 				'fecha' => date('Y-m-d H:i:s'),
	 				'accion' => $accion,
	 			];

	 	if($this->Ticket_model->cerrar_ticket($id,$estado)){
	 		$this->Ticket_model->insertar_h($data1);
	 		redirect('mis_tickets','refresh');
	 	}else{
	 		redirect('ticket/detalle/'.$id,'refresh');
	 	}

	}

}
