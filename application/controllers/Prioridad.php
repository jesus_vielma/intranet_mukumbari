<?php
include_once 'Backend.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Prioridad extends Backend {

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		$this->load->model('Prioridad_model');
	}

	public function index()
	{
		$this->check_seg();
		$data['titulo'] = 'Ticket | Prioridad';
		$data['controlador'] = 'listar';
		$data['filas'] = $this->Prioridad_model->obtener_todos();
		$this->load->view('backend/base/cabecera',$data);
		$this->load->view('backend/prioridad/index');
		$this->load->view('backend/base/pie');
	}

	public function nuevo($fase=1)
	{
		$this->check_seg();
		$data['titulo'] = 'Prioridad';
		$data['controlador'] = 'insertar';

		if($fase ==1){

			$this->load->view('backend/base/cabecera',$data);
			$this->load->view('backend/prioridad/nuevo');
			$this->load->view('backend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('predeterminado','Predeterminada','trim|required');
			$this->form_validation->set_rules('color','Color','trim|required');
			//$this->form_validation->set_rules('organigrama','Organigrama','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{
				if($this->input->post('predeterminado')=='1'){

					$ant= $this->Prioridad_model->buscar_default();

					$this->Prioridad_model->cambiar_default($ant[0]->id_prioridad,'0');
				}

				  	$data = [
						'nombre'=> $this->input->post('nombre'),
						'color'=>$this->input->post('color'),
						'predeterminado' => $this->input->post('predeterminado'),
				 	];

					if($this->Prioridad_model->insertar($data)){
						redirect('admin/prioridad','refresh');
				 	}else {
				  		redirect('admin/prioridad/nuevo','refresh');
				 	}
				}
		}
	}

	public function editar($fase=1,$id)
	{
		$this->check_seg();
		$data['titulo'] = 'Prioridad';
		$data['controlador'] = 'editar';
		$data['fila'] = $this->Prioridad_model->obtener_por_id($id);


		if($fase ==1){

			$this->load->view('backend/base/cabecera',$data);
			$this->load->view('backend/prioridad/editar');
			$this->load->view('backend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('predeterminado','Predeterminada','trim|required');
			$this->form_validation->set_rules('color','Color','trim|required');
			//$this->form_validation->set_rules('organigrama','Organigrama','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{

				if($this->input->post('predeterminado')=='1'){

					$ant= $this->Prioridad_model->buscar_default();

					$this->Prioridad_model->cambiar_default($ant[0]->id_prioridad,'0');

				}

				  	$data = [
						'nombre'=> $this->input->post('nombre'),
						'color'=>$this->input->post('color'),
						'predeterminado' => $this->input->post('predeterminado'),
						'id_prioridad' =>$this->input->post('id_prioridad'),
				 	];

					if($this->Prioridad_model->actualizar($data)){
						redirect('admin/prioridad','refresh');
				 	}else {
				  		redirect('admin/prioridad/nuevo','refresh');
				 	}
				}
		}
	}

	public function cambiar()
	{
		$ant= $this->Prioridad_model->buscar_default();

		$this->Prioridad_model->cambiar_default($ant[0]->id_prioridad,'0');

		if($this->Prioridad_model->cambiar_default($this->input->post('id'),'1')){
			echo '0';
		}
		else{
			echo '1';
		}
	}

}
