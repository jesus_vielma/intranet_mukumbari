<?php
include_once 'Backend.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends Backend {

	private $acentuadas = array('à','á','â','ã','ä','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ú','û','ü','ý','ÿ','À','Á','Â','Ã','Ä','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','Ù','Ú','Û','Ü','Ý','%','#',',','.',';','-');
private $sinacento =  array('a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','u','y','y','A','A','A','A','A','C','E','E','E','E','I','I','I','I','N','O','O','O','O','O','U','U','U','U','Y','','','','','','');
private $espacios = array('          ','         ','        ','        ','      ','     ','    ','   ','  ');
private $esp = array(' ',' ',' ',' ',' ',' ',' ',' ',' ');

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		$this->load->model('Menu_model');
	}

	public function index()
	{
		$this->check_seg();
		$data['titulo'] = 'Menú';
		$data['controlador'] = 'menu';

		$padres = $this->Menu_model->obtener_padres_l();

			foreach ($padres as $padre) {
				$menu[$padre->nombre] = array('0'=>$this->Menu_model->obtener_hijos_l($padre->id_menu),'id_menu'=>$padre->id_menu,'posicion'=>$padre->posicion);

			}
		$data['menu'] = $menu;
		$this->load->view('backend/base/cabecera',$data);
		$this->load->view('backend/menu/index');
		$this->load->view('backend/base/pie');
	}

	public function nuevo($fase=1)
	{
		$this->check_seg();
		$data['titulo'] = 'Menú';
		$data['controlador'] = 'insertar-menu';
		$data['ultimo_padre'] = $this->Menu_model->obtener_ultimo_padre();
		$this->load->model('Categoria_model');
		$data['categorias'] = $this->Categoria_model->obtener_todos();
		$this->load->model('Articulo_model');
		$data['articulos'] = $this->Articulo_model->obtener_todos();

		if($fase ==1){

			$this->load->view('backend/base/cabecera',$data);
			$this->load->view('backend/menu/nuevo');
			$this->load->view('backend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');

			if($this->input->post('url') !='')
			$this->form_validation->set_rules('url','URL','trim|required|prep_url');
			//$this->form_validation->set_rules('organigrama','Organigrama','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{

					if($this->input->post('accion1') == 'categoria'){
						$alias_menu = $this->input->post('accion2');

						if($this->Menu_model->buscar_alias_igual($alias_menu)){
							$this->load->helper('string');
							$alias_menu.='_'.random_string('numeric','2');
						}
						$ruta = "$"."route['".$alias_menu."']"." = "."'articulo/categoria/".$this->input->post('accion2')."';\n";
						$ruta .= "$"."route['".$alias_menu."/([a-z_0-9]+)']"." = "."'articulo/detalle/$"."1';\n";
						$this->ruta($ruta);
					}
					elseif($this->input->post('accion1') == 'articulo'){
						$nombre = str_replace($this->acentuadas,$this->sinacento,$this->input->post('nombre'));
						$nombre = rtrim(str_replace($this->espacios, $this->esp, $nombre));

						$nombre= strtolower($nombre);		
						$alias_menu = str_replace(' ','_', $nombre);

						if($this->Menu_model->buscar_alias_igual($alias_menu)){
							$this->load->helper('string');
							$alias_menu.='_'.random_string('numeric','2');
						}

						$ruta = "$"."route['".$alias_menu."']"." = "."'articulo/detalle/".$this->input->post('accion2')."';\n";

					    $this->ruta($ruta);

					}else{
						$alias_menu = $this->input->post('url');
					}

				  	$data = [
						'nombre'=> $this->input->post('nombre'),
						'alias'=>$alias_menu,
						'posicion' => $this->input->post('posicion'),
						'logueado' => $this->input->post('logueado'),
						'estado'   => '1',
						'padre'    => '0',
						'tipo'     => $this->input->post('tipo'),
				 	];
				 	/*
				 	echo "<pre>";
				 	print_r($data);
				 	echo "<pre>";

				 	echo $ruta;*/
				 	//$nombre_archivo = '/var/www/html/intranet-m/application/config/routes.php';

					if($this->Menu_model->insertar($data)){
						redirect('admin/menu','refresh');
				 	}else {
				  		redirect('admin/menu/nuevo','refresh');
				 	}
				}
		}
	}

	public function editar($fase=1,$id)
	{
		$this->check_seg();
		$data['titulo'] = 'Categoria';
		$data['controlador'] = 'editar';
		$data['fila'] = $this->Menu_model->obtener_por_id($id);


		if($fase ==1){

			$this->load->view('backend/base/cabecera',$data);
			$this->load->view('backend/menu/editar');
			$this->load->view('backend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('default','Predeterminada','trim|required');
			//$this->form_validation->set_rules('organigrama','Organigrama','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{
				if ($this->input->post('alias')=='')
				{
					$nombre = str_replace($this->acentuadas,$this->sinacento,$this->input->post('nombre'));
					$nombre= strtolower($nombre);

					$alias = str_replace(' ','_', $nombre);
				}
				else{
					$alias = $this->input->post('alias');
				}

				if($this->input->post('default')=='1'){

					$ant= $this->Categoria_model->buscar_default();

					$this->Categoria_model->cambiar_default($ant[0]->id_categoria,'0');

				}

				  	$data = [
						'nombre'=> $this->input->post('nombre'),
						'alias'=>$alias,
						'default' => $this->input->post('default'),
						'id_categoria' =>$this->input->post('id_categoria'),
				 	];

					if($this->Categoria_model->actualizar($data)){
						redirect('admin/categoria','refresh');
				 	}else {
				  		redirect('admin/categoria/nuevo','refresh');
				 	}
				}
		}
	}

	public function ordenarMenu()
	{
		$this->check_seg();
		$menu = json_decode($this->input->post('menuNuevo'));
		$i= 1;
		$j =1;
		foreach ($menu as $m => $valor ) {
			foreach ($valor as $key => $value) {
				if(!is_array($value)){
					$a[] = ['id_menu'=> $value, 'posicion'=>$i,'padre'=>'0'];
					$i++;
					$padre = $value;
				}
				else{
					//$padre = $value[0];
					foreach ($value as $key1 => $value1) {
						$a[] = ['id_menu'=> $value1->id, 'posicion'=>$j,'padre'=>$padre];
						$j++;
					}
					$j=1;
				}
				
			}
			/*echo "<pre>";
			print_r($a);
			echo "</pre>";*/
		}
		
		/*echo "<pre>";
		print_r($a);
		echo "</pre>";*/

		if($this->Menu_model->actualizar_orden($a))
			redirect('admin/menu','refresh');
		else
			redirect('admin/menu','refresh');
	}

	private function ruta($ruta){

		$nombre_archivo = $_SERVER['DOCUMENT_ROOT'].'/intranet-m/application/config/routes.php';

	    // En nuestro ejemplo estamos abriendo $nombre_archivo en modo de adición.
	    // El puntero al archivo está al final del archivo
	    // donde irá $contenido cuando usemos fwrite() sobre él.
	    if (!$gestor = fopen($nombre_archivo, 'a')) {
	         //echo "No se puede abrir el archivo ($nombre_archivo)";
	         exit;
	    }

	    // Escribir $contenido a nuestro archivo abierto.
	    if (fwrite($gestor, $ruta) === FALSE) {
	        //echo "No se puede escribir en el archivo ($nombre_archivo)";
	        exit;
	    }

	    //echo "Éxito, se escribió ($ruta) en el archivo ($nombre_archivo)";

	    fclose($gestor);
	}

}
