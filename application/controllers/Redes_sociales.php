<?php
include_once 'Backend.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Redes_sociales extends Backend {


	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		$this->load->model('Redes_sociales_model');
	}

	public function index()
	{
		$this->check_seg();
		$data['titulo'] = 'Redes Sociales';
		$data['controlador'] = 'listar';
		$data['filas'] = $this->Redes_sociales_model->obtener_todos();
		$this->load->view('backend/base/cabecera',$data);
		$this->load->view('backend/redes/index');
		$this->load->view('backend/base/pie');
	}

	public function nuevo($fase=1)
	{
		$this->check_seg();
		$data['titulo'] = 'Redes Sociales';
		$data['controlador'] = 'insertar';

		if($fase ==1){

			$this->load->view('backend/base/cabecera',$data);
			$this->load->view('backend/redes/nuevo');
			$this->load->view('backend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('url','URL','trim|required|valid_url');
			$this->form_validation->set_rules('tipo','Red','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			$this->form_validation->set_message('valid_url', 'El campo {field} debe ser una URL valida');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{

				  	$data = [
						'nombre'=> $this->input->post('nombre'),
						'url'=>$this->input->post('url'),
						'tipo' => $this->input->post('tipo'),
				 	];

					if($this->Redes_sociales_model->insertar($data)){
						redirect('admin/redes_sociales','refresh');
				 	}else {
				  		redirect('admin/redes_sociales/nuevo','refresh');
				 	}
				}
		}
	}

	public function editar($fase=1,$id)
	{
		$this->check_seg();
		$data['titulo'] = 'Redes Sociales';
		$data['controlador'] = 'editar';
		$data['fila'] = $this->Redes_sociales_model->obtener_por_id($id);


		if($fase ==1){

			$this->load->view('backend/base/cabecera',$data);
			$this->load->view('backend/redes/editar');
			$this->load->view('backend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('url','URL','trim|required|valid_url');
			$this->form_validation->set_rules('tipo','Red','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');
			$this->form_validation->set_message('valid_url', 'El campo {field} debe ser una URL valida');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{

				  	$data = [
						'nombre'=> $this->input->post('nombre'),
						'url'=>$this->input->post('url'),
						'tipo' => $this->input->post('tipo'),
						'id_red'=>$this->input->post('id_red'),
				 	];

					if($this->Redes_sociales_model->actualizar($data)){
						redirect('admin/redes_sociales','refresh');
				 	}else {
				  		redirect('admin/redes_sociales/editar/1/'.$id,'refresh');
				 	}
				}
		}
	}

}
