<?php
include_once 'Backend.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Departamento extends Backend {

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		$this->load->model('Departamento_model');
		$this->load->helper(array('text','typography'));
	}

	public function index()
	{
		$this->check_seg();
		$data['titulo'] = 'Departamento';
		$data['controlador'] = 'listar-dpto';
		$data['filas'] = $this->Departamento_model->obtener_todos();
		$this->load->view('backend/base/cabecera',$data);
		$this->load->view('backend/departamento/index');
		$this->load->view('backend/base/pie');
	}

	public function nuevo($fase=1)
	{
		$this->check_seg();
		$data['titulo'] = 'Departamento';
		$data['controlador'] = 'insertar-w';

		if($fase ==1){

			$this->load->view('backend/base/cabecera',$data);
			$this->load->view('backend/departamento/nuevo');
			$this->load->view('backend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('nivel','Nivel','trim|required');
			$this->form_validation->set_rules('descripcion','Descripción','trim|required');
			//$this->form_validation->set_rules('organigrama','Organigrama','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else {

				$config['upload_path']          = './assets/uploads/estructura';
				$config['allowed_types']        = 'jpg|png';

				  $this->load->library('upload', $config);

				  if ( ! $this->upload->do_upload('organigrama'))
				  {
						$data['error'] = $this->upload->display_errors();
						$this->load->view('backend/base/cabecera',$data);
			  			$this->load->view('backend/departamento/nuevo');
			  			$this->load->view('backend/base/pie');
				  }
				  else
				  {
					  $info = $this->upload->data();
					  $data = [
						  'nombre'=> $this->input->post('nombre'),
						  'nivel'=>$this->input->post('nivel'),
						  'organigrama' =>'estructura/'.$info['file_name'],
						  'descripcion' => $this->input->post('descripcion'),
					  ];

					  if($this->Departamento_model->insertar($data)){
						  redirect('admin/departamento','refresh');
					  }else {
					  	redirect('admin/departamento/nuevo','refresh');
					  }
				  }
			}
		}
	}

	public function editar($fase=1,$id)
	{
		$this->check_seg();
		$data['titulo'] = 'Departamento';
		$data['controlador'] = 'editar-w';
		$data['fila'] = $this->Departamento_model->obtener_por_id($id);

		if($fase ==1){

			$this->load->view('backend/base/cabecera',$data);
			$this->load->view('backend/departamento/editar');
			$this->load->view('backend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('nivel','Nivel','trim|required');
			$this->form_validation->set_rules('descripcion','Descripción','trim|required');
			//$this->form_validation->set_rules('organigrama','Organigrama','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->editar(1,$id);
			}
			else {
				if($this->input->post('opcionOrg')!='actual'){
					$config['upload_path']          = './assets/uploads/estructura';
					$config['allowed_types']        = 'jpg|png';

					  $this->load->library('upload', $config);

					  if ( ! $this->upload->do_upload('organigrama'))
					  {
							$data['error'] = $this->upload->display_errors();
							$this->load->view('backend/base/cabecera',$data);
				  			$this->load->view('backend/departamento/nuevo');
				  			$this->load->view('backend/base/pie');
					  }
					  else
					  {
						  $info = $this->upload->data();
						  $data = [
							  'nombre'=> $this->input->post('nombre'),
							  'nivel'=>$this->input->post('nivel'),
							  'organigrama' =>'estructura/'.$info['file_name'],
							  'descripcion' => $this->input->post('descripcion'),
							  'id_departamento'=>$id,
						  ];
							  if($this->Departamento_model->actualizar($data)){
			  					redirect('admin/departamento','refresh');
			  				}else {
			  				  redirect('admin/departamento/nuevo','refresh');
			  				}
					  }
				}
				else {
					$data = [
						'nombre'=> $this->input->post('nombre'),
						'nivel'=>$this->input->post('nivel'),
						'organigrama' =>$this->input->post('organigrama1'),
						'descripcion' => $this->input->post('descripcion'),
						'id_departamento'=>$id,
					];
					if($this->Departamento_model->actualizar($data)){
						redirect('admin/departamento','refresh');
					}else {
					  redirect('admin/departamento/nuevo','refresh');
					}
				}

			}
		}
	}

	public function detalle($id)
	{
		$this->check_seg();
		$data['titulo'] = 'Departamento';
		$data['controlador'] = 'detalle-dep';
		$data['fila'] = $this->Departamento_model->obtener_por_id($id);
		$data['usuarios'] = $this->Departamento_model->obtener_usuarios_activos($id);
		$data['listaUsuarios'] = $this->Departamento_model->obtener_usuarios();
		if($this->session->flashdata('errorVincular')){
			foreach ($this->session->flashdata('errorVincular') as $error) {
				$a[] = $this->Departamento_model->obtener_info_usuario($error);
			}
			$data['erroresVincular']= $a;
		}
		$this->load->view('backend/base/cabecera',$data);
		$this->load->view('backend/departamento/detalle');
		$this->load->view('backend/base/pie');
	}

	public function vincular($desicion)
	{
		$this->check_seg();
		if($desicion ==1){
			$activos = $this->Departamento_model->obtener_todos_activos();
			//print_r($activos);

			foreach ($activos as $ac ) {
				$activos2[] = $ac->id_usuario;
			}

			//$selected ='';
			$usuarios = $this->input->post('usuarios');
			$dep = $this->input->post('id_departamento');
			foreach ($usuarios as $usuario) {
				if (!in_array($usuario, $activos2)) {
					$data[] = ['id_usuario' => $usuario , 'id_departamento' => $dep,'estado'=>'1'];
				}
				else{
					$error[] = $usuario;
				}
			}
			if(isset($data)){
				if($this->Departamento_model->vincular($data)){
					$this->session->set_flashdata('errorVincular',$error);
					redirect('admin/departamento/detalle/'.$dep);
				}
				else{
					redirect('admin/departamento/detalle/'.$dep);
				}
			}
			else{
				$this->session->set_flashdata('errorVincular',$error);
					redirect('admin/departamento/detalle/'.$dep);
			}
		}
		else{
			if($this->Departamento_model->desvincular($this->input->post('id_usuario'),$this->input->post('id_departamento'),'1')){
				redirect('admin/departamento/detalle/'.$this->input->post('id_departamento'));
			}else{
				redirect('admin/departamento/detalle/'.$this->input->post('id_departamento'));
			}
		}
	}

	public function desvincular()
	{
		$this->check_seg();
		$id_u = $this->input->post('id_usuario');
		$id_d = $this->input->post('id_departamento');

		if($this->Departamento_model->desvincular($id_u,$id_d,'0')){
			redirect('admin/departamento/detalle/'.$id_d);
		}else{
			redirect('admin/departamento/detalle/'.$id_d);
		}
	}

}
