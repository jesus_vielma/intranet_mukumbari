<?php
include_once 'Backend.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends Backend {

private $acentuadas = array('à','á','â','ã','ä','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ú','û','ü','ý','ÿ','À','Á','Â','Ã','Ä','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','Ù','Ú','Û','Ü','Ý');
private $sinacento =  array('a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','u','y','y','A','A','A','A','A','C','E','E','E','E','I','I','I','I','N','O','O','O','O','O','U','U','U','U','Y');

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		$this->load->model('Categoria_model');
	}

	public function index()
	{
		$this->check_seg();
		$data['titulo'] = 'Categoria';
		$data['controlador'] = 'listar';
		$data['filas'] = $this->Categoria_model->obtener_todos();
		$this->load->view('backend/base/cabecera',$data);
		$this->load->view('backend/categoria/index');
		$this->load->view('backend/base/pie');
	}

	public function nuevo($fase=1)
	{
		$this->check_seg();
		$data['titulo'] = 'Categoria';
		$data['controlador'] = 'insertar';

		if($fase ==1){

			$this->load->view('backend/base/cabecera',$data);
			$this->load->view('backend/categoria/nuevo');
			$this->load->view('backend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('default','Predeterminada','trim|required');
			//$this->form_validation->set_rules('organigrama','Organigrama','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{
				if ($this->input->post('alias')=='')
				{
					$nombre = str_replace($this->acentuadas,$this->sinacento,$this->input->post('nombre'));
					
					$nombre= strtolower($nombre);					
					$alias = str_replace(' ','_', $nombre);
				}
				else{
					$alias = $this->input->post('alias');
				}

				if($this->input->post('default')=='1'){

					$ant= $this->Categoria_model->buscar_default();

					$this->Categoria_model->cambiar_default($ant[0]->id_categoria,'0');
				}

				  	$data = [
						'nombre'=> $this->input->post('nombre'),
						'alias'=>$alias,
						'default' => $this->input->post('default'),
				 	];

					if($this->Categoria_model->insertar($data)){
						redirect('admin/categoria','refresh');
				 	}else {
				  		redirect('admin/categoria/nuevo','refresh');
				 	}
				}
		}
	}

	public function editar($fase=1,$id)
	{
		$this->check_seg();
		$data['titulo'] = 'Categoria';
		$data['controlador'] = 'editar';
		$data['fila'] = $this->Categoria_model->obtener_por_id($id);


		if($fase ==1){

			$this->load->view('backend/base/cabecera',$data);
			$this->load->view('backend/categoria/editar');
			$this->load->view('backend/base/pie');
		}
		else{
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger">','</div>');

			$this->form_validation->set_rules('nombre','Nombre','trim|required');
			$this->form_validation->set_rules('default','Predeterminada','trim|required');
			//$this->form_validation->set_rules('organigrama','Organigrama','required');

			$this->form_validation->set_message('required', 'El campo %s  es requerido');

			if($this->form_validation->run()== FALSE )
			{
				$this->nuevo(1);
			}
			else{
				if ($this->input->post('alias')=='')
				{
					$nombre = str_replace($this->acentuadas,$this->sinacento,$this->input->post('nombre'));
					$nombre= strtolower($nombre);

					$alias = str_replace(' ','_', $nombre);
				}
				else{
					$alias = $this->input->post('alias');
				}

				if($this->input->post('default')=='1'){

					$ant= $this->Categoria_model->buscar_default();

					$this->Categoria_model->cambiar_default($ant[0]->id_categoria,'0');

				}

				  	$data = [
						'nombre'=> $this->input->post('nombre'),
						'alias'=>$alias,
						'default' => $this->input->post('default'),
						'id_categoria' =>$this->input->post('id_categoria'),
				 	];

					if($this->Categoria_model->actualizar($data)){
						redirect('admin/categoria','refresh');
				 	}else {
				  		redirect('admin/categoria/nuevo','refresh');
				 	}
				}
		}
	}

	public function detalle($id)
	{
		$this->check_seg();
		$data['titulo'] = 'Departamento';
		$data['controlador'] = 'detalle';
		$data['fila'] = $this->Departamento_model->obtener_por_id($id);
		$this->load->view('backend/base/cabecera',$data);
		$this->load->view('backend/departamento/detalle');
		$this->load->view('backend/base/pie');
	}

	public function cambiar()
	{
		$this->check_seg();
		$ant= $this->Categoria_model->buscar_default();

		$this->Categoria_model->cambiar_default($ant[0]->id_categoria,'0');

		if($this->Categoria_model->cambiar_default($this->input->post('id'),'1')){
			echo '0';
		}
		else{
			echo '1';
		}
	}

}
