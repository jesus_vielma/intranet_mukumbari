<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller {
public $usuario;

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Caracas');
		$this->load->model('Backend_model');
		setlocale(LC_ALL,'es_VE.UTF-8','es_VE','Windows-1252','esp','es_ES.UTF-8','es_ES');
	}

	public function index()
	{
		$this->check_seg();
		$data['controlador'] = 'index';
		$data['titulo'] = 'Inicio';
		$this->load->view('backend/base/cabecera',$data);
		$this->load->view('backend/base/vacio');
		$this->load->view('backend/base/pie');
	}

	public function check_seg(){
		if($this->uri->segment(1)!='admin')
			redirect('');
		else {
			if (!$this->session->userdata('usuario')) {
				if($segs =  $this->uri->segment_array()){

					foreach ($segs as $seg ) {
						if($seg == 'editar'){
							$url = $url;
							break;
						}
						elseif($seg == 'mes_fil')
							$url = $url.'mes';
						elseif($seg == 'equipo/insertar/2')
							$url = $url.'equipo/insertar';
						else
							$url.= $seg.'/';
					}
					redirect('admin/?ref='.$url);
				}
				else{
					redirect('admin','refresh');
				}
			}
		}
	}

	public function login($fase=1)
	{
		if(!$this->session->userdata('usuario')){
			if($fase==1){
				$this->load->view('backend/login/index');
			}else{
				$this->form_validation->set_error_delimiters('<div class="alert alert-warning">','</div>');

				$this->form_validation->set_rules('usuario','Usuario','trim|required');
				$this->form_validation->set_rules('clave','Contraseña','trim|required|MD5');

				$this->form_validation->set_message('required', 'El campo %s  es requerido');

				if($this->form_validation->run()== FALSE )
				{
					$this->load->view('backend/login/index');
				}
				elseif($fila= $this->Backend_model->iniciar($this->input)){
					if($fila[0]->permisos =='1'){
						$user_pass = array();

						$user_pass = array(
							'usuario'    => $this->input->post('usuario'),
							'id_usuario' => $fila[0]->id_usuario,
							'user_ip'	 => $_SERVER['REMOTE_ADDR'],
							'datetime'		 => date('Y-m-d H:i:s'),
							'nombre' 	 => $fila[0]->nombre,
							'config'     => $fila[0]->config,
							'foto' 		 => $fila[0]->foto,
							'sess_id' 	 => session_id()
						);
						$this->session->set_userdata('usuario',$user_pass);
						if($this->input->post('pag_ant'))
							redirect($this->input->post('pag_ant'),'refresh');
						else
							redirect('admin/backend/index','refresh');
					}
					else{
						$data['error'] = 'Lo sentimos, usted no tiene los permisos necesarios para ingresar al administrador de la intranet.';
						$this->load->view('backend/login/index',$data);
					}
				}
				else {
					$data['error'] = 'Los datos suministrados son incorrectos. <br> Verifíquelos y vuelva a intentar';
					$this->load->view('backend/login/index',$data);
				}
			}
		}
		else{
			redirect('admin/backend/index','refresh');
		}

	}

	public function salir(){
		$this->session->unset_userdata('usuario');
		if(!$this->session->userdata('front'))
		$this->session->sess_destroy();
		redirect('/admin/', 'refresh');
	}
}
