
<!--<div class="row">-->

		<div class="col-sm-8 blog-main">

			<div class="blog-post">
				<h3 class="blog-post-title">Detalles del evento</h3>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default" style="border-color: <?=($evento[0]->tipo)=='evento' ? '#3b5998' : '#5bc178'?>;">
							<div class="panel-heading" style="background-color: <?=($evento[0]->tipo)=='evento' ? '#3b5998' : '#5bc178'?>; color: #FFFFFF">
								<h3 class="panel-title text-center" style="font-size: 20px"><?=$evento[0]->titulo?></h3>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-6">
										<h3>Fecha de inicio</h3>
										<p><?=strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($evento[0]->fecha_inicio))?></p>
									</div>	
									<div class="col-lg-6">
										<h3>Fecha de fin</h3>
										<p><?=strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($evento[0]->fecha_fin))?></p>
									</div>	
								</div>
								<div class="row">
									<div class="col-lg-12">
										<h3>Descripción e información del evento</h3>
										<?=$evento[0]->descripcion?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			<!-- /.blog-post -->
		