<!DOCTYPE html>
<!-- saved from url=(0038)http://getbootstrap.com/examples/blog/ -->
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="<?=base_url('assets/frontend/img/favicon.ico')?>">

	<title><?=NOMBRE_SITIO?></title>

	<!-- Bootstrap core CSS -->
	<link href="<?=base_url('assets/frontend/css/bootstrap.css')?>" rel="stylesheet">

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<link href="<?=base_url('assets/frontend/css/ie10-viewport-bug-workaround.css')?>" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="<?=base_url('assets/frontend/css/blog.css" rel="stylesheet')?>">
	<!-- Estilo propio -->
	<link href="<?=base_url('assets/frontend/css/style.css" rel="stylesheet')?>">
	<link href="<?=base_url('assets/frontend/css/print.css" rel="stylesheet')?>">
	<link href="<?=base_url('assets/frontend/css/bootstrap-social.css" rel="stylesheet')?>">
	<link href="<?=base_url('assets/frontend/font-awesome/css/font-awesome.css" rel="stylesheet')?>">

	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js')?>"></script><![endif]-->
	<!--<script src="<?=base_url('assets/frontend/css/ie-emulation-modes-warning.js')?>"></script>-->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link href="<?=base_url('assets/frontend/plugins/fancybox/dist/jquery.fancybox.css')?>" type="text/css" rel="stylesheet">   
    <?php if($controlador =='insertar-t'):?>
    	<link href="<?=base_url('assets/frontend/plugins/form-multiselect/css/multi-select.css')?>" type="text/css" rel="stylesheet">
		<link href="<?=base_url('assets/frontend/plugins/datetimepicker/css/bootstrap-datetimepicker.css')?>" type="text/css" rel="stylesheet">
	<?php elseif($controlador== 'calendario'):?>
	<link href="<?=base_url('assets/frontend/plugins/fullcalendar/fullcalendar.css')?>" type="text/css" rel="stylesheet">   <!-- Calendar Plugin -->
    <?php endif;?>
</head>

<body>

	<!-- <div class="blog-masthead">
		<div class="container">
			<nav class="blog-nav navbar-default"> -->
				<!--<a class="blog-nav-item active" href="http://getbootstrap.com/examples/blog/#">Home</a>
          <a class="blog-nav-item" href="http://getbootstrap.com/examples/blog/#">New features</a>
          <a class="blog-nav-item" href="http://getbootstrap.com/examples/blog/#">Press</a>
          <a class="blog-nav-item" href="http://getbootstrap.com/examples/blog/#">New hires</a>
          <a class="blog-nav-item" href="http://getbootstrap.com/examples/blog/#">About</a>-->
				<!-- <div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		              <span class="sr-only">Toggle navigation</span>
		              <span class="icon-bar"></span>
		              <span class="icon-bar"></span>
		              <span class="icon-bar"></span>
		            </button>
					<a class="navbar-brand" href="<?=site_url('')?>"><?=NOMBRE_SITIO?></a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<?php 
							$var='';
							foreach ($menu as $m => $value) {
								if($var!=$m){
									if($value[0]>0){
										echo "<li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>".$m."<span class='caret'></span></a>"; 
										echo "<ul class='dropdown-menu'>";
											if($m =='Noticias' || $m =='Calendario')
											echo "<li><a href=".site_url($value['alias']).">Ver ".strtolower($m)."</a></li>"; 
											foreach ($value[0] as $key) {
												echo "<li><a href='".site_url($key->alias)."'>".$key->nombre."</a></li>"; 
											}
										echo "</ul>";
										echo "</li>";
									}
									else{
										echo "<li><a href=".site_url($value['alias']).">".$m."</a></li>"; 
									}
								}
							}
						?> -->
						<!--<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
						</li>-->
					<!-- </ul>
				</div> -->
				<!-- /.navbar-collapse -->
		<!-- 	</nav>
		</div>
	</div> -->

	<div class="container">
		<div class="jumbotron" id="jumbo1">
			<a href="<?=site_url('')?>"><img src="<?=base_url('assets/frontend/img/Cintillo.jpg')?>" class="img-responsive"></a>
		</div>
		<div class="blog-header">
			<h1 class="blog-title"><a href="<?=site_url('')?>"><?=NOMBRE_SITIO?></a></h1>
			<p class="lead blog-description">Noticias, eventos, y otros.</p>
		</div>
		<div class="row">
			<div class="col-sm-2" id="menu">
			<div class="sidebar-module sidebar-module-inset">
				<ul class="nav nav-pills nav-stacked">
					<?php 
								$var='';
								foreach ($menu as $m => $value) {
									if($var!=$m){
										if($value[0]>0){
											echo "<li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>".$m."<span class='caret'></span></a>"; 
											echo "<ul class='dropdown-menu'>";
												foreach ($value[0] as $key) {
													echo "<li><a href='".site_url($key->alias)."'>".$key->nombre."</a></li>"; 
												}
											echo "</ul>";
											echo "</li>";
										}
										else{
											if($value['tipo']=='interno')
												echo "<li class=''><a href='".site_url($value['alias'])."'>".$m."</a></li>"; 
											else
												echo "<li class=''><a href='".$value['alias']."' target='_blank'>".$m."</a></li>"; 
										}
									}
								}
							?>
				</ul>
			</div>

			<!--<div class="sidebar-module">
				<h4>Archives</h4>
				<ol class="list-unstyled">
					<li><a href="http://getbootstrap.com/examples/blog/#">March 2014</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">February 2014</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">January 2014</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">December 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">November 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">October 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">September 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">August 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">July 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">June 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">May 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">April 2013</a></li>
				</ol>
			</div>
			<div class="sidebar-module">
				<h4>Elsewhere</h4>
				<ol class="list-unstyled">
					<li><a href="http://getbootstrap.com/examples/blog/#">GitHub</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">Twitter</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">Facebook</a></li>
				</ol>
			</div>-->
		</div>
