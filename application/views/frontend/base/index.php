	
		<div class="col-sm-8 blog-main">
		<!-- <p>
			<img usemap="#imgmap2017315145321" src="/tinymce/images/otros/Crear-Ticket.png" border="0" alt="" width="303" height="470" />
			<map id="imgmap2017315145321" name="imgmap2017315145321">
				<area shape="rect" alt="Logo" coords="44,11,131,47" target="" />
				<area shape="rect" alt="Titulo" coords="111,67,186,75" target="" />
				<area shape="rect" alt="Nombre" coords="135,19,256,42" target="" />
			</map>
		</p> -->
			<?php if($articulos!='0'):?>
				<?php foreach ($articulos as $articulo) :?>
				<div class="blog-post">
					<h2 class="blog-post-title"><?=$articulo->titulo?></h2>
					<p class="blog-post-meta"><?=strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($articulo->fecha)) ?> <strong>por</strong> <a href="#"><?=$articulo->autor?></a> <strong>en</strong> <a href="<?=site_url($articulo->catalias)?>"><?=$articulo->categoria?></a> 
					<?php if($articulo->autor == $this->session->userdata('front')['nombre']):?>
						<span class="pull-right"><a href="<?=site_url('articulo/editar/1/'.$articulo->alias)?>" title="Editar articulo"><i class="fa fa-pencil"></i></a></span>
					<?php endif;?>
					</p>
						<?php $link = "<a href='".site_url('articulo/'.$articulo->alias)."'>... Leer completo</a>"?>
					<?=word_limiter($articulo->cuerpo,'160',$link)?>

				</div>
				<!-- /.blog-post -->
				<?php endforeach;?>
				<nav>
					<ul class="pagination">
						<?=$pagination?>
					</ul>
				</nav>
			<?php else:?>
				<div class="alert alert-info">
					<strong>¡Oops algo ha salido mal!</strong> <br>
					No hemos encontrado artículos registrados en la base de datos recarga la pagina para comprobar que el error persiste.
				</div>
			<?php endif;?>

		</div>
		<!-- /.blog-main -->

		