<div class="col-sm-2  blog-sidebar">
		<?php if($redes!='0'):?>
			<div class="sidebar-module" id="redes">
				<h4>Redes Sociales</h4>
				<?php foreach ($redes as $red) :?>
					<a href="<?=$red->url?>" class="btn btn-social-icon btn-<?=$red->tipo?>" title="Abrir <?=$red->nombre?>" target="_blank"><i class="fa fa-<?=$red->tipo?>"></i></a>
				<?php endforeach;?>
			</div>
		<?php endif;?>
			<div class="sidebar-module sidebar-module-inset">
			<?php if(!$this->session->userdata('front')):?>
				<h4>Inicia Sesión</h4>
				<form class="form-horizontal" method="POST" action="<?=site_url('frontend/login')?>">
					<?=validation_errors()?>
					<?php if(isset($error)):?>
							<div class="alert alert-danger">
								<?=$error?>
							</div>
                        <?php elseif(isset($_GET['ref'])):?>
                        <div class="alert alert-info">
                            <span>Necesita haber iniciado sesión para ver esa página</span>
                        </div>
                        <input type="hidden" name="pag_ant" value="<?=$_GET['ref']?>">
                        <?php endif;?>
					<?php if($this->session->flashdata('error')):?>
						<div class="alert alert-danger">
							<?=$this->session->flashdata('error')?>
						</div>
					<?php endif;?>
					<div class="form-group">
						<div class="col-sm-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="Usuario" name="usuario">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10">
							<input type="password" class="form-control" id="inputPassword3" placeholder="Clave" name="clave">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10">
							<button type="submit" class="btn btn-primary">Entrar</button>
						</div>
					</div>
				</form>
			<?php else:?>
				<h4>Buen día</h4>
				<p>Hola, <a href="<?=site_url('perfil')?>"><?=$this->session->userdata('front')['nombre']?></a></p>
				<?php if($this->session->userdata('front')['config'] < 3):?>
				<div class="alert alert-danger">Su perfil que no esta configurado al 100% por favor <a href="<?=site_url('perfil/#config')?>">actualizalo</a></div>
				<?php endif;?>
				<a href="<?=site_url('salir')?>" class="btn btn-warning">Salir</a>
			<?php endif;?>
			
			</div>

			<!--<div class="sidebar-module">
				<h4>Archives</h4>
				<ol class="list-unstyled">
					<li><a href="http://getbootstrap.com/examples/blog/#">March 2014</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">February 2014</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">January 2014</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">December 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">November 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">October 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">September 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">August 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">July 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">June 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">May 2013</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">April 2013</a></li>
				</ol>
			</div>
			<div class="sidebar-module">
				<h4>Elsewhere</h4>
				<ol class="list-unstyled">
					<li><a href="http://getbootstrap.com/examples/blog/#">GitHub</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">Twitter</a></li>
					<li><a href="http://getbootstrap.com/examples/blog/#">Facebook</a></li>
				</ol>
			</div>-->
		</div>
		<!-- /.blog-sidebar -->

	</div>
	<!-- /.row -->
	<div class="jumbotron">
		<img src="<?=base_url('assets/frontend/img/pie.jpg')?>" class="img-responsive">
	</div>
</div>
<!-- /.container -->



	<footer class="blog-footer">
		<p><span class="pull-left"><?=NOMBRE_SITIO?> 2017</span> <span class="pull-right"><a href="#">Ir Arriba</a></span></p>
	</footer>


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?=base_url('assets/frontend/js/jquery.min.js')?>"></script>
	<script>
		window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
	</script>
	<script src="<?=base_url('assets/frontend/js/bootstrap.min.js')?>"></script>

	<script src="<?=base_url('assets/frontend/plugins/fancybox/dist/jquery.fancybox.js')?>"></script>
	<script src="<?=base_url('assets/backend/plugins/fullcalendar/moment.js')?>"></script>   <!-- Calendar Plugin -->
	<script>
		$('[data-fancybox]').fancybox({
			iframe: {
				css: {
					height: '500px'
				}
			}
		});
		function close_window() {
			$.fancybox.close('all');
		} 
		function responsive_filemanager_callback(field_id){
			var url=jQuery('#'+field_id).val();
			//alert('update '+field_id+" with "+url);
			
			close_window();
		}
	</script>

	<?php if($controlador == 'insertar-t' || $controlador =='respuesta'):?>
	<script src="<?=base_url('assets/frontend/plugins/tinymce/tinymce.min.js')?>"></script>  	<!-- TinyMCE -->
	<script>
	tinymce.init({
	selector: 'textarea',
	language: 'es',
	toolbar: 'undo redo | preview | styleselect | bold italic | responsivefilemanager link image imgmap | alignleft aligncenter alignright alignjustify | bullist numlist | table',
	plugins: [
      'advlist autolink link lists charmap preview hr anchor pagebreak',
      'wordcount visualblocks visualchars insertdatetime media nonbreaking',
      'save table paste image responsivefilemanager table contextmenu imgmap'
    ],
	image_class_list: [
		{title: 'Predefinida', value: 'img-responsive'},
		{title: 'Retrato', value: 'img-responsive img-thumbnail'},
		{title: 'Puntas redondas', value: 'img-responsive img-rounded'},
		{title: 'Forma circular', value: 'img-responsive img-circle'}
	],
	/*content_css: [
		'<?=base_url("assets/frontend/css/tinymce.style.css")?>'
	],*/
	menubar: false,
  	relative_urls :false,
  	//allow_script_urls: true,
  	external_filemanager_path:"<?=base_url('assets/frontend/plugins/filemanager/')?>",
  	filemanager_title:"Manejador de archivos" ,
  	external_plugins: { "filemanager" : "<?=base_url('assets/frontend/plugins/filemanager/plugin.min.js')?>"},
  	//extended_valid_elements : "img[usemap|class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],map[id|name],area[shape|alt|coords|href|target]",
	});
	</script>
	<script>
		window.ParsleyConfig = {
			  successClass: 'has-success'
			, errorClass: 'has-error'
			, errorElem: '<span></span>'
			, errorsWrapper: '<span class="help-block"></span>'
			, errorTemplate: "<div></div>"
			, classHandler: function(el) {
				return el.$element.closest(".form-group");
			}
		};
			$('#ponerModal').click(function() {
				$('#modal').val(1);
			})
			$('#quitarModal').click(function() {
				$('#modal').val(0);
			})
	</script>
<script src="<?=base_url('assets/frontend/plugins/parsley/parsley.js')?>"></script>
<script src="<?=base_url('assets/frontend/plugins/parsley/es.js')?>"></script>
<script src="<?=base_url('assets/frontend/plugins/form-multiselect/js/jquery.multi-select.min.js')?>"></script>
<script>
	$(document).ready(function () {
		$('#enviar').on('click', function () {
	        $('#form').parsley().validate();
	    });

	    $('#multi-select').multiSelect();
	});
</script>

<script src="<?=base_url('assets/frontend/plugins/datetimepicker/js/bootstrap-datetimepicker.js')?>"></script>
<script src="<?=base_url('assets/backend/plugins/fullcalendar/moment.js')?>"></script>   <!-- Calendar Plugin -->
<script src="<?=base_url('assets/frontend/plugins/datetimepicker/js/locales/bootstrap-datetimepicker.es.js')?>" charset="UTF-8"></script>
<script type="text/javascript">
    $(".inicio").datetimepicker({
		language: "es",
        format: "dd MM yyyy - HH:ii P",
        autoclose: true,
        todayBtn: true,
		showMeridian: true,
		forceParse: 0,
		linkField: "inicio",
		linkFormat: "yyyy-mm-dd hh:ii:00"
    });
	$(".fin").datetimepicker({
		language:'es',
        format: "dd MM yyyy - HH:ii P",
        showMeridian: true,
        autoclose: true,
        todayBtn: true,
		forceParse: 0,
		linkField: "fin",
		linkFormat: "yyyy-mm-dd hh:ii:00"
    });
</script>   

<?php elseif($controlador == 'calendario'):?>
<script src="<?=base_url('assets/frontend/plugins/fullcalendar/fullcalendar.js')?>"></script>   <!-- Calendar Plugin -->
<script src="<?=base_url('assets/frontend/plugins/fullcalendar/locale/es-do.js')?>"></script>   <!-- Calendar Plugin -->

<script>
	$(document).ready(function() {

	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	
	var calendar = $('#calendar-drag').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		eventSources: [
			{
				'url': '<?=site_url('evento/obtener_eventos')?>',
				'type': 'POST',
				'cache': true,
				'error': function (){
					alert('No hay eventos');
				}
			}
		],
	});
	
});

</script>
<?php elseif($controlador == 'perfil'): ?>
<script>
		$('[data-fancybox]').fancybox({
			iframe: {
				css: {
					height: '500px'
				}
			}
		});
		function close_window() {
			$.fancybox.close('all');
		} 
		function responsive_filemanager_callback(field_id){
			var url=jQuery('#'+field_id).val();
			alert('update '+field_id+" with "+url);
			$('#foto_img').attr('src','<?=base_url('assets/uploads/perfiles/')?>'+url);
			close_window();
		}
	</script>

	<script>
		window.ParsleyConfig = {
			  successClass: 'has-success'
			, errorClass: 'has-error'
			, errorElem: '<span></span>'
			, errorsWrapper: '<span class="help-block"></span>'
			, errorTemplate: "<div></div>"
			, classHandler: function(el) {
				return el.$element.closest(".form-group");
			}
		};
	</script>
<script src="<?=base_url('assets/frontend/plugins/parsley/parsley.js')?>"></script>
<script src="<?=base_url('assets/frontend/plugins/parsley/es.js')?>"></script>
<script>
	$(document).ready(function () {
		$('#clave_nueva').change(function () {
			$(this).prop('required',true);
			$('#repetir_clave').prop('required',true);
		});
		$('#enviar').click(function () {
			alert('Si la validación de los datos es correcta y no existen errores al actualizar su perfil, su sesión sera cerrada y deberá volver a iniciar sesión para que los cambios surtan efecto.');
	        $('#form').parsley().validate();
	    });
	});
</script>
<?php endif;?>
<script>
			$('area').click(function () {				
				var title = $(this).attr('alt');
				$('#titulo').text(title);

				$.post('<?=site_url('articulo/buscar_departamento')?>',{nombre:title},function (data) {
					if(data!=null){
						$('#imagen').attr('src','<?=base_url('assets/uploads/')?>'+data.organigrama);
						$('#cuerpo').html(data.descripcion);
						$('#myModal').modal('show');
					}else{
						var a = '<div class="alert alert-info">';
						a += '<h4>No se encuentra la información solicitada</h4>';
						a+='</div>';
						$('#imagen').attr('src','');
						$('#cuerpo').html(a);
						$('#myModal').modal('show');
					}
				});
				
			})

			$('#printTicket').click(function () {
				moment.locale('es');
				var fecha = moment().format('dddd, D [de] MMMM [de] YYYY [a las] h:mm:ss a');
				$('#fechaPrint').html(fecha);
				window.print();
			})
		</script>
</body>

</html>
