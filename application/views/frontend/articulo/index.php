	
		<div class="col-sm-8 blog-main">
			<?php if($articulo!='0'):?>
				<div class="blog-post">
					<h2 class="blog-post-title"><?=$articulo[0]->titulo?></h2>
					<p class="blog-post-meta"><?=strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($articulo[0]->fecha)) ?> <strong>por</strong> <a href="#"><?=$articulo[0]->autor?></a> <strong>en</strong> <a href="<?=site_url($articulo[0]->catalias)?>"><?=$articulo[0]->categoria?></a> 
					<?php if($articulo[0]->autor == $this->session->userdata('front')['nombre']):?>
						<span class="pull-right"><a href="<?=site_url('articulo/editar/1/'.$articulo[0]->alias)?>" title="Editar articulo"><i class="fa fa-pencil"></i></a></span>
					<?php endif;?>
						</p>
						<?php $link = "<a href='".site_url('articulo/'.$articulo[0]->alias)."'> Leer mas</a>"?>
					<?=trim($articulo[0]->cuerpo," \t\n\r\0\x0B");?>

				</div>
				<!-- /.blog-post -->
			<?php else:?>
				<div class="alert alert-info">
					<strong>¡Oops algo ha salido mal!</strong> <br>
					No hemos encontrado el articulo solicitado.
				</div>
			<?php endif;?>

		</div>
		<!-- /.blog-main -->

		