<?php 
$categoriasS = [''=>'- Seleccione -'];
if($categorias>'0'){
	foreach ($categorias as $categoria ) {
		if($categoria->default == 1)
			$selected = $categoria->id_categoria;
		$categoriasS[$categoria->id_categoria] = $categoria->nombre;
	}	
}

/*if ($departamentos>0) {
	foreach ($departamentos as $departamento) {
		if($departamento->predeterminado == 1){
			$selected = $departamento->id_departamento;
		}
		$opcionesDpto[$departamento->id_departamento] = $departamento->nombre;
	}
}*/
?>
<!-- <div class="row"> -->

		<div class="col-sm-8 blog-main">

			<div class="blog-post">
				<h3 class="blog-post-title">Nuevo articulo</h3>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Redactar un nuevo articulo</h3>
							</div>
							<div class="panel-body">
								<?=form_open('articulo/nuevo/2',array('id'=>'form'))?>
								<?=validation_errors()?>
								<div class="form-group">
									<?=form_label('Titulo','titulo')?>
									<?=form_input(array('name'=>'titulo','id'=>'titulo','required'=>'required','class'=>'form-control','value'=>set_value('titulo'),'maxlength'=>'150','minlength'=>'30'))?>
								</div>
								<div class="form-group">
									<?=form_label('Alias','alias')?>
									<?=form_input(array('name'=>'alias','id'=>'alias','class'=>'form-control','value'=>set_value('alias'),'aria-describedby'=>'helpBlock'))?>
									<span id="helpBlock" class="help-block">Este campo se puede dejar en blanco si no se quiere crear un alías para el articulo, si se deja vació se generara automáticamente. </span>
								</div>
								<div class="form-group">
									<?=form_label('Categoría','categoria')?>
									<?=form_dropdown('categoria',$categoriasS,(set_value('categoria')!='' ? set_value('categoria') : $selected),'class="form-control" id="categoria" required')?>
								</div>
								<div class="form-group">
									<?=form_label('Mensaje','cuerpo')?>
									<textarea name="cuerpo" rows="10" id="cuerpo" required="">
										<?=(set_value('cuerpo')!='' ? set_value('cuerpo') : '')?>
									</textarea>
								</div>
								¿Contiene el mensaje un imagen con función de mapa.?<button type="button" id="ponerModal" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-info">Presione aquí</button>
							</div>
							<div class="panel-footer">
								<button type="submit" class="btn btn-success" id="enviar">Guardar</button>
							</div>
							<input type="hidden" name="modal" id="modal" value="0">
							<?=form_close()?>
						</div>
					</div>
				</div>
				
			</div>
			<!-- /.blog-post -->
		</div>

		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		  <div class="modal-dialog " role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">¿Contiene el mensaje un imagen con función de mapa.?</h4>
		      </div>
		      <div class="modal-body">
		        <strong>¿Que quiere decir esto?</strong><br>
		        <p>Si esta incluyendo en el mensaje del articulo una imagen sobre la cual los lectores puedan hacer clic y cargar una nueva ventana, por ejemplo, para mostrar la información de una coordinación, presione el botón sí la función es parecida a la mencionada y no para mantener el articulo como esta.</p>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" id="quitarModal" data-dismiss="modal">No</button>
		        <button type="button" class="btn btn-primary" id="ponerModal" data-dismiss="modal">Si</button>
		      </div>
		    </div>
		  </div>
		</div>