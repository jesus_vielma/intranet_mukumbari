
<!--<div class="row">-->

		<div class="col-sm-8 blog-main">

			<div class="blog-post">
				<h3 class="blog-post-title">Generar nuevo ticket</h3>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row top-ticket-detail">
									<div class="col-lg-5">
										<ul class="list-unstyled">
											<li>Creado: <?=strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($tickets[0]['fc']))?></li>
											<li>Ultima actualizacion: <?=(isset($tickets[0]['ultima']['0']->fecha)) ? strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($tickets[0]['ultima']['0']->fecha)) : 'Sin actualizaciones'?></li>
										</ul>
									</div>
									<div class="col-lg-4 top-info">
										<ul class="list-unstyled">
											<li>Ticket No: <?=$tickets[0]['id_ticket']?></li>
											<li>Prioridad: <span class="prioridad" style="background-color: <?=$tickets[0]['color']?>"><?=$tickets[0]['nombre_prioridad']?></span></li>
										</ul>	
									</div>	
									<div class="col-lg-3 top-info">
										<ul class="list-unstyled">
											<li>Estado: <?=$tickets[0]['estado']?></li>
											<?php if($tickets[0]['estado'] !='Cerrado'):?>
												<?php if($tickets[0]['usuario'][0]->id_usuario == $this->session->userdata('front')['id_usuario'] ):?>
													<li><a href="<?=site_url('ticket/cambiar_estado/'.$tickets[0]['id_ticket'].'/Cerrado')?>" class="btn btn-danger">Cerrar ticket</a></li>
												<?php endif;?>
											<?php else:?>
													<li><a href="<?=site_url('ticket/cambiar_estado/'.$tickets[0]['id_ticket'].'/Abierto')?>" class="btn btn-success">Re abrir</a></li>
											<?php endif;?>
										</ul>
									</div>
								</div>
								<div class="info-ticket">
									<div class="row">
										<div class="col-lg-12">
											<strong>Asunto:</strong> <span class="text-primary"><?=$tickets[0]['asunto']?></span>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<strong>Mensaje: </strong> <div class="mensaje">
												<?=$tickets[0]['cuerpo']?>
											</div>									
											<div class="row">
												<div class="col-lg-12">
													<strong>Coordinaciones involucradas:</strong>
													<?php if($tickets[0]['dptos']!='0') :?>
														<ul>
															<?php foreach($tickets[0]['dptos'] as $dpto):?>
																<li><?=$dpto->dpto?></li>
															<?php endforeach;?>
														</ul>
													<?php else:?>
														Involucrar coordinaciones
													<?php endif;?>
												</div>
											</div>
										</div>
									</div>
								<div class="info-solicitante">
									<div class="row">
										<div class="col-lg-4 solicitante">
											<strong>Solicitante</strong>
											<br>
											<?=$tickets[0]['usuario'][0]->generador?>
										</div>
										<div class="col-lg-4 contacto-solicitante">
											<strong>Contacto</strong><br>
											<?=$tickets[0]['usuario'][0]->telefono?>
											<br>
											<?=$tickets[0]['usuario'][0]->correo?>
										</div>
										<div class="col-lg-4 departamento-solicitante">
											<strong>Coordinación</strong><br>
											<?=$tickets[0]['usuario'][0]->dep?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-primary no-border">
											<div class="panel-heading">
												<h3 class="panel-title">Hilo de respuestas</h3>
											</div>
											<div class="panel-body">
											<?php if($respuestas!='0'):?>
											<?php foreach($respuestas as $respuesta):?>
												<?php if($respuesta->dep !=$tickets[0]['usuario'][0]->dep){
													$left = "<div class='media-left text-primary'> 
													<i class='fa fa-send fa-3x'></i>
													</div> ";
													$right='';
													}
													else{
														$right = "<div class='media-right text-success'> 
															<i class='fa fa-send fa-3x fa-flip-horizontal'></i>
															</div> ";
															$left='';
													}

												?>
												<div class="media"> 
												<!--<div class="media-left text-primary"> 
													<i class="fa fa-send fa-3x"></i>
												</div> -->
												<?=(isset($left) ? $left : '')?>
													<div class="media-body hilo"> 
														<h4 class="media-heading">Por: <?=$respuesta->usuario?> <small><?=strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($respuesta->fecha)) ?></small></h4> 
														<p><?=$respuesta->mensaje?></p>
														<h5><small>Coordinación <?=$respuesta->dep?></small></h5>
													</div>
												<?=(isset($right) ? $right : '')?>
												</div>
											<?php endforeach;?>
											<?php else:?>
												No hay respuestas aun
											<?php endif;?>
											</div>
										</div>
									</div>
								</div>
								<?php if($tickets[0]['estado'] !='Cerrado'):?>
								<?=form_open('ticket/respuesta',array('id'=>'formm'))?>
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-success no-border">
											<div class="panel-heading">
												<h3 class="panel-title">Publicar respuesta</h3>
											</div>
											<div class="panel-body">
											<?=validation_errors()?>
												<div class="form-group">
													<textarea name="cuerpo" rows="10" id="cuerpo" class="form-control" >
														<?=(set_value('cuerpo')!='' ? set_value('cuerpo') : '')?>
													</textarea>
												</div>
												<?=form_hidden('id_ticket',$tickets[0]['id_ticket'])?>
											</div>
											<div class="panel-footer">
												<!--<label class="radio-inline">
												  <input type="radio" name="accionRespuesta" value="responder"> Responder
												</label>
												<label class="radio-inline">
												  <input type="radio" name="accionRespuesta" value="responderysalir"> Responder y salir
												</label>-->
												<button type="submit" class="btn btn-success pull-right">Publicar</button>
												<br><br>
											</div>
										</div>
									</div>
								</div>
								<?=form_close()?>
								<?php else:?>
									<div class="alert alert-info">
										<strong>Ticket cerrado</strong> <br>
										Este ticket esta cerrado, no puedes responder a el a menos que sea re abierto por la persona que lo generó.
									</div>
								<?php endif;?>
							</div>	
						</div>
					</div>
				</div>
				</div>
			</div>
			<!-- /.blog-post -->
		</div>