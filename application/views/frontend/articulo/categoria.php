	
		<div class="col-sm-8 blog-main">
		<div class="row">
			<div class="col-lg-12">
				<h4 class="text-info">Articulos en las categoria: <?=$cat?></h4>
			</div>
		</div>
			<?php if($articulos!='0'):?>
				<?php foreach ($articulos as $articulo) :?>
				<div class="blog-post">
					<h2 class="blog-post-title"><?=$articulo->titulo?></h2>
					<p class="blog-post-meta"><?=strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($articulo->fecha)) ?> <strong>por</strong> <a href="#"><?=$articulo->autor?></a>
					<?php if($articulo->autor == $this->session->userdata('front')['nombre']):?>
						<span class="pull-right"><a href="<?=site_url('articulo/editar/1/'.$articulo->alias)?>" title="Editar articulo"><i class="fa fa-pencil"></i></a></span>
					<?php endif;?>
						</p>
						<?php $link = "<a href='".site_url('articulo/'.$articulo->alias)."'> Leer mas</a>"?>
					<?=word_limiter($articulo->cuerpo,'160',$link)?>

				</div>
				<!-- /.blog-post -->
				<?php endforeach;?>
				<nav>
					<ul class="pagination">
						<?=$pagination?>
					</ul>
				</nav>
			<?php else:?>
				<div class="alert alert-info">
					<strong>¡Oops algo ha salido mal!</strong> <br>
					No hemos encontrado articulos registrados en la base de datos recarga la pagina para comprobar que el error persiste.
				</div>
			<?php endif;?>

				

		</div>
		<!-- /.blog-main -->

		