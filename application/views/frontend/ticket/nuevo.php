<?php 
$opcionesPrioridad = [''=>'- Seleccione -'];
if($prioridades>0){
	foreach ($prioridades as $prioridad ) {
		$opcionesPrioridad[$prioridad->id_prioridad] = $prioridad->nombre;
	}
}
if ($departamentos>0) {
	foreach ($departamentos as $departamento) {
		if($departamento->predeterminado == 1){
			$pre = $departamento->id_departamento;
		}
		$opcionesDpto[$departamento->id_departamento] = $departamento->nombre;
	}
$selected = [$pre,$dep_user[0]->id_departamento];
}
?>
<!-- <div class="row"> -->

		<div class="col-sm-8 blog-main">

			<div class="blog-post">
				<h3 class="blog-post-title">Generar nuevo ticket</h3>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Generar un nuevo ticket</h3>
							</div>
							<div class="panel-body">
								<?=form_open('ticket/nuevo/2',array('id'=>'form'))?>
								<?=validation_errors()?>
								<div class="form-group">
									<?=form_label('Asunto','asunto')?>
									<?=form_input(array('name'=>'asunto','id'=>'asunto','required'=>'required','class'=>'form-control','value'=>set_value('asunto')))?>
								</div>
								<div class="form-group">
									<?=form_label('Mensaje','cuerpo')?>
									<textarea name="cuerpo" rows="10" id="cuerpo">
										<?=(set_value('cuerpo')!='' ? set_value('cuerpo') : '')?>
									</textarea>
								</div>
								<div class="form-group">
									<?=form_label('Prioridad','prioridad')?>
									<?=form_dropdown('prioridad',$opcionesPrioridad,set_value('prioridad'),'class="form-control" id="prioridad" required')?>
								</div>
								<div class="form-group">
									<?=form_label('Coordinaciones','departamentos')?>
									<?=form_multiselect('departamentos[]',$opcionesDpto,$selected,'class="form-control" id="multi-select" required')?>
								</div>
							</div>
							<div class="panel-footer">
								<button type="submit" class="btn btn-success" id="enviar">Generar</button>
							</div>
							<?=form_close()?>
						</div>
					</div>
				</div>
				
			</div>
			<!-- /.blog-post -->
		</div>