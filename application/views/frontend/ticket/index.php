<!-- <div class="row"> -->

		<div class="col-sm-8 blog-main">

			<div class="blog-post">
				<h3 class="blog-post-title">Mis tickets</h3>
				<!-- <div class="row text-center">
					<div class="col-lg-12">
						<button type="button" class="btn btn-default <?=($boton_activo)== 'departamento' ? 'active': ''?>">Mi departamento</button>
						<a href="<?=site_url('mis_tickets/por_mi')?>" class="btn btn-default">Generados por mi</a>
					</div>
				</div> -->
				<br>
				<div class="row">
					<div class="col-lg-12">
						<?php if($tickets !='0'):?>
							<?php foreach ($tickets as $ticket => $dptos) :?>
								<div class="row">
									<div class="col-sm-12">
										<div class="panel panel-default" style="border-color: <?=$dptos['color'];?>">
											<div class="panel-heading" style="background-color: <?=$dptos['color']?>; color:white">
												<h3 class="panel-title pull-left">
													<strong>Asunto:</strong> <?=$dptos['asunto']?>
												</h3>
												<h3 class="panel-title pull-right">									
													<strong>Ticket No.</strong> <?=$dptos['id_ticket']?>
												</h3>
												<div class="clearfix"></div>
											</div>
											<div class="panel-body">
												<div class="row">
													<div class="col-sm-8">
														<ul class="list-unstyled">
															<li><strong>Generado por: </strong> <?=$dptos['usuario']?></li>
															<li><strong>De la coordinación: </strong> <?=$dptos['depa']?></li>
															<li><strong>Para las coordinaciones:</strong> 
															<?php if($dptos['dptos'] !='0') :?>
																<ul >
																	<?php foreach($dptos['dptos'] as $dpto):?>
																		<li><?=$dpto->dpto?></li>
																	<?php endforeach;?>
																</ul>
																
															<?php else:?>
																NO hay coordinaciones asociadas.
															<?php endif;?>
															</li>
														</ul>
													</div>
													<div class="col-sm-4">
														<ul class="list-unstyled">
															<li><strong>Ultima actualización:</strong> 
																<?php 

																	if($dptos['ultima']['0']>'0')
																		echo strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($dptos['ultima']['0']->fecha));
																	else{
																		echo strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($dptos['fc']));
																	}
																?>
																</li>
															<li><strong>Prioridad: </strong> <?=$dptos['nombre_prioridad']?></li>
															<li><strong>Estado:</strong> <?=$dptos['estado']?></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="panel-footer">
												<a href="<?=site_url('ticket/detalle/'.$dptos['id_ticket'])?>">Ver ticket completo</a>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach;?>
						<nav>
							<ul class="pagination">
								<?=$pagination?>
							</ul>
						</nav>
						<?php else:?>

						<div class="row">
							<div class="col-lg-12">
								<div class="alert alert-info">
									<h3>No tienes tickets registrados</h3>
									No pudimos encontrar ticket registrados para ti <?=$this->session->userdata('front')['nombre']?>
								</div>
							</div>
						</div>
						<?php endif;?>
					</div>
				</div>
				
			</div>
			<!-- /.blog-post -->
		</div>