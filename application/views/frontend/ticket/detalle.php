
<!--<div class="row">-->

		<div class="col-sm-8 blog-main">

			<div class="blog-post">
				<h3 class="blog-post-title">Detalle del ticket</h3>
				<div class="printHeader visible-xs-block" style="display: none">
					<img src="<?=base_url('assets/frontend/img/memo.png')?>" class="img-resposive">
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row top-ticket-detail">
									<div class="col-lg-5 col-xs-5">
										<ul class="list-unstyled">
											<li><h4>Creado: <small><?=strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($tickets[0]['fc']))?></small></h4></li>
											<li><h4 class="hidden-xs">Ultima actualización: <small><?=(isset($tickets[0]['ultima']['0']->fecha)) ? strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($tickets[0]['ultima']['0']->fecha)) : 'Sin actualizaciones'?></small></h4></li>
										</ul>
									</div>
									<div class="col-lg-4 col-xs-4 top-info hidden-xs">
										<ul class="list-unstyled">
											<li><h4>Ticket No: <small><?=$tickets[0]['id_ticket']?></small></h4> </li>
											<li><h4>Prioridad: <small class="prioridad" style="background-color: <?=$tickets[0]['color']?>"><?=$tickets[0]['nombre_prioridad']?></small></h4></li>
										</ul>	
									</div>	
									<div class="col-lg-3 col-xs-3 top-info hidden-xs">
										<ul class="list-unstyled">
											<li><h4>Estado: <small><?=$tickets[0]['estado']?></small></h4></li>
											<?php if($tickets[0]['estado'] !='Cerrado'):?>
												<?php if($tickets[0]['usuario'][0]->id_usuario == $this->session->userdata('front')['id_usuario'] ):?>
													<li><a href="<?=site_url('ticket/cambiar_estado/'.$tickets[0]['id_ticket'].'/Cerrado')?>" class="btn btn-danger">Cerrar ticket</a></li>
												<?php endif;?>
											<?php else:?>
													<li><a href="<?=site_url('ticket/cambiar_estado/'.$tickets[0]['id_ticket'].'/Abierto')?>" class="btn btn-success">Re abrir</a></li>
											<?php endif;?>
											<button class="btn btn-default" id="printTicket"><i class="fa fa-print"></i></button>
										</ul>
									</div>
								</div>
								<div class="info-ticket">
									<div class="row">
										<div class="col-lg-12">
											<h4>Asunto: <small><?=$tickets[0]['asunto']?></small></h4> 
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<h4>Mensaje: </h4> 
											<blockquote>
												<?=$tickets[0]['cuerpo']?>
											</blockquote>
												
										</div>
									</div>
									<div class="row" id="coordInv">
										<div class="col-lg-12">
											<h4>Coordinaciones involucradas:</h4>
											<?php if($tickets[0]['dptos']!='0') :?>
												<ul>
													<?php foreach($tickets[0]['dptos'] as $dpto):?>
														<li><?=$dpto->dpto?></li>
													<?php endforeach;?>
												</ul>
											<?php else:?>
												Involucrar coordinaciones
											<?php endif;?>
										</div>
									</div>
								<div class="info-solicitante">
									<div class="row">
										<div class="col-lg-4 col-xs-4 solicitante">
											<strong>Solicitante</strong>
											<br>
											<?=$tickets[0]['usuario'][0]->generador?>
										</div>
										<div class="col-lg-4 col-xs-4 contacto-solicitante">
											<strong>Contacto</strong><br>
											<?=$tickets[0]['usuario'][0]->telefono?>
											<br>
											<?=$tickets[0]['usuario'][0]->correo?>
										</div>
										<div class="col-lg-4 col-xs-4 departamento-solicitante">
											<strong>Coordinación</strong><br>
											<?=$tickets[0]['usuario'][0]->dep?>
										</div>
									</div>
								</div>
								<div class="row" id="respuestas">
									<div class="col-lg-12">
										<div class="panel panel-primary no-border">
											<div class="panel-heading">
												<h3 class="panel-title">Hilo de respuestas</h3>
											</div>
											<div class="panel-body">
											<?php if($respuestas!='0'):?>
											<?php foreach($respuestas as $respuesta):?>
												<?php if($respuesta->dep !=$tickets[0]['usuario'][0]->dep){
													$left = "<div class='media-left text-primary'> 
													<i class='fa fa-send fa-3x'></i>
													</div> ";
													$right='';
													}
													else{
														$right = "<div class='media-right text-success'> 
															<i class='fa fa-send fa-3x fa-flip-horizontal'></i>
															</div> ";
															$left='';
													}

												?>
												<div class="media"> 
												<!--<div class="media-left text-primary"> 
													<i class="fa fa-send fa-3x"></i>
												</div> -->
												<?=(isset($left) ? $left : '')?>
													<div class="media-body hilo"> 
														<h4 class="media-heading">Por: <?=$respuesta->usuario?> <small><?=strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($respuesta->fecha)) ?></small></h4> 
														<p><?=$respuesta->mensaje?></p>
														<h5><small>Coordinación <?=$respuesta->dep?></small></h5>
													</div>
												<?=(isset($right) ? $right : '')?>
												</div>
											<?php endforeach;?>
											<?php else:?>
												No hay respuestas aun
											<?php endif;?>
											</div>
										</div>
									</div>
								</div>
								<?php if($tickets[0]['estado'] !='Cerrado'):?>
								<div class="row" id="form-resp">
								<?=form_open('ticket/respuesta',array('id'=>'formm'))?>
									<div class="col-lg-12">
										<div class="panel panel-success no-border">
											<div class="panel-heading">
												<h3 class="panel-title">Publicar respuesta</h3>
											</div>
											<div class="panel-body">
											<?=validation_errors()?>
												<div class="form-group">
													<textarea name="cuerpo" rows="10" id="cuerpo" class="form-control" >
														<?=(set_value('cuerpo')!='' ? set_value('cuerpo') : '')?>
													</textarea>
												</div>
												<?=form_hidden('id_ticket',$tickets[0]['id_ticket'])?>
											</div>
											<div class="panel-footer">
												<!--<label class="radio-inline">
												  <input type="radio" name="accionRespuesta" value="responder"> Responder
												</label>
												<label class="radio-inline">
												  <input type="radio" name="accionRespuesta" value="responderysalir"> Responder y salir
												</label>-->
												<button type="submit" class="btn btn-success pull-right">Publicar</button>
												<br><br>
											</div>
										</div>
									</div>
									<?=form_close()?>
								</div>
								<?php else:?>
									<div class="alert alert-info">
										<strong>Ticket cerrado</strong> <br>
										Este ticket esta cerrado, no puedes responder a el a menos que sea re abierto por la persona que lo generó.
									</div>
								<?php endif;?>
								<br>
								<div class="row visible-xs-block">
									<div class="col-xs-6">
										<h4>Impreso el: <small id="fechaPrint"></small></h4>
									</div>
									<div class="col-xs-6">
										<h4>Impreso por: <small><?=$this->session->userdata('front')['nombre']?></small></h4>
										<br><br><br><br><br><br>
										<h4>Firma y Sello </h4>
									</div>
								</div>
							</div>	
						</div>
					</div>
				</div>
				</div>
				<div class="printFooter visible-xs-block" style="display: none">
					<img src="<?=base_url('assets/frontend/img/zamora-memo.png')?>" class="img-resposive">
				</div>
			</div>
			<!-- /.blog-post -->
		</div>