	
		<div class="col-sm-8 blog-main">
			<?php if($this->session->userdata('front')['config'] < 3 ):?>
			<div class="alert alert-warning" id="config">
					<h4>¡Su perfil no esta configurado!</h4>
					<p>Estas son las opciones en su perfil que aún no están configuradas</p>
					<ul>
						<?php if($fila[0]->config == '0'):?>
						<li>Debe actualizar la foto de perfil</li>
						<li>Debe cambiar su clave</li>
						<?php elseif($fila[0]->config == '1'):?>
						<li>Debe actualizar la foto de perfil</li>
						<?php elseif($fila[0]->config == '2'):?>
						<li>Debe cambiar su clave</li>
						<?php endif;?>
					</ul>
			</div>
			<?php endif;?>
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Mi perfil</h3>
				</div>
				<div class="panel-body">
					<?=form_open('perfil/actualizar/',array('id'=>'form'))?>
					<?=validation_errors()?>
						<div class="row">
							<div class="col-lg-3">
							<?php if($fila[0]->foto!='') $foto = $fila[0]->foto; else $foto ='perfil.jpg';?>
								<img src="<?=base_url('assets/uploads/perfiles/'.$foto)?>" alt="" class="img-responsive img-circle" id="foto_img">
								<input type="hidden" id="foto" name="foto_nueva" >
								<input type="hidden" id="foto" name="foto_actual" value="<?=$foto?>">
								<a data-src="<?=base_url('assets/frontend/plugins/filemanager-alone/dialog.php?type=1&field_id=foto&fldr='.$fila[0]->usuario.'&lang=es&relative_url=1')?>" data-fancybox href="javascript:;" class="btn btn-default">Subir o cambiar foto</a>
							</div>
							<div class="col-lg-9">
								<h3>Información Personal</h3>
								<div class="form-group">
									<?=form_label('Nombre', 'nombre')?>
									<?=form_input(array('name'=>'nombre','id'=>'nombre','class'=>'form-control','value'=>$fila[0]->nombre,'required'=>'required'))?>
								</div>
								<div class="form-group">
									<?=form_label('Correo', 'correo')?>
									<?=form_input(array('name'=>'correo','id'=>'correo','class'=>'form-control','value'=>$fila[0]->correo,'required'=>'required'))?>
								</div>
								<div class="form-group">
									<?=form_label('Teléfono', 'telefono')?>
									<?=form_input(array('name'=>'telefono','id'=>'telefono','class'=>'form-control','value'=>$fila[0]->telefono,'required'=>'required'))?>
								</div>
								<hr>
								<h3>Información del usuario</h3>
								<div class="form-group">
									<?=form_label('Usuario', 'usuario')?>
									<?=form_input(array('name'=>'usuario','id'=>'usuario','class'=>'form-control','value'=>$fila[0]->usuario,'readonly'=>'readolny'))?>
								</div>
								<div class="form-group" id="clave">
									<?=form_label('Nueva clave', 'clave_nueva')?>
									<?=form_password(array('name'=>'clave_nueva','id'=>'clave_nueva','class'=>'form-control','minlength'=>'6'))?>
								</div>
								<div class="form-group">
									<?=form_label('Repita su nueva clave', 'repetir_clave')?>
									<?=form_password(array('name'=>'repetir_clave','id'=>'repetir_clave','class'=>'form-control','minlength'=>'6','data-parsley-equalto'=>'#clave_nueva'))?>
								</div>
								<?=form_hidden('id_usuario', $fila[0]->id_usuario)?>
								<?=form_hidden('conf_actual', $fila[0]->config)?>
								<div class="form-group">
								<?=form_label('Ingrese su clave actual', 'clave_actual')?>
								<?=form_password(array('name'=>'clave_actual','id'=>'clave_actual','class'=>'form-control','required'=>'required'))?>
								</div>
							</div>
						</div>
				</div>
				<div class="panel-footer">
					<button type="submit" class="btn btn-success" id="enviar">Guardar</button>
					<?=form_close()?>
				</div>
			</div>
		</div>
		<!-- /.blog-main -->

		