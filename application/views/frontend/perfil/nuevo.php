<!-- <div class="row"> -->

		<div class="col-sm-8 blog-main">

			<div class="blog-post">
				<h3 class="blog-post-title">Nuevo evento</h3>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Nuevo evento interno</h3>
							</div>
							<div class="panel-body">
								<?=form_open('evento/nuevo/2',array('id'=>'form'))?>
								<?=validation_errors()?>
								<div class="form-group">
									<?=form_label('Titulo','titulo')?>
									<?=form_input(array('name'=>'titulo','id'=>'titulo','required'=>'required','class'=>'form-control','value'=>set_value('titulo'),'maxlength'=>'150','minlength'=>'30'))?>
								</div>
								<div class="form-group ">
									<div class="col-sm-6">
										<label>Inico</label>
										<div class="input-group date inicio" id="dtp-3">
											<input type="text" class="form-control">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											<span class="input-group-addon"><i class="fa fa-times"></i></span>
										</div>
										<input type="hidden" name="inicio" id="inicio">
									</div>
									<div class="col-sm-6">
										<label >Meridian Format</label>
										<div class="input-group date fin" id="dtp-5">
											<input type="text" class="form-control">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											<span class="input-group-addon"><i class="fa fa-times"></i></span>
										</div>
										<input type="hidden" name="fin" id="fin">
									</div>
								</div>
								<div class="form-group">
									<?=form_label('Tipo','tipo')?>
									<?=form_dropdown('tipo',array('evento'=>'Evento','cumpleanos'=>'Cumpleaños'),(set_value('tipo')!='' ? set_value('tipo') : ''),'class="form-control" id="tipo" required')?>
								</div>
								<div class="form-group">
									<?=form_label('Descripción','descripcion')?>
									<textarea name="descripcion" rows="10" id="descripcion" required="">
										<?=(set_value('descripcion')!='' ? set_value('descripcion') : '')?>
									</textarea>
								</div>
							</div>
							<div class="panel-footer">
								<button type="submit" class="btn btn-success" id="enviar">Guardar</button>
							</div>
							<?=form_close()?>
						</div>
					</div>
				</div>
				
			</div>
			<!-- /.blog-post -->
		</div>