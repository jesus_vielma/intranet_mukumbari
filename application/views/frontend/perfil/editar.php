<?php 
$categoriasS = [''=>'- Seleccione -'];
if($categorias>'0'){
	foreach ($categorias as $categoria ) {
		$categoriasS[$categoria->id_categoria] = $categoria->nombre;
	}	
}

/*if ($departamentos>0) {
	foreach ($departamentos as $departamento) {
		if($departamento->predeterminado == 1){
			$selected = $departamento->id_departamento;
		}
		$opcionesDpto[$departamento->id_departamento] = $departamento->nombre;
	}
}*/
?>
<!-- <div class="row"> -->

		<div class="col-sm-8 blog-main">

			<div class="blog-post">
				<h3 class="blog-post-title">Nuevo articulo</h3>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Editar el articulo</h3>
							</div>
							<div class="panel-body">
								<?=form_open('articulo/editar/2/'.$articulo[0]->id_articulo,array('id'=>'form'))?>
								<?=validation_errors()?>
								<div class="form-group">
									<?=form_label('Titulo','titulo')?>
									<?=form_input(array('name'=>'titulo','id'=>'titulo','required'=>'required','class'=>'form-control','value'=>(set_value('titulo')!='' ? set_value('titulo') : $articulo[0]->titulo),'maxlength'=>'150','minlength'=>'30'))?>
								</div>
								<div class="form-group">
									<?=form_label('Alias','alias')?>
									<?=form_input(array('name'=>'alias','id'=>'alias','class'=>'form-control','value'=>(set_value('alias')!='' ? set_value('alias') : $articulo[0]->alias)))?>
								</div>
								<div class="form-group">
									<?=form_label('Categoría','categoria')?>
									<?=form_dropdown('categoria',$categoriasS,(set_value('categoria')!='' ? set_value('categoria') : $articulo[0]->id_categoria),'class="form-control" id="categoria" required')?>
								</div>
								<div class="form-group">
									<?=form_label('Mensaje','cuerpo')?>
									<textarea name="cuerpo" rows="10" id="cuerpo" required="">
										<?=(set_value('cuerpo')!='' ? set_value('cuerpo') : $articulo[0]->cuerpo)?>
									</textarea>
								</div>
							</div>
							<div class="panel-footer">
								<?=form_hidden('id_articulo',$articulo[0]->id_articulo)?>
								<?=form_hidden('fecha',$articulo[0]->fecha)?>
								<button type="submit" class="btn btn-success" id="enviar">Guardar</button>
							</div>
							<?=form_close()?>
						</div>
					</div>
				</div>
				
			</div>
			<!-- /.blog-post -->
		</div>