<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!-- <ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol> -->
			<div class="page-heading">
				<h1>Categorías para los artículos</h1>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2>Editar categoría</h2>
								<div class="panel-ctrls">
								</div>
							</div>
							<div class="panel-body ">
								<?=form_open('admin/categoria/editar/2/'.$fila[0]->id_categoria,array('id' =>'validate-form'))?>
								<?=validation_errors()?>
								<?php if (isset($error)):?>
									<div class="alert alert-danger">
										<?=$error?>
									</div>
								<?php endif; ?>
								<div class="form-group">
									<?=form_label('Nombre','nombre')?>
									<?=form_input(array('name' => 'nombre','id'=>'nombre','class'=>'form-control','required'=>'required','value'=>(set_value('nombre'))!='' ? set_value('nombre') : $fila[0]->nombre) )?>
								</div>
								<div class="form-group">
									<?=form_label('Alias','alias')?>
									<?=form_input(array('name'=>'alias','id'=>'alias','class'=>'form-control','required'=>'required','value'=>(set_value('alias'))!='' ? set_value('alias') : $fila[0]->alias) )?>
								</div>
								<div class="form-group">
									<label>Predeterminada</label>
									<div class="btn-group mb10 btn-group-justified" data-toggle="buttons">
                            			<label class="btn btn-orange <?=($fila[0]->default)=='1' ? 'active' : ''?>">
                                			<input type="radio" name="default" value="1" <?=($fila[0]->default)=='1' ? 'checked' : ''?> > Si
                            			</label>
			                            <label class="btn btn-purple <?=($fila[0]->default)=='0' ? 'active' : ''?>">
			                                <input type="radio" name="default" value="0" <?=($fila[0]->default)=='0' ? 'checked' : ''?> > No
			                            </label>
                        			</div>
								</div>
					            <?=form_hidden('id_categoria',$fila[0]->id_categoria)?>
								<div class="panel-footer text-center">
									<button type="submit" class="btn btn-success btn-lg" id="submit">Guardar</button>
								</div>
								<?=form_close()?>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
