<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title><?=$titulo?> | Aministrador Intranet</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">

	<link href="<?=base_url('assets/backend/fonts/fonts.googleapis.com/css%3ffamily=RobotoDraft:300,400,400italic,500,700')?>" rel='stylesheet' type='text/css'>
	<link href='<?=base_url('assets/backend/fonts/fonts.googleapis.com/css%3ffamily=Open+Sans:300,400,400italic,600,700')?>' rel='stylesheet' type='text/css'>

	<!--[if lt IE 10]>
        <script src="assets/backend/js/media.match.min.js"></script>
        <script src="assets/backend/js/placeholder.min.js"></script>
    <![endif]-->

	<link href="<?=base_url('assets/backend/fonts/font-awesome/css/font-awesome.min.css')?>" type="text/css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?=base_url('assets/backend/css/styles.css')?>" type="text/css" rel="stylesheet">
	<!-- Core CSS with all styles -->

	<link href="<?=base_url('assets/backend/plugins/jstree/dist/themes/avenger/style.min.css')?>" type="text/css" rel="stylesheet">
	<!-- jsTree -->
	<link href="<?=base_url('assets/backend/plugins/codeprettifier/prettify.css')?>" type="text/css" rel="stylesheet">
	<!-- Code Prettifier -->
	<link href="<?=base_url('assets/backend/plugins/iCheck/skins/minimal/_all.css')?>" type="text/css" rel="stylesheet">
	<!-- iCheck -->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
	<!--[if lt IE 9]>
        <link href="assets/backend/css/ie8.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
        <script type="text/javascript" src="assets/backend/plugins/charts-flot/excanvas.min.js"></script>
        <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

	<?php if($controlador=='listar'):?>
	<link href="<?=base_url('assets/backend/plugins/datatables/dataTables.bootstrap.css')?>" type="text/css" rel="stylesheet">
	<link href="<?=base_url('assets/backend/plugins/datatables/dataTables.fontAwesome.css')?>" type="text/css" rel="stylesheet">
	<?php elseif ($controlador=='menu') :?>
	<link href="<?=base_url('assets/backend/plugins/form-nestable/jquery.nestable.css')?>" type="text/css" rel="stylesheet">
	<?php elseif ($controlador == 'detalle-dep') :?>
	<link href="<?=base_url('assets/backend/plugins/form-multiselect/css/multi-select.css')?>" type="text/css" rel="stylesheet">
	<?php endif; ?>

</head>

<body class="infobar-offcanvas">

	<div id="headerbar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-6 col-sm-2">
					<a href="index.html#" class="shortcut-tile tile-brown">
						<div class="tile-body">
							<div class="pull-left"><i class="fa fa-pencil"></i></div>
						</div>
						<div class="tile-footer">
							Create Post
						</div>
					</a>
				</div>
				<div class="col-xs-6 col-sm-2">
					<a href="index.html#" class="shortcut-tile tile-grape">
						<div class="tile-body">
							<div class="pull-left"><i class="fa fa-group"></i></div>
							<div class="pull-right"><span class="badge">2</span></div>
						</div>
						<div class="tile-footer">
							Contacts
						</div>
					</a>
				</div>
				<div class="col-xs-6 col-sm-2">
					<a href="index.html#" class="shortcut-tile tile-primary">
						<div class="tile-body">
							<div class="pull-left"><i class="fa fa-envelope-o"></i></div>
							<div class="pull-right"><span class="badge">10</span></div>
						</div>
						<div class="tile-footer">
							Messages
						</div>
					</a>
				</div>
				<div class="col-xs-6 col-sm-2">
					<a href="index.html#" class="shortcut-tile tile-inverse">
						<div class="tile-body">
							<div class="pull-left"><i class="fa fa-camera"></i></div>
							<div class="pull-right"><span class="badge">3</span></div>
						</div>
						<div class="tile-footer">
							Gallery
						</div>
					</a>
				</div>

				<div class="col-xs-6 col-sm-2">
					<a href="index.html#" class="shortcut-tile tile-midnightblue">
						<div class="tile-body">
							<div class="pull-left"><i class="fa fa-cog"></i></div>
						</div>
						<div class="tile-footer">
							Settings
						</div>
					</a>
				</div>
				<div class="col-xs-6 col-sm-2">
					<a href="index.html#" class="shortcut-tile tile-orange">
						<div class="tile-body">
							<div class="pull-left"><i class="fa fa-wrench"></i></div>
						</div>
						<div class="tile-footer">
							Plugins
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
	<header id="topnav" class="navbar navbar-midnightblue navbar-fixed-top clearfix" role="banner">

		<span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
		<a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar"><span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a>
		</span>

		<a class="navbar-brand" href="index.html">Administrador Intranet</a>

		<!--<span id="trigger-infobar" class="toolbar-trigger toolbar-icon-bg">
		<a data-toggle="tooltips" data-placement="left" title="Toggle Infobar"><span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a>
		</span>-->


		<ul class="nav navbar-nav toolbar pull-right">
			<li class="dropdown toolbar-icon-bg">
				<a href="index.html#" id="navbar-links-toggle" data-toggle="collapse" data-target="header>.navbar-collapse">
					<span class="icon-bg">
					<i class="fa fa-fw fa-ellipsis-h"></i>
				</span>
				</a>
			</li>

			<!-- <li class="toolbar-icon-bg demo-headerdrop-hidden">
				<a href="index.html#" id="headerbardropdown"><span class="icon-bg"><i class="fa fa-fw fa-level-down"></i></span></i></a>
			</li>

			<li class="toolbar-icon-bg hidden-xs" id="trigger-fullscreen">
				<a href="#" class="toggle-fullscreen"><span class="icon-bg"><i class="fa fa-fw fa-arrows-alt"></i></span></i></a>
			</li> -->

			<?php if($this->session->userdata('usuario')['config']<3) :?>
			<li class="dropdown toolbar-icon-bg">
				<a href="index.html#" class="hasnotifications dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="fa fa-fw fa-bell"></i></span><span class="badge badge-info">1</span></a>
				<div class="dropdown-menu dropdown-alternate notifications arrow">
					<div class="dd-header">
						<span>Notificaciones</span>
					</div>
					<div class="scrollthis scroll-pane">
						<ul class="scroll-content">
							<li class="">
								<a href="<?=site_url('admin/usuario/perfil/'.$this->session->userdata('usuario')['id_usuario'])?>" class="notification-warning">
									<div class="notification-icon"><i class="fa fa-user fa-fw"></i></div>
									<div class="notification-content">No haz cambiado tu contraseña</div>
									<div class="notification-time"></div>
								</a>
							</li>
						</ul>
					</div>
					<div class="dd-footer">
						<!--<a href="index.html#">View all notifications</a>-->
					</div>
				</div>
			</li>
			<?php endif;?>
			<!-- <li class="dropdown toolbar-icon-bg hidden-xs">
				<a href="index.html#" class="hasnotifications dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="fa fa-fw fa-envelope"></i></span></a>
				<div class="dropdown-menu dropdown-alternate messages arrow">
					<div class="dd-header">
						<span>Messages</span>
						<span><a href="index.html#">Settings</a></span>
					</div>

					<div class="scrollthis scroll-pane">
						<ul class="scroll-content">
							<li class="">
								<a href="index.html#">
									<img class="msg-avatar" src="assets/backend/demo/avatar/avatar_09.png" alt="avatar" />
									<div class="msg-content">
										<span class="name">Steven Shipe</span>
										<span class="msg">Nonummy nibh epismod lorem ipsum</span>
									</div>
									<span class="msg-time">30s</span>
								</a>
							</li>
							<li>
								<a href="index.html#">
									<img class="msg-avatar" src="assets/backend/demo/avatar/avatar_01.png" alt="avatar" />
									<div class="msg-content">
										<span class="name">Roxann Hollingworth <i class="fa fa-paperclip attachment"></i></span>
										<span class="msg">Lorem ipsum dolor sit amet consectetur adipisicing elit</span>
									</div>
									<span class="msg-time">5m</span>
								</a>
							</li>
							<li>
								<a href="index.html#">
									<img class="msg-avatar" src="assets/backend/demo/avatar/avatar_05.png" alt="avatar" />
									<div class="msg-content">
										<span class="name">Diamond Harlands</span>
										<span class="msg">:)</span>
									</div>
									<span class="msg-time">3h</span>
								</a>
							</li>
							<li>
								<a href="index.html#">
									<img class="msg-avatar" src="assets/backend/demo/avatar/avatar_02.png" alt="avatar" />
									<div class="msg-content">
										<span class="name">Michael Serio <i class="fa fa-paperclip attachment"></i></span>
										<span class="msg">Sed distinctio dolores fuga molestiae modi?</span>
									</div>
									<span class="msg-time">12h</span>
								</a>
							</li>
							<li>
								<a href="index.html#">
									<img class="msg-avatar" src="assets/backend/demo/avatar/avatar_03.png" alt="avatar" />
									<div class="msg-content">
										<span class="name">Matt Jones</span>
										<span class="msg">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et mole</span>
									</div>
									<span class="msg-time">2d</span>
								</a>
							</li>
							<li>
								<a href="index.html#">
									<img class="msg-avatar" src="assets/backend/demo/avatar/avatar_07.png" alt="avatar" />
									<div class="msg-content">
										<span class="name">John Doe</span>
										<span class="msg">Neque porro quisquam est qui dolorem</span>
									</div>
									<span class="msg-time">7d</span>
								</a>
							</li>
						</ul>
					</div>

					<div class="dd-footer"><a href="index.html#">View all messages</a></div>
				</div>
			</li>  -->



			<li class="dropdown toolbar-icon-bg">
				<a href="index.html#" class="dropdown-toggle" data-toggle='dropdown'><span class="icon-bg"><i class="fa fa-fw fa-user"></i></span></a>
				<ul class="dropdown-menu userinfo arrow">
					<li><a href="<?=site_url('admin/usuario/perfil/'.$this->session->userdata('usuario')['id_usuario'])?>"><span class="pull-left">Perfil</span> </a></li>
					<li class="divider"></li>
					<li><a href="<?=site_url('admin/backend/salir')?>"><span class="pull-left">Salir</span> <i class="pull-right fa fa-sign-out"></i></a></li>
				</ul>
			</li>

		</ul>

	</header>

	<div id="wrapper">
		<div id="layout-static">
			<div class="static-sidebar-wrapper sidebar-midnightblue">
				<div class="static-sidebar">
					<div class="sidebar">
						<div class="widget stay-on-collapse" id="widget-welcomebox">
							<div class="widget-body welcome-box tabular">
								<div class="tabular-row">
									<div class="tabular-cell welcome-avatar">
									<a href="<?=site_url('admin/perfil')?>">
										<?php $foto = $this->session->userdata('usuario')['foto']; if ($foto == '' || $foto == 'perfil.jpg'): ?>
											<img src="<?=base_url('assets/uploads/perfiles/perfil.jpg')?>" class="avatar">
										<?php else: ?>
											<img src="<?=base_url('assets/uploads/perfiles/'.$foto)?>" class="avatar">
										<?php endif; ?>
										</a>
									</div>
									<div class="tabular-cell welcome-options">
										<span class="welcome-text">Bienvenido (a),</span>
										<a href="<?=site_url('admin/perfil')?>" class="name"><?=$this->session->userdata('usuario')['nombre']?></a>
									</div>
								</div>
							</div>
						</div>
						<div class="widget stay-on-collapse" id="widget-sidebar">
							<nav role="navigation" class="widget-body">
								<ul class="acc-menu">
									<li class="nav-separator">Menú</li>
									<li><a href="<?=site_url('admin/backend/index')?>"><i class="fa fa-home"></i><span>Panel de control</span></a></li>
									<li><a href="javascript:;"><i class="fa fa-columns"></i><span>Departamento</span></a>
										<ul class="acc-menu">
											<li><a href="<?=site_url('admin/departamento/nuevo')?>">Nuevo</a></li>
											<li><a href="<?=site_url('admin/departamento/')?>">Listar</a></li>
										</ul>
									</li>
									<li><a href="javascript:;"><i class="fa fa-users"></i><span> Usuario</span></a>
										<ul class="acc-menu">
											<li><a href="<?=site_url('admin/usuario')?>">Ver todos</a></li>
											<li><a href="<?=site_url('admin/usuario/nuevo')?>">Nuevo</a></li>
										</ul>
									</li>
									<li><a href="javascript:;"><i class="fa fa-folder-open"></i><span>Categorías</span></a>
										<ul class="acc-menu">
											<li><a href="<?=site_url('admin/categoria')?>">Ver todas</a></li>
											<li><a href="<?=site_url('admin/categoria/nuevo')?>">Nueva</a></li>
										</ul>
									</li>
									<li><a href="javascript:;"><i class="fa fa-list"></i><span>Menú del sitio</span></a>
										<ul class="acc-menu">
											<li><a href="<?=site_url('admin/menu')?>">Ver</a></li>
											<li><a href="<?=site_url('admin/menu/nuevo')?>">Nuevo elemento</a></li>
										</ul>
									</li>
									<li><a href="javascript:;"><i class="fa fa-sitemap"></i><span>Redes Sociales</span></a>
										<ul class="acc-menu">
											<li><a href="<?=site_url('admin/redes_sociales')?>">Ver</a></li>
											<li><a href="<?=site_url('admin/redes_sociales/nuevo')?>">Nueva</a></li>
										</ul>
									</li>
									<li><a href="javascript:;"><i class="fa fa-ticket"></i><span>Tickets</span></a>
										<ul class="acc-menu">
											<!-- <li class="disabled-link"><a href="#">Ver todos</a></li> -->
											<li><a href="javascript:;"><span>Prioridades</span></a>
												<ul class="acc-menu">
													<li><a href="<?=site_url('admin/prioridad')?>">Ver todas</a></li>
													<li><a href="<?=site_url('admin/prioridad/nuevo')?>">Nueva</a></li>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
