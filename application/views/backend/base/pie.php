			<footer role="contentinfo">
				<div class="clearfix">
					<ul class="list-unstyled list-inline pull-left">
						<li>
							<h6 style="margin: 0;"> &copy; 2015 Avenger</h6></li>
					</ul>
					<button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></button>
				</div>
			</footer>
		</div>
	</div>
</div>


<div class="infobar-wrapper scroll-pane">
	<div class="infobar scroll-content">

		<div id="widgetarea">

			<div class="widget" id="widget-sparkline">
				<div class="widget-heading">
					<a href="javascript:;" data-toggle="collapse" data-target="#sparklinestats"><h4>Sparkline Stats</h4></a>
				</div>
				<div id="sparklinestats" class="collapse in">
					<div class="widget-body">
						<ul class="sparklinestats">
							<li>
								<div class="title">Earnings</div>
								<div class="stats">$22,500</div>
								<div class="sparkline" id="infobar-earningsstats" style="100%"></div>
							</li>
							<li>
								<div class="title">Orders</div>
								<div class="stats">4,750</div>
								<div class="sparkline" id="infobar-orderstats" style="100%"></div>
							</li>
						</ul>
						<a href="index.html#" class="more">More Sparklines</a>
					</div>
				</div>
			</div>

			<div class="widget">
				<div class="widget-heading">
					<a href="javascript:;" data-toggle="collapse" data-target="#recentactivity"><h4>Recent Activity</h4></a>
				</div>
				<div id="recentactivity" class="collapse in">
					<div class="widget-body">
						<ul class="recent-activities">
							<li>
								<div class="avatar">
									<img src="<?=base_url('assets/backend/demo/avatar/avatar_11.png')?>" class="img-responsive img-circle">
								</div>
								<div class="content">
									<span class="msg"><a href="index.html#" class="person">Jean Alanis</a> invited 3 unconfirmed members</span>
									<span class="time">2 mins ago</span>

								</div>
							</li>
							<li>
								<div class="avatar">
									<img src="<?=base_url('assets/backend/demo/avatar/avatar_09.png')?>" class="img-responsive img-circle">
								</div>
								<div class="content">
									<span class="msg"><a href="index.html#" class="person">Anthony Ware</a> is now following you</span>
									<span class="time">4 hours ago</span>

								</div>
							</li>
							<li>
								<div class="avatar">
									<img src="<?=base_url('assets/backend/demo/avatar/avatar_04.png')?>" class="img-responsive img-circle">
								</div>
								<div class="content">
									<span class="msg"><a href="index.html#" class="person">Bruce Ory</a> commented on <a href="index.html#">Dashboard UI</a></span>
									<span class="time">16 hours ago</span>
								</div>
							</li>
							<li>
								<div class="avatar">
									<img src="<?=base_url('assets/backend/demo/avatar/avatar_01.png')?>" class="img-responsive img-circle">
								</div>
								<div class="content">
									<span class="msg"><a href="index.html#" class="person">Roxann Hollingworth</a>is now following you</span>
									<span class="time">Feb 13, 2015</span>
								</div>
							</li>
						</ul>
						<a href="index.html#" class="more">See all activities</a>
					</div>
				</div>
			</div>

			<div class="widget">
				<div class="widget-heading">
					<a href="javascript:;" data-toggle="collapse" data-target="#widget-milestones"><h4>Milestones</h4></a>
				</div>
				<div id="widget-milestones" class="collapse in">
					<div class="widget-body">
						<div class="contextual-progress">
							<div class="clearfix">
								<div class="progress-title">UI Design</div>
								<div class="progress-percentage">12/16</div>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-lime" style="width: 75%"></div>
							</div>
						</div>
						<div class="contextual-progress">
							<div class="clearfix">
								<div class="progress-title">UX Design</div>
								<div class="progress-percentage">8/24</div>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-orange" style="width: 33.3%"></div>
							</div>
						</div>
						<div class="contextual-progress">
							<div class="clearfix">
								<div class="progress-title">Frontend Development</div>
								<div class="progress-percentage">8/40</div>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-purple" style="width: 20%"></div>
							</div>
						</div>
						<div class="contextual-progress m0">
							<div class="clearfix">
								<div class="progress-title">Backend Development</div>
								<div class="progress-percentage">24/48</div>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-danger" style="width: 50%"></div>
							</div>
						</div>
						<a href="index.html#" class="more">See All</a>
					</div>
				</div>
			</div>

			<div class="widget">
				<div class="widget-heading">
					<a href="javascript:;" data-toggle="collapse" data-target="#widget-contact"><h4>Contacts</h4></a>
				</div>
				<div id="widget-contact" class="collapse in">
					<div class="widget-body">
						<ul class="contact-list">
							<li id="contact-1">
								<a href="javascript:;"><img src="<?=base_url('assets/backend/demo/avatar/avatar_02.png')?>" alt=""><span>Jeremy Potter</span></a>
								<!-- <div class="contact-card contactdetails" data-child-of="contact-1">
				<div class="avatar">
					<img src="assets/backend/demo/avatar/avatar_11.png" class="img-responsive img-circle">
				</div>
				<span class="contact-name">Jeremy Potter</span>
				<span class="contact-status">Client Representative</span>
				<ul class="details">
					<li><a href="#"><i class="fa fa-envelope-o"></i>&nbsp;p.bateman@gmail.com</a></li>
					<li><i class="fa fa-phone"></i>&nbsp;+1 234 567 890</li>
					<li><i class="fa fa-map-marker"></i>&nbsp;Hollywood Hills, California</li>
				</ul>
			</div> -->
							</li>
							<li id="contact-2">
								<a href="javascript:;"><img src="<?=base_url('assets/backend/demo/avatar/avatar_07.png')?>" alt=""><span>David Tennant</span></a>
								<!-- <div class="contact-card contactdetails" data-child-of="contact-2">
				<div class="avatar">
					<img src="assets/backend/demo/avatar/avatar_11.png" class="img-responsive img-circle">
				</div>
				<span class="contact-name">David Tennant</span>
				<span class="contact-status">Client Representative</span>
				<ul class="details">
					<li><a href="#"><i class="fa fa-envelope-o"></i>&nbsp;p.bateman@gmail.com</a></li>
					<li><i class="fa fa-phone"></i>&nbsp;+1 234 567 890</li>
					<li><i class="fa fa-map-marker"></i>&nbsp;Hollywood Hills, California</li>
				</ul>
			</div> -->
							</li>
							<li id="contact-3">
								<a href="javascript:;"><img src="<?=base_url('assets/backend/demo/avatar/avatar_03.png')?>" alt=""><span>Anna Johansson</span></a>
								<!-- <div class="contact-card contactdetails" data-child-of="contact-3">
				<div class="avatar">
					<img src="assets/backend/demo/avatar/avatar_11.png" class="img-responsive img-circle">
				</div>
				<span class="contact-name">Anna Johansson</span>
				<span class="contact-status">Client Representative</span>
				<ul class="details">
					<li><a href="#"><i class="fa fa-envelope-o"></i>&nbsp;p.bateman@gmail.com</a></li>
					<li><i class="fa fa-phone"></i>&nbsp;+1 234 567 890</li>
					<li><i class="fa fa-map-marker"></i>&nbsp;Hollywood Hills, California</li>
				</ul>
			</div> -->
							</li>
							<li id="contact-4">
								<a href="javascript:;"><img src="<?=base_url('assets/backend/demo/avatar/avatar_09.png')?>" alt=""><span>Alan Doyle</span></a>
								<!-- <div class="contact-card contactdetails" data-child-of="contact-4">
				<div class="avatar">
					<img src="assets/backend/demo/avatar/avatar_11.png" class="img-responsive img-circle">
				</div>
				<span class="contact-name">Alan Doyle</span>
				<span class="contact-status">Client Representative</span>
				<ul class="details">
					<li><a href="#"><i class="fa fa-envelope-o"></i>&nbsp;p.bateman@gmail.com</a></li>
					<li><i class="fa fa-phone"></i>&nbsp;+1 234 567 890</li>
					<li><i class="fa fa-map-marker"></i>&nbsp;Hollywood Hills, California</li>
				</ul>
			</div> -->
							</li>
							<li id="contact-5">
								<a href="javascript:;"><img src="<?=base_url('assets/backend/demo/avatar/avatar_05.png')?>" alt=""><span>Simon Corbett</span></a>
								<!-- <div class="contact-card contactdetails" data-child-of="contact-5">
				<div class="avatar">
					<img src="assets/backend/demo/avatar/avatar_11.png" class="img-responsive img-circle">
				</div>
				<span class="contact-name">Simon Corbett</span>
				<span class="contact-status">Client Representative</span>
				<ul class="details">
					<li><a href="#"><i class="fa fa-envelope-o"></i>&nbsp;p.bateman@gmail.com</a></li>
					<li><i class="fa fa-phone"></i>&nbsp;+1 234 567 890</li>
					<li><i class="fa fa-map-marker"></i>&nbsp;Hollywood Hills, California</li>
				</ul>
			</div> -->
							</li>
							<li id="contact-6">
								<a href="javascript:;"><img src="<?=base_url('assets/backend/demo/avatar/avatar_01.png')?>" alt=""><span>Polly Paton</span></a>
								<!-- <div class="contact-card contactdetails" data-child-of="contact-6">
				<div class="avatar">
					<img src="assets/backend/demo/avatar/avatar_11.png" class="img-responsive img-circle">
				</div>
				<span class="contact-name">Polly Paton</span>
				<span class="contact-status">Client Representative</span>
				<ul class="details">
					<li><a href="#"><i class="fa fa-envelope-o"></i>&nbsp;p.bateman@gmail.com</a></li>
					<li><i class="fa fa-phone"></i>&nbsp;+1 234 567 890</li>
					<li><i class="fa fa-map-marker"></i>&nbsp;Hollywood Hills, California</li>
				</ul>
			</div> -->
							</li>
						</ul>
						<a href="index.html#" class="more">See All</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Load site level scripts -->

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->

<script src="<?=base_url('assets/backend/js/jquery-1.10.2.min.js')?>"></script>
<!-- Load jQuery -->
<script src="<?=base_url('assets/backend/js/jqueryui-1.9.2.min.js')?>"></script>
<!-- Load jQueryUI -->

<script src="<?=base_url('assets/backend/js/bootstrap.min.js')?>"></script>
<!-- Load Bootstrap -->


<script src="<?=base_url('assets/backend/plugins/easypiechart/jquery.easypiechart.js')?>"></script>
<!-- EasyPieChart-->
<script src="<?=base_url('assets/backend/plugins/sparklines/jquery.sparklines.min.js')?>"></script>
<!-- Sparkline -->
<script src="<?=base_url('assets/backend/plugins/jstree/dist/jstree.min.js')?>"></script>
<!-- jsTree -->

<script src="<?=base_url('assets/backend/plugins/codeprettifier/prettify.js')?>"></script>
<!-- Code Prettifier  -->
<script src="<?=base_url('assets/backend/plugins/bootstrap-switch/bootstrap-switch.js')?>"></script>
<!-- Swith/Toggle Button -->

<script src="<?=base_url('assets/backend/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js')?>"></script>
<!-- Bootstrap Tabdrop -->

<script src="<?=base_url('assets/backend/plugins/iCheck/icheck.min.js')?>"></script>
<!-- iCheck -->

<script src="<?=base_url('assets/backend/js/enquire.min.js')?>"></script>
<!-- Enquire for Responsiveness -->

<script src="<?=base_url('assets/backend/plugins/bootbox/bootbox.js')?>"></script>
<!-- Bootbox -->

<script src="<?=base_url('assets/backend/plugins/simpleWeather/jquery.simpleWeather.min.js')?>"></script>
<!-- Weather plugin-->

<script src="<?=base_url('assets/backend/plugins/nanoScroller/js/jquery.nanoscroller.min.js')?>"></script>
<!-- nano scroller -->

<script src="<?=base_url('assets/backend/plugins/jquery-mousewheel/jquery.mousewheel.min.js')?>"></script>
<!-- Mousewheel support needed for jScrollPane -->
<script src="<?=base_url('assets/backend/js/application.js')?>"></script>
<!-- End loading page level scripts-->

<?php if($controlador=='listar'):?>

<script src="<?=base_url('assets/backend/plugins/datatables/jquery.dataTables.js')?>"></script>
<script>
$(document).ready(function() {
	$('#tabla').dataTable({
		"language": {
			"lengthMenu": "_MENU_",
			"processing":"Procesando...",
			"zeroRecords":    "No se encontraron resultados",
			"emptyTable":     "Ningún dato disponible en esta tabla",
			"info":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"infoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"infoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"infoPostFix":    "",
			"search":         "Buscar:",
			"url":            "",
			"infoThousands":  ",",
			"loadingRecords": "Cargando...",
			"paginate": {
				"first":    "Primero",
				"last":     "Último",
				"next":     "Siguiente",
				"previous": "Anterior"
			},
			"aria": {
				"sortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});
	$('.dataTables_filter input').attr('placeholder','Buscar...');


	//DOM Manipulation to move datatable elements integrate to panel
	$('.panel-ctrls').append($('.dataTables_filter').addClass("pull-right")).find("label").addClass("panel-ctrls-center");
	$('.panel-ctrls').append("<i class='separator'></i>");
	$('.panel-ctrls').append($('.dataTables_length').addClass("pull-left")).find("label").addClass("panel-ctrls-center");

	$('.panel-footer').append($(".dataTable+.row"));
	$('.dataTables_paginate>ul.pagination').addClass("pull-right m0");

});
	function btn_cambiar(id) {
		$('#'+id).css('color','#f9a825');
		$.post('<?=site_url('admin/'.$this->uri->segment(2).'/cambiar')?>',{id:id}, function (data) {
			if(data!=0){
				alert('No se pudo cambiar la categoría predeterminada')
			}
			else{
				location.reload();
			}
		});
	}

	function over(id){
		$('#'+id).css('color','#f9a825');
	}
	function salir(id){
		$('#'+id).css('color','');
	}
</script>
<script src="<?=base_url('assets/backend/plugins/datatables/dataTables.bootstrap.js')?>"></script>

<?php elseif ($controlador == 'insertar-w' || $controlador == 'editar-w') :?>
	<script src="<?=base_url('assets/backend/plugins/tinymce/tinymce.min.js')?>"></script>  	<!-- TinyMCE -->
	<script>
	tinymce.init({
	selector: 'textarea',
	language: 'es',
	toolbar: 'undo redo | styleselect | bold italic | link | alignleft aligncenter alignright alignjustify | bullist numlist',
	plugins: [
      'advlist autolink link lists charmap preview hr anchor pagebreak',
      'wordcount visualblocks visualchars insertdatetime media nonbreaking',
      'save table contextmenu paste '
    ],
	menubar: false,
	forced_root_block : false
	});
	</script>
	<script src="<?=base_url('assets/backend/js/bootstrap-filestyle.js')?>"></script>
	<script>
	$('#organigrama').filestyle({
		iconName : 'fa fa-file-text',
		buttonText : ' Seleccionar archivo',
		buttonName : 'btn-success'
	});
	</script>
	<script>
		window.ParsleyConfig = {
			  successClass: 'has-success'
			, errorClass: 'has-error'
			, errorElem: '<span></span>'
			, errorsWrapper: '<span class="help-block"></span>'
			, errorTemplate: "<div></div>"
			, classHandler: function(el) {
				return el.$element.closest(".form-group");
			}
		};
	</script>
<script src="<?=base_url('assets/backend/plugins/parsley/parsley.js')?>"></script>
<script src="<?=base_url('assets/backend/plugins/parsley/es.js')?>"></script>
<script>
	$(document).ready(function () {
		$('#validate-form #submit').on('click', function () {
	        $('#validate-form').parsley().validate();
	    });
		// iCheck
	// Loop through all the checkbox/radio classes and assign them colors and styles
		var myArr=["minimal","flat","square"];
		var aCol=['red','green','aero','grey','orange','pink','purple','yellow','purple','yellow','blue']

		for (var i = 0; i < myArr.length; ++i) {
			for (var j = 0; j < aCol.length; ++j) {
				// $('.icheck-minimal .blue.icheck input').iCheck({checkboxClass: 'icheckbox_minimal-blue',radioClass: 'iradio_minimal-blue'});
				$('.icheck-' + myArr[i] + ' .' + aCol[j] + '.icheck input').iCheck({checkboxClass: 'icheckbox_' + myArr[i] + '-' + aCol[j],radioClass: 'iradio_' + myArr[i] + '-' + aCol[j]});
			}
		};
		$('label#opcionOrg1').click(function () {
			if($('#org').hasClass('hide')){
				$('#orgSel').hide();
				$('#org').removeClass('hide')
				$('#org').show('slow');
			}
		});
		$('input#opcionOrg1').click(function () {
			if($('#org').hasClass('hide')){
				$('#orgSel').hide();
				$('#org').removeClass('hide')
				$('#org').show('slow');
			}
		});
	});
</script>

<?php elseif ($controlador =='insertar' || $controlador =='editar') :?>
	<script>
		window.ParsleyConfig = {
			  successClass: 'has-success'
			, errorClass: 'has-error'
			, errorElem: '<span></span>'
			, errorsWrapper: '<span class="help-block"></span>'
			, errorTemplate: "<div></div>"
			, classHandler: function(el) {
				return el.$element.closest(".form-group");
			}
		};
	</script>
<script src="<?=base_url('assets/backend/plugins/parsley/parsley.js')?>"></script>
<script src="<?=base_url('assets/backend/plugins/parsley/es.js')?>"></script>
<script >
	$(document).ready(function () {
		$('#validate-form #submit').on('click', function () {
	        $('#validate-form').parsley().validate();
	    });
	    $('#generarClave').click(function () {
	    	$.post('<?=site_url('admin/usuario/generarClave')?>', function (data) {
	    		$('#clave').val(data.clave);
	    	})
	    });

	})
</script>
<script src="<?=base_url('assets/backend/plugins/form-colorpicker/js/bootstrap-colorpicker.min.js')?>"></script> 			<!-- Color Picker -->
<script>
	$('.cpicker').colorpicker();
</script>

<?php elseif ($controlador=='menu') :?>
	<script src="<?=base_url('assets/backend/plugins/form-nestable/jquery.nestable.min.js')?>"></script>
	<script >
		$('#nestable_list_3').nestable().on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable_list_3').data('output', $('#nestable_list_3_output')));

    function updateOutput(e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            var salida = window.JSON.stringify(list.nestable('serialize'));
             output.val(salida); //, null, 2));
        } else {
            alert('Se necesita que su navegador que soporte JSON par algunas funciones.');
        }
    }
	</script>
<?php elseif($controlador == 'detalle-dep'):?>
<script src="<?=base_url('assets/backend/plugins/parsley/parsley.js')?>"></script>
<script src="<?=base_url('assets/backend/plugins/parsley/es.js')?>"></script>
<script src="<?=base_url('assets/backend/plugins/form-multiselect/js/jquery.multi-select.min.js')?>"></script>
<script>
	window.ParsleyConfig = {
			  successClass: 'has-success'
			, errorClass: 'has-error'
			, errorElem: '<span></span>'
			, errorsWrapper: '<span class="help-block"></span>'
			, errorTemplate: "<div></div>"
			, classHandler: function(el) {
				return el.$element.closest(".form-group");
			}
		};
	$(document).ready(function () {
		$('#multi-select').multiSelect();
		$('#validate-form #submit').on('click', function () {
	        $('#validate-form').parsley().validate();
	    });


	})
</script>

<?php elseif($controlador == 'insertar-menu'):?>

	<script>
		window.ParsleyConfig = {
			  successClass: 'has-success'
			, errorClass: 'has-error'
			, errorElem: '<span></span>'
			, errorsWrapper: '<span class="help-block"></span>'
			, errorTemplate: "<div></div>"
			, classHandler: function(el) {
				return el.$element.closest(".form-group");
			}
		};
	</script>
<script src="<?=base_url('assets/backend/plugins/parsley/parsley.js')?>"></script>
<script src="<?=base_url('assets/backend/plugins/parsley/es.js')?>"></script>
<script src="<?=base_url('assets/backend/plugins/jquiery-chained/jquery.chained.min.js')?>"></script>
<link href="<?=base_url('assets/backend/plugins/form-select2/select2.css')?>" type="text/css" rel="stylesheet">
<script src="<?=base_url('assets/backend/plugins/form-select2/select2.min.js')?>"></script>
<script >
	
	$(document).ready(function () {
		$('#accion2').chained('#accion1');

		$('#validate-form #submit').on('click', function () {
	        $('#validate-form').parsley().validate();
	    });

	    $('#accion1').change(function () {
	    	$('#accion2').select2();
	    	if($(this).val() == 'vinculo'){
	    		$('#tipo').val('externo');
	    		$('#URL1').show('slow');
	    		$('#url').prop('type','url');
	    		$('#url').prop('required',true);
	    	}	
	    	else{
	    		$('#tipo').val('interno');
	    		$('#URL1').hide('slow');
	    		$('#url').prop('type','hidden');
	    		$('#url').prop('required',false);
	    	}
	    });

	    $('input[name="dentro"]').change(function () {
	    	if($(this).val() == '1'){
	    		$('#pregunta2').hide('slow');
	    		$('#posicion').val(1);
	    	}
	    	else{
	    		$('#pregunta2').show('slow');
	    		$('#posicion').val(0);
	    	}
	    });

	});
</script>
<?php endif; ?>

</body>

</html>

