<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Usuario</h1>
			</div>
			<div class="container-fluid">
				<div class="row">
	<div class="col-md-3">
		<div class="panel panel-profile">
			<div class="panel-body">
				<div class="user-card">
                    <div class="avatar">
                        <i class="fa fa-user" style="font-size: 10em"></i>
                    </div>
                    <div class="contact-name"><?=$fila[0]->nombre?></div>
                    <ul class="details">
                        <li><a href="#"><?=$fila[0]->correo?></a></li>
                        <li><?=$fila[0]->telefono?></li>
                    </ul>
                </div>
                <!--<hr class="outsider">
                <div class="text-center">
                    <a href="extras-profile.html#" class="btn btn-social btn-facebook"><i class="fa fa-facebook"></i></a>
                    <a href="extras-profile.html#" class="btn btn-social btn-twitter"><i class="fa fa-twitter"></i></a>
                    <a href="extras-profile.html#" class="btn btn-social btn-github"><i class="fa fa-github"></i></a>
                    <a href="extras-profile.html#" class="btn btn-social btn-flickr"><i class="fa fa-flickr"></i></a>
                    <a href="extras-profile.html#" class="btn btn-social btn-instagram"><i class="fa fa-instagram"></i></a>
                </div>
                <hr class="outsider">
                <p class="m-n">Asperiores in eveniet sapiente error fuga tenetur ex ea dignissimos voluptas ab molestiae eos totam quo dolorem maxime illo neque quia itaque.</p>-->
			</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>
					<ul class="nav nav-tabs">
						<li class="active"><a href="#timeline" data-toggle="tab">Ultimos post</a></li>
						<li><a href="#perfil" data-toggle="tab">Modificar perfil</a></li>
					</ul>
				</h2>
			</div>
			<div class="panel-body">
				<div class="tab-content">
					<div class="tab-pane active" id="timeline">
						<ul class="timeline">
							<li class="timeline-orange">
								<div class="timeline-icon"><i class="fa fa-camera"></i></div>
								<div class="timeline-body">
									<div class="timeline-header">
										<span class="author">Posted by <a href="extras-profile.html#">David Tennant</a></span>
										<span class="date">Monday, November 11, 2014</span>
									</div>
									<div class="timeline-content">
										<h3>Lorem Ipsum Dolor Sit Amet</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, officiis, molestiae, deserunt asperiores architecto ut vel repudiandae dolore inventore nesciunt necessitatibus doloribus ratione facere consectetur suscipit! Quasi, officia, veniam mollitia recusandae iure aperiam totam culpa aut nobis eveniet porro laborum quisquam non.</p>
									</div>
									<div class="timeline-footer">
										<a href="extras-profile.html#" class="btn-link pull-left">Read Full Story</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="tab-pane" id="perfil">
						<div class="row">
							<div class="col-lg-8 col-lg-offset-2">
								<?=form_open('admin/usuario/editar/'.$fila[0]->id_usuario,array('id' =>'validate-form'))?>
								<?=validation_errors()?>
								<div class="form-group">
									<?=form_label('Nombre completo','nombre')?>
									<?=form_input(array('name' => 'nombre','id'=>'nombre','class'=>'form-control','required'=>'required','value'=>$fila[0]->nombre))?>
								</div>
								<div class="form-group">
									<?=form_label('Usuario','usuario')?>
									<?=form_input(array('name'=>'usuario','id'=>'usuario','class'=>'form-control','required'=>'required','value'=>$fila[0]->usuario,'readonly'=>'readonly'))?>
								</div>
								<div class="form-group">
									<?=form_label('Correo','correo')?>
									<?=form_input(array('name'=>'correo','id'=>'correo','class'=>'form-control','required'=>'required','value'=>$fila[0]->correo))?>
								</div>
								<div class="form-group">
									<?=form_label('Teléfono','telefono')?>
									<?=form_input(array('name'=>'telefono','id'=>'telefono','class'=>'form-control','required'=>'required','value'=>$fila[0]->telefono ))?>
								</div>
								<div class="form-group">
									<?=form_label('Clave','clave')?>
									<input name="clave" type="text" class="form-control">
								</div>
								<div class="form-group">
									<label>Permisos</label>
									<div class="btn-group mb10 btn-group-justified" data-toggle="buttons">
                            			<label class="btn btn-orange <?=($fila[0]->permisos)==1 ? 'active' : '' ?>">
                                			<input type="radio" name="permiso" value="1" <?=($fila[0]->permisos)==1 ? 'checked' : '' ?>> Administrador
                            			</label>
			                            <label class="btn btn-purple <?=($fila[0]->permisos)==0 ? 'active' : '' ?>">
			                                <input type="radio" name="permiso" value="0" <?=($fila[0]->permisos)==0 ? 'checked' : '' ?>> Usuario frontend
			                            </label>
                        			</div>
								</div>
								<?php 
									if($this->session->userdata('usuario')['id_usuario'] == $fila[0]->id_usuario){
										if($fila[0]->config==0) 
											echo form_hidden('config','1');
									}
								?>
								<div class="panel-footer text-center">
									<button type="submit" class="btn btn-success btn-lg" id="submit">Guardar</button>
								</div>
								<?=form_close()?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
