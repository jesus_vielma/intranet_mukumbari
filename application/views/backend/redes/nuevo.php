<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Redes sociales</h1>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2>Nueva red social</h2>
								<div class="panel-ctrls">
								</div>
							</div>
							<div class="panel-body ">
								<?=form_open('admin/redes_sociales/nuevo/2',array('id' =>'validate-form'))?>
								<?=validation_errors()?>
								<div class="form-group">
									<?=form_label('Nombre','nombre')?>
									<?=form_input(array('name' => 'nombre','id'=>'nombre','class'=>'form-control','required'=>'required','value'=>set_value('nombre')) )?>
								</div>
								<div class="form-group">
									<?=form_label('URL','url')?>
									<?=form_input(array('type'=>'url','name'=>'url','id'=>'url','class'=>'form-control','value'=>set_value('url'),'required'=>'required'))?>
								</div>
								<div class="form-group">
									<label class="control-label">Red</label> <br>
									<div class="btn-group mb10" data-toggle="buttons">
                            			<label class="btn btn-social btn-facebook-alt">
                                			<input type="radio" name="tipo" value="facebook"> <i class="fa fa-facebook"></i>
                            			</label>
			                            <label class="btn btn-social btn-foursquare-alt">
			                                <input type="radio" name="tipo" value="foursquare" > <i class="fa fa-foursquare"></i>
			                            </label>
			                            <label class="btn btn-social btn-google-alt">
			                                <input type="radio" name="tipo" value="google" > <i class="fa fa-google"></i>
			                            </label>
			                            <label class="btn btn-social btn-instagram-alt">
			                                <input type="radio" name="tipo" value="instagram" > <i class="fa fa-instagram"></i>
			                            </label>
			                            <label class="btn btn-social btn-linkedin-alt">
			                                <input type="radio" name="tipo" value="linkedin" > <i class="fa fa-linkedin"></i>
			                            </label>
			                            <label class="btn btn-social btn-soundcloud-alt">
			                                <input type="radio" name="tipo" value="soundcloud" > <i class="fa fa-soundcloud"></i>
			                            </label> 
			                            <label class="btn btn-social btn-twitter-alt">
			                                <input type="radio" name="tipo" value="twitter" > <i class="fa fa-twitter"></i>
			                            </label>
			                            <label class="btn btn-social btn-youtube-alt">
			                                <input type="radio" name="tipo" value="youtube" > <i class="fa fa-youtube"></i>
			                            </label>
                        			</div>
								</div>
								<div class="panel-footer text-center">
									<button type="submit" class="btn btn-success btn-lg" id="submit">Guardar</button>
								</div>
								<?=form_close()?>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
