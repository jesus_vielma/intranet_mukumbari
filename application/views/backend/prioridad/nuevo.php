<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Departamento</h1>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2>Nuevo usuario</h2>
								<div class="panel-ctrls">
								</div>
							</div>
							<div class="panel-body ">
								<?=form_open('admin/prioridad/nuevo/2',array('id' =>'validate-form'))?>
								<?=validation_errors()?>
								<div class="form-group">
									<?=form_label('Nombre','nombre')?>
									<?=form_input(array('name' => 'nombre','id'=>'nombre','class'=>'form-control','required'=>'required','value'=>set_value('nombre')) )?>
								</div>
								<div class="form-group">
									<label class="control-label">Color</label>
										<div class="input-group color cpicker" data-color-format="hex" data-color="#000000">
											<input type="text" readonly class="form-control" value="" name="color">
											<span class="input-group-addon"><i style="background-color: rgb(0,0,0); margin-left: 8px;"></i></span>
										</div>
								</div>
								<div class="form-group">
									<label>Predeterminada</label>
									<div class="btn-group mb10 btn-group-justified" data-toggle="buttons">
                            			<label class="btn btn-orange">
                                			<input type="radio" name="predeterminado" value="1"> Si
                            			</label>
			                            <label class="btn btn-purple">
			                                <input type="radio" name="predeterminado" value="0"> No
			                            </label>
                        			</div>
								</div>
								<div class="panel-footer text-center">
									<button type="submit" class="btn btn-success btn-lg" id="submit">Guardar</button>
								</div>
								<?=form_close()?>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
