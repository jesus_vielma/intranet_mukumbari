<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Prioridad de tickets</h1>
				<div class="options">
					<div class="btn-toolbar">
						<a href="<?=site_url('admin/prioridad/nuevo')?>" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Nueva</a>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<?php if ($filas !=0) :?>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h2>Prioridades</h2>
									<div class="panel-ctrls">
									</div>
								</div>
								<div class="panel-body panel-no-padding">
									<table id="tabla" class="table table-striped table-bordered" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>Nombre</th>
												<th>Color</th>
												<th>Predeterminada</th>
												<th>Acciones</th>
											</tr>
										</thead>
										<tbody>
								<?php foreach ($filas as $fila):?>
											<tr class="">
												<td><?=$fila->nombre?></td>
												<td style="background-color:<?=$fila->color?>"><?=$fila->color?></td>
												<td>
													<?php if($fila->predeterminado == '1'):?>
														<button type="button" class="btn btn-predeterminado" style="color: #f9a825"><i class="fa fa-star"></i></button>
													<?php else:?>
														<button onclick="btn_cambiar(this.id)" onmouseover="over(this.id)" onmouseout="salir(this.id)" type="button" class="btn btn-predeterminado" id="<?=$fila->id_prioridad?>" ><i class="fa fa-star"></i></button>
													<?php endif;?>

												</td>
												<td><a href="<?=site_url('admin/prioridad/editar/1/'.$fila->id_prioridad)?>">Editar</a></td>
											   </tr>
								<?php endforeach;?>
										</tbody>
									</table>
									<div class="panel-footer"></div>
								</div>

							</div>
						</div>
					</div>
				<?php else: ?>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="panel panel-inverse">
								<div class="panel-heading">
									<h2>Departamentos</h2>
									<div class="panel-ctrls">
									</div>
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<h1>No se encuentra información</h1>
									</div>
									<div class="panel-footer">
										<a href="<?=site_url('admin/departamento/nuevo')?>" class="btn btn-primary">Nuevo </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>

			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
