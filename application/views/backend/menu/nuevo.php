<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Menú del sitio</h1>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2>Nuevo elemento</h2>
								<div class="panel-ctrls">
								</div>
							</div>
							<div class="panel-body ">
								<?=form_open('admin/menu/nuevo/2',array('id' =>'validate-form'))?>
								<?=validation_errors()?>
								<div class="form-group">
									<?=form_label('Nombre','nombre')?>
									<?=form_input(array('name' => 'nombre','id'=>'nombre','class'=>'form-control','required'=>'required','value'=>set_value('nombre')) )?>
								</div>
								<div class="form-group">
									<label>¿El elemento sera visible sin haber iniciado sesión?</label>
									<div class="btn-group mb10 btn-group-justified" data-toggle="buttons">
                            			<label class="btn btn-orange">
                                			<input type="radio" name="logueado" value="0"> Si
                            			</label>
			                            <label class="btn btn-purple">
			                                <input type="radio" name="logueado" value="1"> No
			                            </label>
                        			</div>
								</div>
								<div class="form-group">
									<label>Que hará este menú</label>
									<select id="accion1" class="form-control" name="accion1">
										<option value="">- Seleccione -</option>
										<option value="categoria">Ver categoría</option>
										<option value="articulo">Ver un articulo</option>
										<option value="vinculo">Enlace externo</option>
									</select>
								</div>
								<div class="form-group">
									<select id="accion2" class="form-control" name="accion2">
										<option value="" >- Seleccione -</option>
										<?php foreach ($categorias as $cat ): ?>
											<option value="<?=$cat->alias?>" class="categoria"><?=$cat->nombre?></option>
										<?php endforeach ?>
										<?php foreach ($articulos as $art ): ?>
											<option value="<?=$art->alias?>" class="articulo"><?=$art->titulo?></option>
										<?php endforeach ?>
									</select>
								</div>
								<input type="hidden" name="posicion" value="<?=$ultimo_padre[0]->posicion+1?>">
								<?=form_input(array('name'=>'tipo','type'=>'hidden','id'=>'tipo')) ?>
								<div class="form-group">
									<label id="URL1" style="display: none">URL</label>
								<?=form_input(array('name'=>'url','type'=>'hidden','id'=>'url','class'=>'form-control')) ?>
								</div>
								<div class="panel-footer text-center">
									<button type="submit" class="btn btn-success btn-lg" id="submit">Guardar</button>
								</div>
								<?=form_close()?>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
