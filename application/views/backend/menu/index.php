<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Menú del sitio</h1>
				<div class="options">
					<div class="btn-toolbar">
						<a href="<?=site_url('admin/menu/nuevo')?>" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Nueva</a>
					</div>
				</div>
			</div>
			<div class="container-fluid">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h2>Menú del sitio</h2>
									<div class="panel-ctrls">
									</div>
								</div>
								<div class="panel-body">
									<div class="dd" id="nestable_list_3">
					                    <ol class="dd-list">
					                    	<?php 
					                    		$var='';
												foreach ($menu as $m => $value) {
													if($var!=$m){
														if($value[0]>0){
															echo "<li class='dd-item dd3-item' data-id='".$value['id_menu']."'>"; 
															echo "<div class='dd-handle dd3-handle'></div>";
															//echo "<div class='dd3-content'>".$m."<span class='pull-right'><a title='Editar este menú' href='".site_url('admin/menu/editar/1/'.$value['id_menu'])."'><i class='fa fa-pencil'></i></a></span></div>";
															echo "<div class='dd3-content'>".$m."</div>";
															echo "<ol class='dd-list'>";
																foreach ($value[0] as $key) {
																	echo "<li class='dd-item dd3-item' data-id='".$key->id_menu."'>"; 
																	echo "<div class='dd-handle dd3-handle'></div>";
																	//echo "<div class='dd3-content'>".$key->nombre."<span class='pull-right'><a title='Editar este menú' href='".site_url('admin/menu/editar/1/'.$value['id_menu'])."'><i class='fa fa-pencil'></i></a></span></div></li>";
																	echo "<div class='dd3-content'>".$key->nombre."</div></li>";
																}
															echo "</ol>";
															echo "</li>";
															
														}
														else{
															echo "<li class='dd-item dd3-item' data-id='".$value['id_menu']."'>";
															echo "<div class='dd-handle dd3-handle'></div>";
															//echo "<div class='dd3-content'>".$m."<span class='pull-right'><a title='Editar este menú' href='".site_url('admin/menu/editar/1/'.$value['id_menu'])."'><i class='fa fa-pencil'></i></a></span></div>";
															echo "<div class='dd3-content'>".$m."<span class='pull-right'></div>";
															echo "</li>";
														}
													}
												}

					                    	?>
					                        <!--<li class="dd-item dd3-item" data-id="13">
					                            <div class="dd-handle dd3-handle"></div>
					                            <div class="dd3-content">Item 13</div>
					                        </li>
					                        <li class="dd-item dd3-item" data-id="14">
					                            <div class="dd-handle dd3-handle"></div>
					                            <div class="dd3-content">Item 14</div>
					                        </li>
					                        <li class="dd-item dd3-item" data-id="15">
					                            <div class="dd-handle dd3-handle"></div>
					                            <div class="dd3-content">Item 15</div>
					                            <ol class="dd-list">
					                                <li class="dd-item dd3-item" data-id="16">
					                                    <div class="dd-handle dd3-handle"></div>
					                                    <div class="dd3-content">Item 16</div>
					                                </li>
					                                <li class="dd-item dd3-item" data-id="17">
					                                    <div class="dd-handle dd3-handle"></div>
					                                    <div class="dd3-content">Item 17</div>
					                                </li>
					                                <li class="dd-item dd3-item" data-id="18">
					                                    <div class="dd-handle dd3-handle"></div>
					                                    <div class="dd3-content">Item 18</div>
					                                </li>
					                            </ol>
					                        </li>-->
					                    </ol>
					                </div>
									<div class="panel-footer">
									<h5>Si haz realizado algún cambio en el orden del menú recuerda presionar el botón guardar</h5>
									<?=form_open('admin/menu/ordenarMenu')?>
								        <input type="hidden" id="nestable_list_3_output" name="menuNuevo"/>
								        <button type="submit" class="btn btn-success">Guardar</button>
								    <?=form_close()?>
									</div>
								</div>

							</div>
						</div>
					</div>

			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
