<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Departamento</h1>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2>Nuevo usuario</h2>
								<div class="panel-ctrls">
								</div>
							</div>
							<div class="panel-body ">
								<?=form_open('admin/usuario/nuevo/2',array('id' =>'validate-form'))?>
								<?=validation_errors()?>
								<div class="form-group">
									<?=form_label('Nombre completo','nombre')?>
									<?=form_input(array('name' => 'nombre','id'=>'nombre','class'=>'form-control','required'=>'required','value'=>set_value('nombre')) )?>
								</div>
								<div class="form-group">
									<?=form_label('Usuario','usuario')?>
									<?=form_input(array('name'=>'usuario','id'=>'usuario','class'=>'form-control','required'=>'required','value'=>set_value('usuario') ))?>
								</div>
								<div class="form-group">
									<?=form_label('Correo','correo')?>
									<?=form_input(array('name'=>'correo','id'=>'correo','class'=>'form-control','required'=>'required','value'=>set_value('correo') ))?>
								</div>
								<div class="form-group">
									<?=form_label('Teléfono','telefono')?>
									<?=form_input(array('name'=>'telefono','id'=>'telefono','class'=>'form-control','required'=>'required','value'=>set_value('telefono') ))?>
								</div>
								<div class="form-group">
									<?=form_label('Clave','clave')?>
									<div class="input-group">
										<input name="clave" type="text" class="form-control" readonly="" required="" id="clave">
										<div class="input-group-btn">
											<button type="button" class="btn btn-primary" id="generarClave">Generar Clave</button>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Permisos</label>
									<div class="btn-group mb10 btn-group-justified" data-toggle="buttons">
                            			<label class="btn btn-orange">
                                			<input type="radio" name="permiso" value="1"> Administrador
                            			</label>
			                            <label class="btn btn-purple">
			                                <input type="radio" name="permiso" value="0" checked=""> Usuario
			                            </label>
                        			</div>
								</div>
								<div class="panel-footer text-center">
									<button type="submit" class="btn btn-success btn-lg" id="submit">Guardar</button>
								</div>
								<?=form_close()?>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
