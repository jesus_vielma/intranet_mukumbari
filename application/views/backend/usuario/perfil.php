<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Usuario</h1>
			</div>
			<div class="container-fluid">
			<?php if (validation_errors()): ?>
				
				<div class="row">
					<div class="col-lg-6 col-lg-offset-4">
						<div class="alert alert-danger">
							<h3>Errores de validación del perfil</h3>
							<?=validation_errors()?>
						</div>
					</div>
				</div>
			<?php endif ?>
				<div class="row">
	<div class="col-md-3">
		<div class="panel panel-profile">
			<div class="panel-body">
				<div class="user-card">
                    <div class="avatar">
                    <?php if($fila[0]->foto!='') $foto = $fila[0]->foto; else $foto = 'perfil.jpg';?>
                        <img src="<?=base_url('assets/uploads/perfiles/'.$foto)?>" class="img-responsive img-circle" alt="<?=$fila[0]->nombre?>">
                    </div>
                    <div class="contact-name"><?=$fila[0]->nombre?></div>
                    <ul class="details">
                        <li><a href="#"><?=$fila[0]->correo?></a></li>
                        <li><?=$fila[0]->telefono?></li>
                        <li><?=isset($fila[0]->dpto) ? $fila[0]->dpto : 'No esta asignado a un departamento'?></li>
                    </ul>
                </div>
                <!--<hr class="outsider">
                <div class="text-center">
                    <a href="extras-profile.html#" class="btn btn-social btn-facebook"><i class="fa fa-facebook"></i></a>
                    <a href="extras-profile.html#" class="btn btn-social btn-twitter"><i class="fa fa-twitter"></i></a>
                    <a href="extras-profile.html#" class="btn btn-social btn-github"><i class="fa fa-github"></i></a>
                    <a href="extras-profile.html#" class="btn btn-social btn-flickr"><i class="fa fa-flickr"></i></a>
                    <a href="extras-profile.html#" class="btn btn-social btn-instagram"><i class="fa fa-instagram"></i></a>
                </div>
                <hr class="outsider">
                <p class="m-n">Asperiores in eveniet sapiente error fuga tenetur ex ea dignissimos voluptas ab molestiae eos totam quo dolorem maxime illo neque quia itaque.</p>-->
			</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>
					<ul class="nav nav-tabs">
						<li class="active"><a href="#timeline" data-toggle="tab">Últimos 5 artículos</a></li>
						<!-- <li><a href="#timeline2" data-toggle="tab">Últimos 5 eventos</a></li> -->
						<li><a href="#perfil" data-toggle="tab">Modificar perfil</a></li>
					</ul>
				</h2>
			</div>
			<div class="panel-body">
				<div class="tab-content">
					<div class="tab-pane active" id="timeline">
						<?php if($articulos !='0'):?>
						<ul class="timeline">
						<?php foreach($articulos as $articulo):?>
							<li class="timeline-success">
								<div class="timeline-icon"><i class="fa fa-pencil"></i></div>
								<div class="timeline-body">
									<div class="timeline-header">
										<!-- <span class="author">Posted by <a href="extras-profile.html#">David Tennant</a></span> -->
										<span class="date"><?=strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($articulo->fecha))?></span>
									</div>
									<div class="timeline-content">
										<h3><?=$articulo->titulo?> <small>en <?=$articulo->cat?></small></h3>
										<?=word_limiter($articulo->cuerpo,100)?>
									</div>
									<div class="timeline-footer">
										<a href="<?=site_url('articulo/'.$articulo->alias)?>" class="btn-link pull-left" target="_blank">Leer completo</a>
									</div>
								</div>
							</li>
						<?php endforeach;?>
						</ul>
						<?php else:?>
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">No se encontraron artículos</h3>
								</div>
								<div class="panel-body">
									El usuario no tiene artículos registrados
								</div>
							</div>
						<?php endif;?>
					</div>
					<!-- <div class="tab-pane active" id="timeline2">
						<?php if($articulos !='0'):?>
						<ul class="timeline">
						<?php foreach($articulos as $articulo):?>
							<li class="timeline-success">
								<div class="timeline-icon"><i class="fa fa-pencil"></i></div>
								<div class="timeline-body">
									<div class="timeline-header">
										<span class="author">Posted by <a href="extras-profile.html#">David Tennant</a></span> 
										<span class="date"><?=strftime('%e de %B de %Y a las %l:%M:%S %p',strtotime($articulo->fecha))?></span>
									</div>
									<div class="timeline-content">
										<h3><?=$articulo->titulo?> <small>en <?=$articulo->cat?></small></h3>
										<?=word_limiter($articulo->cuerpo,100)?>
									</div>
									<div class="timeline-footer">
										<a href="extras-profile.html#" class="btn-link pull-left">Read Full Story</a>
									</div>
								</div>
							</li>
						<?php endforeach;?>
						</ul>
						<?php else:?>
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">Panel title</h3>
								</div>
								<div class="panel-body">
									Panel content
								</div>
							</div>
						<?php endif;?>
					</div> -->
					<div class="tab-pane" id="perfil">
						<div class="row">
							<div class="col-lg-8 col-lg-offset-2">
								<?=form_open('admin/usuario/editar/'.$fila[0]->id_usuario,array('id' =>'validate-form'))?>
								<div class="form-group">
									<?=form_label('Nombre completo','nombre')?>
									<?=form_input(array('name' => 'nombre','id'=>'nombre','class'=>'form-control','required'=>'required','value'=>$fila[0]->nombre))?>
								</div>
								<div class="form-group">
									<?=form_label('Usuario','usuario')?>
									<?=form_input(array('name'=>'usuario','id'=>'usuario','class'=>'form-control','required'=>'required','value'=>$fila[0]->usuario,'readonly'=>'readonly'))?>
								</div>
								<div class="form-group">
									<?=form_label('Correo','correo')?>
									<?=form_input(array('name'=>'correo','id'=>'correo','class'=>'form-control','required'=>'required','value'=>$fila[0]->correo))?>
								</div>
								<div class="form-group">
									<?=form_label('Teléfono','telefono')?>
									<?=form_input(array('name'=>'telefono','id'=>'telefono','class'=>'form-control','required'=>'required','value'=>$fila[0]->telefono ))?>
								</div>
								<div class="form-group">
									<?=form_label('Clave','clave')?>
									<input name="clave" type="password" class="form-control" >
								</div>
								<div class="form-group">
									<label>Permisos</label>
									<div class="btn-group mb10 btn-group-justified" data-toggle="buttons">
                            			<label class="btn btn-orange <?=($fila[0]->permisos)==1 ? 'active' : '' ?>">
                                			<input type="radio" name="permiso" value="1" <?=($fila[0]->permisos)==1 ? 'checked' : '' ?>> Administrador
                            			</label>
			                            <label class="btn btn-purple <?=($fila[0]->permisos)==0 ? 'active' : '' ?>">
			                                <input type="radio" name="permiso" value="0" <?=($fila[0]->permisos)==0 ? 'checked' : '' ?>> Usuario 
			                            </label>
                        			</div>
								</div>
								<?php 
									if($this->session->userdata('usuario')['id_usuario'] == $fila[0]->id_usuario){
											echo form_hidden('config',$fila[0]->config);
									}
								?>
								<div class="panel-footer text-center">
									<button type="submit" class="btn btn-success btn-lg" id="submit">Guardar</button>
								</div>
								<?=form_close()?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>