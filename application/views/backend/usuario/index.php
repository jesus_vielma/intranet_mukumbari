<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Usuarios</h1>
				<div class="options">
					<div class="btn-toolbar">
						<a href="<?=site_url('admin/usuario/nuevo')?>" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Nuevo</a>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<?php if ($filas !=0) :?>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h2>Usuarios</h2>
									<div class="panel-ctrls">
									</div>
								</div>
								<div class="panel-body panel-no-padding">
									<table id="tabla" class="table table-striped table-bordered" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>Usuario</th>
												<th>Nombre</th>
												<th>Permisos</th>
												<th>Configurado</th>
												<th>Acciones</th>
											</tr>
										</thead>
										<tbody>
								<?php foreach ($filas as $fila):?>
											<tr class="odd gradeX">
												<td><?=$fila->usuario?></td>
												<td><?=$fila->nombre?></td>
												<td><?=($fila->permisos)==0 ? 'Usuario' : 'Administrador'?></td>
												<td><i class="fa fa-<?=($fila->config)==3 ? 'check text-success fa-2x' : 'times text-warning fa-2x'?>"></i></td>
												<td><a href="<?=site_url('admin/usuario/perfil/'.$fila->id_usuario)?>">Ver perfil</a></td>
											   </tr>
								<?php endforeach;?>
										</tbody>
									</table>
									<div class="panel-footer"></div>
								</div>

							</div>
						</div>
					</div>
				<?php else: ?>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="panel panel-inverse">
								<div class="panel-heading">
									<h2>Departamentos</h2>
									<div class="panel-ctrls">
									</div>
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<h1>No se encuentra información</h1>
									</div>
									<div class="panel-footer">
										<a href="<?=site_url('admin/departamento/nuevo')?>" class="btn btn-primary">Nuevo </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>

			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
