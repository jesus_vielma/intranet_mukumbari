<!DOCTYPE html>
<html lang="en" class="coming-soon">
<head>
    <meta charset="utf-8">
    <title>Iniciar sesión administrador</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href="<?=base_url('assets/backend/plugins/iCheck/skins/minimal/blue.css')?>" type="text/css" rel="stylesheet">
    <link href="<?=base_url('assets/backend/fonts/font-awesome/css/font-awesome.min.css')?>" type="text/css" rel="stylesheet">
    <link href="<?=base_url('assets/backend/css/styles.css')?>" type="text/css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
    <!--[if lt IE 9]>
        <link href="assets/css/ie8.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The following CSS are included as plugins and can be removed if unused-->

    </head>

    <body class="focused-form">


<div class="container" id="login-form">
	<a href="" class="login-logo"><h1>Administrador <br>IntranetMukumbari</h1></a>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading"><h2>Iniciar sesión</h2></div>
				<div class="panel-body">

					<form action="<?=site_url('admin/backend/login/2')?>" class="form-horizontal" id="validate-form" method="post" data-parsley-errors-container="#errorContainer">
						<?=validation_errors()?>
						<?php if(isset($error)):?>
							<div class="alert alert-danger">
								<?=$error?>
							</div>
                        <?php elseif(isset($_GET['ref'])):?>
                        <div class="alert alert-info">
                            <span>Necesita haber iniciado sesión para ver esa página</span>
                        </div>
                        <input type="hidden" name="pag_ant" value="<?=$_GET['ref']?>">
                        <?php endif;?>
						<div class="form-group">
	                        <div class="col-xs-12">
	                        	<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-user"></i>
									</span>
									<input type="text" class="form-control" placeholder="Usuario" required name="usuario">
								</div>
	                        </div>
						</div>

						<div class="form-group">
	                        <div class="col-xs-12">
	                        	<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-key"></i>
									</span>
									<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Clave" name="clave" required>
								</div>
	                        </div>
						</div>

						<!-- <div class="form-group">
							<div class="col-xs-12">
								<a href="extras-forgotpassword.html" class="pull-left">Forgot password?</a>
								<div class="checkbox-inline icheck pull-right pt0">
									<label for="">
										<input type="checkbox"></input>
										Remember me
									</label>
								</div>
							</div>
						</div> -->


						<div class="panel-footer">
							<div class="clearfix text-center">
								<!-- <a href="extras-registration.html" class="btn btn-default pull-left">Register</a> -->
								<button class="btn btn-primary" type="submit">Iniciar sesión</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>



    <!-- Load site level scripts -->

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->

<script src="<?=base_url('assets/backend/js/jquery-1.10.2.min.js')?>"></script> 							<!-- Load jQuery -->
<script src="<?=base_url('assets/backend/js/jqueryui-1.9.2.min.js')?>"></script> 							<!-- Load jQueryUI -->

<script src="<?=base_url('assets/backend/js/bootstrap.min.js')?>"></script> 								<!-- Load Bootstrap -->


<script src="<?=base_url('assets/backend/js/enquire.min.js')?>"></script> 									<!-- Enquire for Responsiveness -->

<script src="<?=base_url('assets/backend/plugins/bootbox/bootbox.js')?>"></script>							<!-- Bootbox -->

<script src="<?=base_url('assets/backend/plugins/simpleWeather/jquery.simpleWeather.min.js')?>"></script> <!-- Weather plugin-->

<script src="<?=base_url('assets/backend/plugins/nanoScroller/js/jquery.nanoscroller.min.js')?>"></script> <!-- nano scroller -->

<script src="<?=base_url('assets/backend/plugins/jquery-mousewheel/jquery.mousewheel.min.js')?>"></script> 	<!-- Mousewheel support needed for jScrollPane -->

<script src="<?=base_url('assets/backend/js/application.js')?>"></script>

<!-- End loading site level scripts -->
    <!-- Load page level scripts-->
	<script>
	// See Docs
		window.ParsleyConfig = {
	    	  successClass: 'has-success'
			, errorClass: 'has-error'
			, errorElem: '<span></span>'
			, errorsWrapper: '<span class="help-block"></span>'
			, errorTemplate: "<div></div>"
			, classHandler: function(el) {
	    		return el.$element.closest(".form-group");
			}
		};
	</script>
<script src="<?=base_url('assets/backend/plugins/parsley/parsley.js')?>"></script>
<script src="<?=base_url('assets/backend/plugins/parsley/es.js')?>"></script>
<script src="<?=base_url('assets/backend/plugins/parsley/demo-formvalidation.js')?>"></script>

    <!-- End loading page level scripts-->
    </body>
</html>
