<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Departamento</h1>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2>Nuevo departamento</h2>
								<div class="panel-ctrls">
								</div>
							</div>
							<div class="panel-body ">
								<?=form_open_multipart('admin/departamento/nuevo/2',array('id' =>'validate-form'))?>
								<?=validation_errors()?>
								<?php if (isset($error)):?>
									<div class="alert alert-danger">
										<?=$error?>
									</div>
								<?php endif; ?>
								<div class="form-group">
									<?=form_label('Nombre','nombre')?>
									<?=form_input(array('name' => 'nombre','id'=>'nombre','class'=>'form-control','required'=>'required') )?>
								</div>
								<div class="form-group">
									<?=form_label('Seleccione una imagen','organigrama')?>
									<?=form_input(array('name' => 'organigrama','id'=>'organigrama','required'=>'required','type'=>'file'))?>
								</div>
								<div class="form-group">
									<?=form_label('Nivel en el organigrama general','nivel')?>
									<?=form_input(array('name'=>'nivel','id'=>'nivel','class'=>'form-control','required'=>'required' ))?>
								</div>
								<div class="form-group">
									<?=form_label('Descripción','descripcion')?>
					                <textarea name="descripcion" rows="10" placeholder="Descripción" id='descripcion' required=""></textarea>
					            </div>

								<div class="panel-footer text-center">
									<button type="submit" class="btn btn-success btn-lg" id="submit">Guardar</button>
								</div>
								<?=form_close()?>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
