<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Departamento</h1>
			</div>
			<div class="container-fluid">
				<?php if(isset($erroresVincular)):?>
					<div class="row">
						<div class="col-lg-6 col-lg-offset-3">
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h3>No se pueden vincular algunos usuarios</h3>
								<p align="justify">Esta es la lista de usuarios que no hemos podido vincular a este departamento puesto que estan vinculados a otro departamento, para vincularlos a este departamento deben ser desvinculados del departamento en que estan actualmente.</p>
								<ul>
									<?php foreach ($erroresVincular as $a) :?>
										<li><?=$a[0]->nombre?></li>
									<?php endforeach;?>
								</ul>
							</div>
						</div>
					</div>
				<?Php endif;?>
				<?php if ($fila !=0) :?>
					<div class="row">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h2><?=$fila[0]->nombre?></h2>
									<div class="panel-ctrls">
										<span class="badge badge-primary"><?=$fila[0]->nivel?></span>
									</div>
								</div>
								<div class="panel-body ">
									<h3>Organigrama interno</h3>
									<img src="<?=base_url('assets/uploads/'.$fila[0]->organigrama)?>" alt="" class="img-responsive">
									<h3>Descripción y funciones</h3>
									<?=$fila[0]->descripcion?>
									<div class="panel-footer text-center">
										<a href="<?=site_url('admin/departamento/editar/1/'.$fila[0]->id_departamento)?>" class="btn btn-primary">Editar la información mostrada</a>
									</div>
								</div>

							</div>
						</div>
						<div class="col-lg-4">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h2 class="panel-title">Usuarios en esta coordinación</h2>
								</div>
								<div class="panel-body">
									<?php if($usuarios!='0'):?>
										<table class="table table-hover">
											<thead>
												<tr>
													<th>Nombre</th>
													<th width="10%">Acción</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach($usuarios as $usuario):?>
												<tr>
													<td><?=$usuario->persona?></td>
													<?php if($usuario->estado =='1'):?>
													<td>
													<?php echo form_open('admin/departamento/desvincular/'); 
													echo form_hidden('id_departamento',$fila[0]->id_departamento);
													echo form_hidden('id_usuario',$usuario->id);
													echo "<button type='submit' title='Desvincular' class='btn btn-orange btn-sm'><i class='fa fa-unlink'></i></button>";
													echo form_close();
													?>
													</td>
													<?php else:?>
													<td>
													<?php echo form_open('admin/departamento/vincular/2'); 
													echo form_hidden('id_departamento',$fila[0]->id_departamento);
													echo form_hidden('id_usuario',$usuario->id);
													echo "<button type='submit' title='Volver a vincular' class='btn btn-success btn-sm'><i class='fa fa-link'></i></button>";
													echo form_close();
													?>
													</td>
													<?php endif;?>
												</tr>
												<?php $a[] =$usuario->id;?>
												<?php endforeach;?>
											</tbody>
										</table>
									<?php else:?>
										No hay personas asociadas a esta coordinación.
									<?php endif;?>
									<div class="panel-footer text-center">
										<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Vincular otro usuario</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
<!-- Button trigger modal -->


<?php
if($listaUsuarios!='0'){

//$option = "<option value=''>- Seleccione -</option>";
$option ='';
foreach ($listaUsuarios as $usuario ) {
	//$UsuariosSelect[$usuario->id_usuario] = $usuario->nombre;
	if(isset($a)){
		if(in_array($usuario->id_usuario, $a))
			$option.= "<option value='".$usuario->id_usuario."' selected disabled>".$usuario->nombre."</option>";
		else{
			$option.= "<option value='".$usuario->id_usuario."'>".$usuario->nombre."</option>";
		}
	}
	else
		$option.= "<option value='".$usuario->id_usuario."'>".$usuario->nombre."</option>";
}
}
?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Vincular usuarios</h4>
      </div>
      <div class="modal-body">
        <?=form_open('admin/departamento/vincular/1',array('id'=>'validate-form'))?>
        <div class="form-group">
        	<?=form_label('Usuarios','usuarios')?>
        	<!--<?=form_multiselect('usuarios[]',$UsuariosSelect,$a,'class="form-control" id="multi-select" required')?>-->
        	<select class="form-control" required="" name="usuarios[]" id="multi-select" multiple="">
        		<?=$option?>
        	</select>
        	<?=form_hidden('id_departamento',$fila[0]->id_departamento)?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
        <?=form_close()?>
      </div>
    </div>
  </div>
</div>