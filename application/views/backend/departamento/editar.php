<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Departamento</h1>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h2>Nuevo departamento</h2>
								<div class="panel-ctrls">
								</div>
							</div>
							<div class="panel-body ">
								<?=form_open_multipart('admin/departamento/editar/2/'.$fila[0]->id_departamento,array('id' =>'validate-form'))?>
								<?=validation_errors()?>
								<?php if (isset($error)):?>
									<div class="alert alert-danger">
										<?=$error?>
									</div>
								<?php endif; ?>
								<div class="form-group">
									<?=form_label('Nombre','nombre')?>
									<?=form_input(array('name' => 'nombre','id'=>'nombre','class'=>'form-control','required'=>'required','value'=>(set_value('nombre'))!='' ? set_value('nombre') : $fila[0]->nombre) )?>
								</div>
								<div class="form-group" id="orgSel">
									<label class="control-label">¿Que desea hacer con el organigrama?</label>
									<div class="icheck-minimal">
										<div class="radio-inline green icheck">
											<label id="opcionOrg1">
												<input type="radio" name="opcionOrg" value="nuevo" id="opcionOrg1" >
												Nuevo
											</label>
										</div>
										<div class="radio-inline blue icheck">
											<label id="opcionOrg2">
												<input type="radio" name="opcionOrg" value="actual" id='opcionOrg' checked>
												Mantener actual
											</label>
										</div>
									</div>
								</div>
								<div class="form-group hide" id="org" style="display:none">
									<?=form_input(array('name' => 'organigrama','id'=>'organigrama','type'=>'file'))?>
									<input type="hidden" name="organigrama1" value="<?=$fila[0]->organigrama?>">
								</div>
								<div class="form-group">
									<?=form_label('Nivel en el organigrama general','nivel')?>
									<?=form_input(array('name'=>'nivel','id'=>'nivel','class'=>'form-control','required'=>'required','value'=>(set_value('nivel'))!='' ? set_value('nivel') : $fila[0]->nivel) )?>
								</div>
								<div class="form-group">
									<?=form_label('Descripción','descripcion')?>
					                <textarea name="descripcion" rows="15" placeholder="Descripción" id='descripcion' required><?=$fila[0]->descripcion?></textarea>
					            </div>
					            <?=form_hidden('id_departamento',$fila[0]->id_departamento)?>
								<div class="panel-footer text-center">
									<button type="submit" class="btn btn-success btn-lg" id="submit">Guardar</button>
								</div>
								<?=form_close()?>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
