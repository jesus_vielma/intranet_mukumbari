<div class="static-content-wrapper">
	<div class="static-content">
		<div class="page-content">
			<!--<ol class="breadcrumb">

				<li class=""><a href="index.html">Home</a></li>
				<li class="active"><a href="index.html">Dashboard</a></li>

			</ol>-->
			<div class="page-heading">
				<h1>Departamento</h1>
				<div class="options">
					<div class="btn-toolbar">
						<a href="<?=site_url('admin/departamento/nuevo')?>" class="btn btn-primary"><i class="fa fa-fw fa-plus"></i> Nuevo</a>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<?php if ($filas !=0) :?>
					<div class="row">
						<?php foreach ($filas as $fila):?>
							<div class="col-md-4">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h2><?=$fila->nombre?></h2>
									</div>
									<div class="panel-body">
										<?=word_limiter($fila->descripcion,35,'');?>
										<div class="panel-footer">
											<a href="<?=site_url('admin/departamento/detalle/'.$fila->id_departamento)?>">Ver información completa</a>
										</div>
									</div>	
								</div>
							</div>
						<?php endforeach;?>
					</div>
				<?php else: ?>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="panel panel-inverse">
								<div class="panel-heading">
									<h2>Departamentos</h2>
									<div class="panel-ctrls">
									</div>
								</div>
								<div class="panel-body">
									<div class="alert alert-info">
										<h1>No se encuentra información</h1>
									</div>
									<div class="panel-footer">
										<a href="<?=site_url('admin/departamento/nuevo')?>" class="btn btn-primary">Nuevo </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>

			</div>
			<!-- .container-fluid -->
		</div>
		<!-- #page-content -->
	</div>
