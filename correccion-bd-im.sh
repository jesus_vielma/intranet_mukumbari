#!/bin/bash
echo "Bienvenido (a)";
echo "Ingrese el usuario de base de datos ej. usu presione [ENTER]";
read usu; 
echo "Ingrese la clave de usuario $usu y presione [ENTER]";
read clave; 
echo "la ruta completa del del respado de la bd y presione [ENTER]";
read respaldo; 
echo "Comienza proceso de truncado de tablas";
mysql -u $usu --password=$clave -Nse "show tables" intranet_m | while read table; do mysql -u $usu --password=$clave -e "SET FOREIGN_KEY_CHECKS =0; truncate table $table" intranet_m; done;
echo "Comienza proceso de restauracion de la base de datos";
mysql -u $usu --password=$clave --init-command="SET SESSION FOREIGN_KEY_CHECKS =0;" intranet_m < $respaldo;
echo "Proceso terminado, Feliz Día";
