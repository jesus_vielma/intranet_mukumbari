-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 12-05-2017 a las 08:42:39
-- Versión del servidor: 5.5.54-0+deb8u1
-- Versión de PHP: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `intranet_m`
--

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`id_articulo`, `titulo`, `fecha`, `cuerpo`, `alias`, `id_categoria`, `id_usuario`) VALUES
(1, 'Estructura Organizacional', '2017-02-28 00:00:00', '<h1 style="text-align: center;">Organigrama general</h1>\r\n<p><em><strong>Haga clic sobre el el cuadro de la coordinaci&oacute;n que desea ver</strong></em></p>\r\n<p><img class="img-responsive" src="/intranet-m/assets/uploads/estructura/Organigrama-Mukumbari.png" alt="Estructura organizativa" usemap="#imgmap201731516335" width="803" height="503" border="0" /><map id="imgmap201731516335" name="imgmap201731516335"> \r\n<area alt="Gerencia Mukumbar&iacute; STM" coords="351,4,495,74" shape="rect" target="" />\r\n \r\n<area alt="Oficina de Atenci&oacute;n al Ciudadano" coords="438,112,580,180" shape="rect" target="" />\r\n \r\n<area alt="Coordinaci&oacute;n Gesti&oacute;n Administrativa" coords="264,212,406,280" shape="rect" target="" />\r\n \r\n<area alt="Coordinaci&oacute;n Gesti&oacute;n Humana" coords="440,212,578,282" shape="rect" target="" />\r\n \r\n<area alt="Coordinaci&oacute;n de Seguridad F&iacute;sica e Industrial " coords="266,308,406,374" shape="rect" target="" />\r\n \r\n<area alt="Coordinaci&oacute;n Gesti&oacute;n Tecnol&oacute;gica" coords="440,308,578,376" shape="rect" target="" />\r\n \r\n<area alt="Coordinaci&oacute;n de Operaciones Electromec&aacute;nicas" coords="102,406,298,482" shape="rect" target="" />\r\n \r\n<area alt="Coordinaci&oacute;n Servicios Turistico" coords="332,403,510,479" shape="rect" target="" />\r\n \r\n<area alt="Coordinaci&oacute;n de Comercializaci&oacute;n" coords="566,413,739,479" shape="rect" target="" />\r\n \r\n<area alt="Consultoria Juridica" coords="271,118,413,187" shape="rect" target="" />\r\n</map></p>\r\n<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1"><br />\r\n<div class="modal-dialog modal-lg">\r\n<div class="modal-content">\r\n<div class="modal-header"><button class="close" type="button" data-dismiss="modal">&times;</button>\r\n<h4 id="titulo" class="modal-title"></h4></div>\r\n<div class="modal-body"><img id="imagen" class="img-responsive" border="0" />\r\n<div id="cuerpo"></div>\r\n<div class="modal-footer"><button id="quitarModal" class="btn btn-default" type="button" data-dismiss="modal">Salir</button></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 'estructura_organizacional', 3, 1),
(5, 'Indicativos de llamadas y comandos de telecomunicación', '2017-04-12 10:57:10', '<p><img class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" src="/intranet-m/assets/uploads/1.png" alt="" width="1100" height="758" /></p>', 'indicativos_de_llamadas_y_comandos_de_telecomunicacion', 5, 2),
(6, 'Extenciones Ip de telefonía e interfonia STM', '2017-04-12 10:58:47', '<p><img class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" src="/intranet-m/assets/uploads/ip.png" alt="" width="522" height="583" /></p>', 'extenciones_ip_de_telefonia_e_interfonia_stm', 5, 2),
(7, 'Extenciones telefonía, interfonia y megafono STM', '2017-04-12 11:02:53', '<p><img class="img-responsive" style="float: left;" src="/intranet-m/assets/uploads/megafno.png" alt="" width="317" height="354" /><img class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" src="/intranet-m/assets/uploads/mega.png" alt="" width="323" height="360" /></p>\r\n<p><img class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" src="/intranet-m/assets/uploads/otraparte.png" alt="" width="567" height="398" /></p>\r\n<p><strong>Descargar documento:&nbsp;<a title="Extensiones Telefonos IP y Megafonos" href="/intranet-m/assets/uploads/ExtensionesTelefonosIPyMegafonos.pdf">/intranet-m/assets/uploads/ExtensionesTelefonosIPyMegafonos.pdf</a></strong></p>', 'extenciones_telefonia_interfonia_y_megafono_stm', 5, 2),
(8, 'Boletín Turístico # 28  Coordinación de Comercialización', '2017-04-23 14:48:10', '<p>BUen dia se adjunta el boletin turistico #28</p>\r\n<p><a href="/intranet-m/assets/uploads/Boleti%CC%81n%20ViajANDO%20N%C2%AA%20%2028.pdf">/intranet-m/assets/uploads/Boleti%CC%81n%20ViajANDO%20N%C2%AA%20%2028.pdf</a></p>', 'boletin_turistico_28_coordinacion_de_comercializacion', 5, 11),
(9, 'Enlace de las notas de prensa publicados en la página de Mukumbarí STM', '2017-04-26 18:02:53', '<p>Saludos,</p>\r\n<p>adjunto enlace de la nota de prensa referente a los nuevos emprendimientos, publicada en la p&aacute;gina de Mukumbar&iacute; STM.&nbsp;</p>\r\n<p><a title="NUEVOS EMPRENDIMIENTOS TIENE TELEF&Eacute;RICO MUKUMBAR&Iacute; COMO OFERTA TUR&Iacute;STICA" href="http://mukumbari.com/prensa/post/nuevos-emprendimientos-tiene-teleferico-mukumbari-como-oferta-turistica" target="_blank" rel="noopener noreferrer">Nuevos emprendimientos como oferta tur&iacute;stica</a></p>', 'enlace_de_las_notas_de_prensa_publicados_en_la_pagina_de_mukumbari', 1, 12),
(10, 'Enlace de las noticias publicadas en la página de Mukumbarí STM', '2017-04-26 18:07:02', '<p>Saludos,&nbsp;</p>\r\n<p>adjunto enlace de las noticias publicadas en la p&aacute;gina de Mukumbar&iacute; STM&nbsp;</p>\r\n<p><a title="Noticias" href="http://mukumbari.com/noticias" target="_blank" rel="noopener noreferrer">http://mukumbari.com/noticias</a></p>', 'enlace_de_las_noticias_publicadas_en_la_pagina_de_mukumbari', 5, 12),
(12, 'Boletín Viajando numero 29 emitido por MINTUR', '2017-04-27 11:25:11', '<p>&nbsp;</p>\r\n<p><img class="img-responsive" src="/intranet-m/assets/uploads/Captura%20de%20pantalla%20de%202017-04-27%2015%3A30%3A37.png" alt="" width="514" height="663" /></p>\r\n<p><a href="/intranet-m/assets/uploads/Boleti%CC%81n%20ViajANDO%20N%C2%BA%2029%2026.4.2017%20(1).pdf">/intranet-m/assets/uploads/Boleti%CC%81n%20ViajANDO%20N%C2%BA%2029%2026.4.2017%20(1).pdf</a></p>\r\n<p>&nbsp;</p>\r\n<p>Buenos D&iacute;as se adjunbta el Bolet&iacute;n Viajando n&uacute;mero 29, en el mismo se hace referencia a la activacion de los nuevos emprendimientos turisticos del MUKUMBAR&Iuml;</p>\r\n<p>Saludos Cordiaes&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Miguel Rangel&nbsp;</p>', 'boletin_viajando_numero_29_emitido_por_mintur', 1, 11),
(13, 'JORNADA DE ALIMENTACIÓN CLAP OBRERO MUKUMBARÍ STM', '2017-04-27 16:15:01', '<p><strong><img class="img-responsive" src="/intranet-m/assets/uploads/CLAP%20%23-1.jpg" alt="" width="2539" height="1653" /><img class="img-responsive" src="/intranet-m/assets/uploads/CLAP%20%23-2.jpg" width="2539" height="1653" /></strong></p>\r\n<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1">\r\n<div class="modal-dialog modal-lg">\r\n<div class="modal-content">\r\n<div class="modal-header"><button class="close" type="button" data-dismiss="modal">&times;</button>\r\n<h4 id="titulo" class="modal-title">&nbsp;</h4>\r\n</div>\r\n<div class="modal-body"><img id="imagen" /><br />\r\n<div id="cuerpo">&nbsp;</div>\r\n</div>\r\n<div class="modal-footer"><button id="quitarModal" class="btn btn-default" type="button" data-dismiss="modal">Salir</button></div>\r\n</div>\r\n</div>\r\n</div>', 'jornada_de_alimentacion_clap_obrero_mukumbari', 1, 20),
(15, 'Nuevos emprendimientos como oferta turística', '2017-04-28 09:33:40', '<p style="text-align: justify;">&nbsp;</p>\r\n<p style="text-align: justify;"><img class="img-responsive" src="/intranet-m/assets/uploads/otra.png" alt="" width="319" height="209" />&nbsp; &nbsp;<img class="img-responsive" src="/intranet-m/assets/uploads/sendero.png" alt="" width="380" height="334" />&nbsp;&nbsp;<img class="img-responsive" src="/intranet-m/assets/uploads/sendero2.png" alt="" width="387" height="314" /></p>\r\n<p style="text-align: justify;"><strong>Sendero a la Laguna de Los Anteojos y Bosque de Los Coloraditos</strong>: Se puede conocer el origen de la formaci&oacute;n de las lagunas mediante una m&aacute;gica caminata dentro del ecosistema de p&aacute;ramo altiandino, donde adem&aacute;s se penetra en un singular bosque de &aacute;rboles con troncos rojizos.</p>\r\n<p style="text-align: justify;"><span lang="es-ES"><strong>El Alto de la Cruz: </strong></span><span lang="es-ES">En la ruta que conduce al pueblo de </span><span lang="es-ES"><strong>Los Nevados</strong></span><span lang="es-ES">, los amantes del senderismo tienen el desaf&iacute;o de subir por un empinado camino entre los 4 </span><span lang="es-ES"><strong>mil y 4 mil 500 metros sobre el nivel del mar</strong></span><span lang="es-ES"> (msnm), al ras de los picos m&aacute;s elevados de </span><span lang="es-ES"><strong>Venezuela</strong></span><span lang="es-ES">, tras observar la </span><span lang="es-ES"><strong>Laguna</strong></span> <span lang="es-ES"><strong>de Los Anteojos y Bosque de Los Coloraditos.</strong></span></p>\r\n<p style="text-align: justify;"><span lang="es-ES"><strong>Camino tem&aacute;tico &ldquo;Pobladores ind&iacute;genas&rdquo;, </strong></span><span lang="es-ES">entre las estaciones</span><span lang="es-ES"><strong> Loma Redonda (4&ordf;) y La Aguada (3&ordf;): </strong></span><span lang="es-ES">A trav&eacute;s de un amigable descenso caminando, familias y amigos aventureros pueden apreciar</span> la transici&oacute;n entre dos ecosistemas dentro del p&aacute;ramo meride&ntilde;o.</p>', 'nuevos_emprendimientos_como_oferta_turistica', 1, 2),
(16, 'Solicitud de plan de trabajo de las coordinaciones', '2017-05-04 11:46:28', '<p>Saludos cordiales.</p>\r\n<p>La presente es para solicitar a todas las coordinaciones el plan de trabajo, a fin de determinar la informaci&oacute;n para generar los contenidos que formar&aacute;n parte del plan de redes sociales, considerando que la comunicaci&oacute;n es un &aacute;rea transversar a todas las dem&aacute;s.</p>\r\n<p>Atentamente,&nbsp;</p>\r\n<p>Lic. Leida T. Salcedo</p>\r\n<p>Gestora de Redes Sociales Mukumbar&iacute; STM</p>', 'solicitud_de_plan_de_trabajo_de_las coordinaciones', 1, 12),
(17, 'Boletín Viajando numero 30 emitido por MINTUR', '2017-05-05 14:31:07', '<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><a href="/intranet-m/assets/uploads/Boleti%CC%81n%20ViajANDO%20N%C2%BA%2029%2026.4.2017%20(1).pdf"><img class="img-responsive" src="/intranet-m/assets/uploads/Captura%20de%20pantalla%20de%202017-05-05%2014%3A28%3A12.png" alt="" width="501" height="580" /></a></p>\r\n<p>&nbsp;</p>\r\n<p><a href="/intranet-m/assets/uploads/Boleti%CC%81n%20Digital%20ViajANDO%20N%2030_2.pdf">/intranet-m/assets/uploads/Boleti%CC%81n%20Digital%20ViajANDO%20N%2030_2.pdf</a></p>', 'boletin_viajando_numero_30_emitido_por_mintur', 1, 11),
(18, 'Correo electrónico para enviar plan de trabajo de las Coordinaciones', '2017-05-07 11:00:00', '<p style="text-align: justify;">Saludos cordiales,</p>\r\n<p style="text-align: justify;">Por instrucciones del Coordinador de Comercializaci&oacute;n y Comunicaciones Miguel Rangel, se solicita que el plan de trabajo de todas las coordinaciones sea enviado al correo stmcomercializacion@gmail.com, a fin de determinar los contenidos que formar&aacute;n parte del Plan de Redes Sociales, de acuerdo a los objetivos planteados en la Estructura Organizacional de Mukumbar&iacute; STM, especialmente para la Coordinaci&oacute;n de Comercializaci&oacute;n.</p>\r\n<p style="text-align: justify;">1. Coordinar, dirigir y supervisar la implementaci&oacute;n de los lineamientos y estrategias del plan de comercializaci&oacute;n dise&ntilde;ado para la promoci&oacute;n y marketing de los productos, servicios y experiencias tur&iacute;stcas ofrecidas por VENTEL, C. A., a los turistas y visitantes del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</p>\r\n<p style="text-align: justify;"><br />2. Desarrollar y coordinar un sistema de precios y tarifas para los productos, servicios y experiencias tur&iacute;stcas del parque, a fin de garantzar el cumplimiento del plan de comercializaci&oacute;n del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</p>\r\n<p style="text-align: justify;"><br />3. Coordinar y supervisar la aplicaci&oacute;n de instrumentos, entrevistas y cuestonarios con el prop&oacute;sito de examinar la diferencias demogr&aacute;ficas, psicogr&aacute;ficas y de comportamiento de los visitantes y turistas, as&iacute; como su satsfacci&oacute;n con los atributos de calidad de servicios y productos tur&iacute;stcos ofrecidos en el Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</p>\r\n<p style="text-align: justify;"><br />4. Coordinar supervisar la implementaci&oacute;n de los planes, programas y proyectos dirigidos a la promoci&oacute;n de la imagen, las experiencias y servicios tur&iacute;stcos del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, con el prop&oacute;sito de incrementar la asistencia de los visitantes y turistas a los distintos destinos tur&iacute;sticos administrados por la empresa</p>\r\n<p style="text-align: justify;"><br />5. Programar, coordinar y supervisar investigaciones de mercado relacionados con la oferta tur&iacute;stca del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida que permitan la comprensi&oacute;n, anticipaci&oacute;n y toma de decisiones sobre la evoluci&oacute;n de los segmentos de inter&eacute;s tur&iacute;stcos en el &aacute;mbito regional, nacional e internacional.</p>\r\n<p style="text-align: justify;"><br />6. Dise&ntilde;ar, controlar y hacer seguimiento de la segmentaci&oacute;n del mercado social tur&iacute;stico, a fin de ofrecer servicios y experiencias tur&iacute;stcas dirigidas a satisfacer las necesidades de los grupos de inter&eacute;s que eligen los destinos tur&iacute;sticos vinculados al Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</p>\r\n<p style="text-align: justify;">Sin m&aacute;s a que hacer referencia,</p>\r\n<p style="text-align: justify;">atentamente,&nbsp;</p>\r\n<p style="text-align: justify;">Lcda. Leida Tamara Salcedo</p>\r\n<p style="text-align: justify;">Gestora de Redes Sociales Mukumbar&iacute; STM</p>', 'correo_electronico_para_enviar_plan_de_trabajo_de_las_coordinaciones', 1, 12),
(19, 'LLEGADA DEL ESPECIALISTA DE CABLE', '2017-05-10 10:01:26', '<p>El dia jueves 11/05/17, el sistema de carga del primer tramo no prestara servicio ya que estara en un proceso de revisi&oacute;n de&nbsp;los cables tractores y portadores, por un especialista de la empresa constructora. Asi que por favor tomen sus previsiones al respecto</p>', 'llegada_del_especialista_de_cable', 1, 23);

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre`, `alias`, `default`) VALUES
(1, 'Noticias', 'noticias', 1),
(2, 'Empleado del mes', 'empleado_del_mes', 0),
(3, 'Estructura Organizacional', 'estructura_organizacional', 0),
(5, 'Telecomunicaciones', 'Telecomunicaciones', 0);

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id_departamento`, `nombre`, `organigrama`, `descripcion`, `nivel`, `predeterminado`) VALUES
(1, 'Gerencia Mukumbarí STM', 'estructura/Gerencia.png', '<p><span style="font-size: small;"><strong><span style="color: #000000;"><span style="font-family: arial, sans-serif;">Gerente: Coronel Luis Rafael Moreno Machado<br /><br /><br /></span></span></strong></span></p>\r\n<ol>\r\n<li style="text-align: justify;">Planificar, evaluar y hacer seguimiento de la ejecuci&oacute;n de revisiones e inspecciones con el prop&oacute;sito determinar el estado general de las instalaciones en materia de infraestructura mec&aacute;nica, el&eacute;ctrica, electr&oacute;nica e hidr&aacute;ulica, a fin de implementar programas&nbsp;de mantenimiento preventvo o correctvo para el funcionamiento &oacute;ptmo del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li style="text-align: justify;">Planificar, verificar y controlar la correcta aplicaci&oacute;n del sistema de precios y tarifas establecidos para los productos, servicios y&nbsp;experiencias tur&iacute;stcas ofrecidas por VENTEL, C.A., a fin de garantzar el ingreso de recursos financieros que coadyuven a la&nbsp;sustentabilidad de la empresa.</li>\r\n<li style="text-align: justify;">Dirigir, promover y verificar la implementaci&oacute;n de los lineamientos, estrategias y metas del plan de comercializaci&oacute;n dise&ntilde;ado&nbsp;para la promoci&oacute;n y marketng de los productos, servicios y experiencias tur&iacute;stcas ofrecidas por VENTEL, C. A., a los turistas y&nbsp;visitantes del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>\r\n<li style="text-align: justify;">Administrar y controlar la gest&oacute;n de los recursos financieros obtenidos, en coordinaci&oacute;n con la Gerencia de Desarrollo Tur&iacute;stco&nbsp;de VENTEL, C.A., de la comercializaci&oacute;n de las experiencias tur&iacute;stcas, boleter&iacute;a, eventos culturales y gastron&oacute;micos realizados en&nbsp;los espacios del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>\r\n<li style="text-align: justify;">Planificar, controlar y hacer seguimiento del plan de inspecciones a los operadores que prestan servicios tur&iacute;stcos en los espacios f&iacute;sicos administrados por VENTEL, C. A., en el Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>\r\n<li style="text-align: justify;">Administrar y controlar el funcionamiento de los espacios de ventas f&iacute;sicos y virtuales necesarios para el intercambio comercial entre las empresas, comerciantes, visitantes, y turistas que desarrollan actvidades en el Mukumbar&iacute; Sistema Telef&eacute;rico de&nbsp;M&eacute;rida.&nbsp;</li>\r\n<li style="text-align: justify;">Desarrollar proyectos y programas que optmicen el proceso de convenir la adquisici&oacute;n de las experiencias tur&iacute;stcas y el disfrute&nbsp;de los destnos tur&iacute;stcos promocionados por VENTEL, C.A., con el prop&oacute;sito de incrementar progresivamente la afluencia de&nbsp;visitantes y turistas al Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.1. Planificar, controlar y hacer seguimiento de los procesos relacionados con la gest&oacute;n del talento humano en el Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, siguiendo los lineamientos emanados de la Gerencia de Gest&oacute;n Humana de VENTEL, C.A.,y&nbsp;lo establecido en la normatva legal vigente en materia de las relaciones sociales del trabajo.&nbsp;</li>\r\n<li style="text-align: justify;">Evaluar, controlar y hacer seguimiento de la implementaci&oacute;n de los planes, proyectos, normas y procedimientos que&nbsp;contribuyan a la prevenci&oacute;n de accidentes y enfermedades ocupacionales, as&iacute; como a la disminuci&oacute;n de riesgos mec&aacute;nicos&nbsp;o f&iacute;sicos provenientes del medio ambiente laboral.&nbsp;</li>\r\n<li style="text-align: justify;">Evaluar, controlar y hacer seguimiento de la implementaci&oacute;n de planes, proyectos, normas y procedimientos que permitan la detecci&oacute;n y prevenci&oacute;n de riesgos que afecten la integridad f&iacute;sica de los turistas y visitantes en los espacios&nbsp;f&iacute;sicos del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>\r\n<li style="text-align: justify;">Planificar, gestonar y controlar la operaci&oacute;n de los sistemas el&eacute;ctricos, mec&aacute;nicos, de redes, voz y datos que conforman la&nbsp;plataforma tecnol&oacute;gica y operatva del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, a fin de garantzar el desplazamiento&nbsp;seguro y confiable de los turistas y visitantes en los distntos espacios del parque.</li>\r\n</ol>', 'Estratégico', 1),
(2, 'Consultoria Juridica', 'estructura/juridica1.png', '<strong>Coordinadora:&nbsp;Gisela Silva</strong><br /><br />\r\n<ol>\r\n<li style="text-align: justify;">Velar por el cumplimiento de la Consttuci&oacute;n, Leyes, Decretos, Reglamentos, Instructvos y otras normatvas legales, relacionadas con las actvidades de la Organizaci&oacute;n y en general con las Leyes de la Rep&uacute;blica Bolivariana de Venezuela.</li>\r\n<li style="text-align: justify;">Asesorar jur&iacute;dicamente a la Gerencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, a solicitud de &eacute;sta, de las diferentes Coordinaciones y&nbsp;dem&aacute;s dependencias, sobre asuntos de car&aacute;cter legal y jur&iacute;dico&nbsp;</li>\r\n<li style="text-align: justify;">Asistr por instrucciones de la Consultor&iacute;a Jur&iacute;dica de VENTEL, C.A. a las reuniones en las cuales se requiera la presencia de la Consultor&iacute;a&nbsp;Jur&iacute;dica, con el fin de unificar los criterios tanto del Ejecutvo Nacional como del Ministro o Ministra, la directva de VENTEL C.A., en cuanto a&nbsp;las leyes en proceso de promulgaci&oacute;n, as&iacute; como de cualquier otro instrumento legal.&nbsp;</li>\r\n<li style="text-align: justify;">Realizar oportunamente, las gestones ante otros organismos p&uacute;blicos, relacionadas con actos administratvos dictados por este ente, as&iacute;&nbsp;como aquellos que en virtud de su contenido, sean inherentes a esta Organizaci&oacute;n.&nbsp;</li>\r\n<li style="text-align: justify;">Representar en todos los asuntos judiciales o extrajudiciales que conciernen a VENTEL, C.A., dentro del Estado Bolivariano de M&eacute;rida.</li>\r\n<li style="text-align: justify;">Elaborar por solicitud de la Gerencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida propuestas de convenios, contratos y cualquier otro&nbsp;documento de car&aacute;cter legal.</li>\r\n</ol>', 'Estretegico', 0),
(3, 'Coordinación Gestión Tecnológica', 'estructura/tecnologica.png', '<br /><strong>Coordinadora: &nbsp;Syra lacruz</strong><br />\r\n<ol>\r\n<li style="text-align: justify;">Coordinar, supervisar y hacer seguimiento de la detecci&oacute;n de necesidades y verificaci&oacute;n de requerimientos tecnol&oacute;gicos de las unidades organizatvas que conforman el Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, a fin de realizar un diagnostco de las&nbsp;demandas de hardware y softwares de la Gerencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li style="text-align: justify;">Supervisar y verificar el funcionamiento adecuado de las redes inform&aacute;tcas y de comunicaciones que ha desarrollado VENTEL,&nbsp;C.A. en los espacios f&iacute;sicos y virtuales, a fin de garantzar la operaci&oacute;n eficiente del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>\r\n<li style="text-align: justify;">Coordinar y supervisar la implementaci&oacute;n de pol&iacute;tcas que garantcen la seguridad e integridad de la informaci&oacute;n que se&nbsp;procesa y almacena en los servidores y estaciones de trabajo de la Gerencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>\r\n<li style="text-align: justify;">Programar , coordinar, y supervisar la actualizaci&oacute;n y mantenimiento de los sistemas inform&aacute;tcos, hardwares y softwares que soportan los procesos administratvos y operatvos de las unidades organizatvas que conforman la Gerencia del Mukumbar&iacute;&nbsp;Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>\r\n<li style="text-align: justify;">Dirigir y coordinar el dise&ntilde;o y desarrollo de proyectos para la adopci&oacute;n de soluciones tecnol&oacute;gicas que permitan la optmizaci&oacute;n&nbsp;de los procesos de gest&oacute;n, comercializaci&oacute;n y prestaci&oacute;n de servicios tur&iacute;stcos del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n</ol>', 'Apoyo', 0),
(4, 'Oficina de Atención al Ciudadano', 'estructura/atencion-ciudadano.png', '<strong>Coordinadora:&nbsp;Jenifer Rincon</strong><br />\r\n<ol>\r\n<li style="text-align: justify;">Promover la partcipaci&oacute;n ciudadana en las diversas &aacute;reas de inter&eacute;s del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida: social, tur&iacute;stca, cultural,&nbsp;educatva, deportva, ambiental, entre otras.&nbsp;</li>\r\n<li style="text-align: justify;">Apoyar, orientar, recibir y tramitar, las denuncias, quejas, reclamos, sugerencias y petciones formuladas por los ciudadanos al Mukumbar&iacute;&nbsp;Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>\r\n<li style="text-align: justify;">Fomentar la partcipaci&oacute;n de la poblaci&oacute;n con la finalidad de desarrollar iniciatvas que contribuyan en la ejecuci&oacute;n de los programas de turismo como actvidad comunitaria social.&nbsp;</li>\r\n<li style="text-align: justify;">Informar a la ciudadan&iacute;a sobre la utlizaci&oacute;n de los recursos que integran el patrimonio p&uacute;blico del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida,&nbsp;as&iacute; como tambi&eacute;n la difusi&oacute;n de valores socialistas a trav&eacute;s de la formaci&oacute;n y la sensibilizaci&oacute;n de los (as) trabajadores (as) de la empresa y la&nbsp;comunidad organizada.</li>\r\n</ol>', 'Estratégico', 0),
(9, 'Coordinación Gestión Administrativa', 'estructura/gestionadmin.png', '<strong>Coordinador: &nbsp;Osman Rodriguez</strong><br />\r\n<ol>\r\n<li>Supervisar, coordinar y verificar la gest&oacute;n, administraci&oacute;n y contabilizaci&oacute;n de los recursos financieros obtenidos mediante la comercializaci&oacute;n de las experiencias tur&iacute;stcas, venta de boleter&iacute;a, eventos culturales, eventos gastron&oacute;micos y dem&aacute;s actvidades comerciales realizadas en los espacios del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Coordinar, verificar y programar la adquisici&oacute;n de bienes y servicios requeridos por la Gerencia y divisiones que conforman el Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, considerando el marco jur&iacute;dico que regula los procesos de contrataciones del Estado as&iacute; como las normas y procedimientos existentes en la empresa, con el prop&oacute;sito de coadyuvar a la prestaci&oacute;n de servicios tur&iacute;stcos con excelentes atributos de calidad a los visitantes y turistas que acuden al Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Coordinar, supervisar y hacer seguimiento de la implementaci&oacute;n de las normas, lineamientos, directrices y pautas t&eacute;cnicas dictadas por la Superintendencia de Bienes P&uacute;blicos, relatvas a los bienes que conforman el patrimonio de la Gerencia de Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Coordinar, verificar y programar la tramitaci&oacute;n, elaboraci&oacute;n, declaraci&oacute;n y liquidaci&oacute;n de los impuestos y pagos a terceros de acuerdo a lo establecido en el ordenamiento jur&iacute;dico que regula la materia tributaria.</li>\r\n<li>Coordinar, supervisar y hacer seguimiento, conjuntamente con las divisiones de Comercializaci&oacute;n y Servicios Tur&iacute;stcos, de los ingresos percibidos por la venta de experiencias tur&iacute;stcas, servicios y promoci&oacute;n de eventos en los espacios del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Supervisar y controlar la programaci&oacute;n y ejecuci&oacute;n del presupuesto de gastos de la Gerencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, a fin de garantzar su cumplimiento con total eficacia, eficiencia y transparencia acorde a la normatva legal vigente y en artculaci&oacute;n con la Oficina de Gerencia de Planificaci&oacute;n y Presupuesto de VENTEL, C.A.</li>\r\n</ol>', 'Nivel de Apoyo', 0),
(10, 'Coordinación Gestión Humana', 'estructura/gestionhumana.png', '<strong>Coordinador:&nbsp;Richard Goffin</strong><br />\r\n<ol>\r\n<li>Programar y supervisar el proceso de captaci&oacute;n de talento humano, siguiendo los lineamientos emanados de la Gerencia de Talento Humano de VENTEL, C.A., a fin de proveer trabajadores y trabajadoras que satsfagan los requerimientos de las distntas unidades organizatvas que conforman la Gerencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Coordinar, programar y hacer seguimiento del dise&ntilde;o y ejecuci&oacute;n de los planes, programas y acciones de desarrollo del talento humano, considerando los objetvos estrat&eacute;gicos y las necesidades del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Supervisar, verificar y evaluar el proceso de egreso de personal de VENTEL, C.A., aplicando las normas y procedimientos que en materia de administraci&oacute;n de personal se&ntilde;ale el Decreto con Rango, Valor y Fuerza de Ley Org&aacute;nica del Trabajo, los Trabajadores y Trabajadoras y dem&aacute;s disposiciones legales aplicables vigentes.</li>\r\n<li>Coordinar y hacer seguimiento del otorgamiento, a los trabajadores y trabajadoras, de los beneficios sociales, econ&oacute;micos y laborales derivados de las Leyes, Reglamentos y Contrataci&oacute;n Colectva vigente aplicables al Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Coordinar, supervisar y hacer seguimiento de la aplicaci&oacute;n del sistema de evaluaci&oacute;n del desempe&ntilde;o a los trabajadores &nbsp;y trabajadoras dela Gerencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, de conformidad a los lineamientos de Gerencia de Talento Humano de VENTEL, C.A.</li>\r\n<li>Coordinar con la divisi&oacute;n de Gest&oacute;n Administratva, la ejecuci&oacute;n de los pagos del personal de conformidad con lo establecido en el Decreto con Rango, Valor y Fuerza de Ley Org&aacute;nica del Trabajo, los Trabajadores y Trabajadoras y dem&aacute;s disposiciones legales aplicables.</li>\r\n<li>Coordinar, supervisar y organizar el registro f&iacute;sico y digital de los expedientes administratvos del personal de VENTEL, C.A.</li>\r\n</ol>', 'Nivel de Apoyo', 0),
(11, 'Coordinación Seguridad Física e Industrial', 'estructura/seguridad.png', '<strong>Coordinador: &nbsp;Mayor Jorge Montilla</strong><br /><br />\r\n<ol>\r\n<li>Coordinar y supervisar la implementaci&oacute;n de los planes, proyectos, normas y procedimientos que contribuyan a la prevenci&oacute;n de accidentes y enfermedades ocupacionales, as&iacute; como a la disminuci&oacute;n de riesgos mec&aacute;nicos o f&iacute;sicos provenientes del medio ambiente laboral.</li>\r\n<li>Coordinar y supervisar la implementaci&oacute;n de planes, proyectos, normas y procedimientos que permitan la detecci&oacute;n y prevenci&oacute;n de riesgos que afecten la integridad f&iacute;sica de los turistas y visitantes en los espacios del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Coordinar y dirigir el desarrollo de programas que orienten la preparaci&oacute;n antcipada de acciones para evitar los acontecimientos de presunci&oacute;n delictva que puedan afectar al Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, garantzando la seguridad de los visitantes y turistas que acuden a los espacios del parque.</li>\r\n<li>Dirigir, coordinar y supervisar la implementaci&oacute;n de programas y proyectos dirigidos a resguardar la integridad f&iacute;sica de las personas e instalaciones, haciendo &eacute;nfasis en la vigilancia y custodia de la insttuci&oacute;n y en la protecci&oacute;n de las personas y los bienes patrimoniales de la empresa.</li>\r\n<li>Dirigir coordinar y supervisar la implementaci&oacute;n planes y programas de operaciones de emergencias, a fin de proporcionar atenci&oacute;n inmediata y adecuada a situaciones provocadas o fortuitas, en las instalaciones o destnos tur&iacute;stcos administrados por la empresa.</li>\r\n<li>Asesorar a las diferentes coordinaciones del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida en materia de seguridad de las instalaciones y de los bienes patrimoniales, a fin de garantzar niveles &oacute;ptmos de seguridad patrimonial y f&iacute;sica.</li>\r\n<li>Integrar dentro de la estrategia de la organizaci&oacute;n y a trav&eacute;s del desarrollo e implementaci&oacute;n del Plan de Gest&oacute;n Ambiental (PGA), la prevenci&oacute;n de impactos ambientales, la protecci&oacute;n, conservaci&oacute;n y respeto por el medio ambiente, partcularmente en el &aacute;rea de influencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n</ol>', 'Nivel de Apoyo', 0),
(12, 'Coordinación de Operaciones Electromecánicas', 'estructura/operavcines.png', '<strong>Coordinador:&nbsp;Mayor Mendez Alizo</strong><br /><br />\r\n<ol>\r\n<li>Programar, coordinar y verificar la correcta operaci&oacute;n de los sistemas el&eacute;ctricos, mec&aacute;nicos e hidr&aacute;ulicos que conforman la plataforma tecnol&oacute;gica y operatva del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, a fin de garantzar el desplazamiento seguro y confiable de los turistas y visitantes en los distntos espacios del parque.</li>\r\n<li>Coordinar y hacer seguimiento de las actvidades programadas dirigidas a la revisi&oacute;n e inspecci&oacute;n de instalaciones en materia de infraestructura mec&aacute;nica, el&eacute;ctrica, electr&oacute;nica e hidr&aacute;ulica, a fin de garantzar el dise&ntilde;o de planes, programas y proyectos de que contribuyan al funcionamiento &oacute;ptmo del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Coordinar, supervisar y evaluar la implementaci&oacute;n de planes, programas y acciones de mantenimiento preventvo o correctvo de la infraestructura mec&aacute;nica, el&eacute;ctrica, electr&oacute;nica e hidr&aacute;ulica, a fin de garantzar el funcionamiento &oacute;ptimo del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Planificar y controlar las actvidades relacionadas con las obras de ingenier&iacute;a destnadas al mejoramiento de la infraestructura e instalaciones del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Coordinar y supervisar la adquisici&oacute;n de equipos, componentes y materiales que garantcen la existencia de un inventario &oacute;ptimo para la ejecuci&oacute;n del mantenimiento de la infraestructura mec&aacute;nica, el&eacute;ctrica, electr&oacute;nica e hidr&aacute;ulica del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Monitorear y coordinar los trabajos de mantenimiento de pica y poda ejecutados por empresas contratstas en el Mukumbar&iacute;, a fin de garantzar el &oacute;ptmo funcionamiento del sistema telef&eacute;rico que opera en este parque.</li>\r\n<li>Supervisar y garantzar la operatvidad y buen funcionamiento de los diferentes sistemas y subsitemas ubicados en las estaciones del Mukumbar&iacute; STM, tales como: el&eacute;ctrico, detecci&oacute;n de incendio, contra incendio, aguas blancas, aguas negras y plantas de tratamiento, gases medicinales, aire acondicionado, ascensores, extractores, gas licuado de petroleo, entre otros.</li>\r\n</ol>', 'Nivel de Operativo', 0),
(13, 'Coordinación de Comercialización', 'estructura/comer.png', '<strong>Coordinador: &nbsp;Miguel Ranguel</strong><br />\r\n<ol>\r\n<li>Coordinar, dirigir y supervisar la implementaci&oacute;n de los lineamientos y estrategias del plan de comercializaci&oacute;n dise&ntilde;ado para la promoci&oacute;n y marketng de los productos, servicios y experiencias tur&iacute;stcas ofrecidas por VENTEL, C. A., a los turistas y visitantes del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Desarrollar y coordinar un sistema de precios y tarifas para los productos, servicios y experiencias tur&iacute;stcas del parque, a fin de garantzar el cumplimiento del plan de comercializaci&oacute;n del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Coordinar y supervisar la aplicaci&oacute;n de instrumentos, entrevistas y cuestonarios con el prop&oacute;sito de examinar la diferencias demogr&aacute;ficas, psicogr&aacute;ficas y de comportamiento de los visitantes y turistas, as&iacute; como su satsfacci&oacute;n con los atributos de calidad de servicios y productos tur&iacute;stcos ofrecidos en el Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Coordinar supervisar la implementaci&oacute;n de los planes, programas y proyectos dirigidos a la promoci&oacute;n de la imagen, las experiencias y servicios tur&iacute;stcos del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, con el prop&oacute;sito de incrementar la asistencia de los visitantes y turistas a los distntos destnos tur&iacute;stcos administrados por la empresa</li>\r\n<li>Programar, coordinar y supervisar investgaciones de mercado relacionados con la oferta tur&iacute;stca del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida que permitan la comprensi&oacute;n, antcipaci&oacute;n y toma de decisiones sobre la evoluci&oacute;n de los segmentos de inter&eacute;s tur&iacute;stcos en el &aacute;mbito regional, nacional e internacional.</li>\r\n<li>Dise&ntilde;ar, controlar y hacer seguimiento de la segmentaci&oacute;n del mercado social tur&iacute;stco, a fin de ofrecer servicios y experiencias tur&iacute;stcas dirigidas a satsfacer las necesidades de los grupos de inter&eacute;s que eligen los destnos tur&iacute;stcos vinculados al Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n</ol>', 'Nivel de Operativo', 0),
(14, 'Coordinación Servicios Turístico', 'estructura/servicios.png', '<strong>Coordinadora: &nbsp;Eranyelli Rondon <br /><br /></strong>\r\n<ol>\r\n<li>Coordinar y supervisar la organizaci&oacute;n y desarrollo de productos y servicios tur&iacute;stcos ofrecidos a empresas, visitantes, y turistas que eligen como destno los espacios del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Coordinar y supervisar la organizaci&oacute;n de eventos corporatvos y gastron&oacute;micos realizados en los espacios del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, de acuerdo a los par&aacute;metros de calidad de servicio y coexistencia con el medio ambiente prestablecidos por VENTEL, C.A.</li>\r\n<li>Coordinar y supervisar el funcionamiento de los espacios de ventas f&iacute;sicos y virtuales necesarios para el intercambio comercial entre las empresas, comerciantes , visitantes, y turistas que desarrollan actvidades en el Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n<li>Dirigir, coordinar y supervisar el plan de inspecciones a los operadores que prestan servicios tur&iacute;stcos en los espacios f&iacute;sicos administrados por VENTEL C. A. en el Parque Nacional Mukumbar&iacute;.</li>\r\n<li>Coordinar, supervisar y promocionar la atenci&oacute;n de turistas y visitantes nacionales e internacionales, de acuerdo a principios, est&aacute;ndares y atributos de excelencia en la calidad de servicio.</li>\r\n<li>Coordinar, supervisar e inspeccionar la operaci&oacute;n, comercializaci&oacute;n, arrendamiento y mantenimiento de los espacios f&iacute;sicos dirigidos a servicios gastron&oacute;micos y de recreaci&oacute;n ofertados en el Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>\r\n</ol>', 'Nivel de Operativo', 0);

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id_menu`, `nombre`, `alias`, `posicion`, `logueado`, `estado`, `padre`, `tipo`) VALUES
(1, 'Noticias', 'noticias', 3, 0, 1, 0, 'interno'),
(2, 'Articulo nuevo', 'nuevo_articulo', 4, 1, 1, 0, 'interno'),
(3, 'Eventos', 'eventos', 6, 0, 1, 0, 'interno'),
(4, 'Gestión de solicitudes', 'ges', 7, 1, 1, 0, 'interno'),
(5, 'Nuevo ticket', 'nuevo_ticket', 2, 1, 1, 4, 'interno'),
(6, 'Mis tickets', 'mis_tickets', 1, 1, 1, 4, 'interno'),
(7, 'Inicio', 'inicio', 1, 0, 1, 0, 'interno'),
(8, 'Nuevo evento', 'nuevo_evento', 2, 1, 1, 3, 'interno'),
(9, 'Calendario', 'eventos', 1, 1, 1, 3, 'interno'),
(10, 'Mukumbarí STM', 'mukumbari_stm', 2, 1, 1, 0, 'interno'),
(11, 'Estructura Organizacional', 'estructura_organizacional', 1, 1, 1, 10, 'interno'),
(12, 'Empleado del mes', 'empleado_del_mes', 5, 0, 1, 0, 'interno'),
(13, 'Sistema de Almacen', 'http://192.168.1.44', 8, 1, 1, 0, 'externo'),
(16, 'Telecomunicaciones', 'Telecomunicaciones', 9, 0, 1, 0, 'interno');

--
-- Volcado de datos para la tabla `redes_sociales`
--

INSERT INTO `redes_sociales` (`id_red`, `nombre`, `tipo`, `url`) VALUES
(1, 'Sistema Teleférico Mukumbarí', 'facebook', 'https://www.facebook.com/STMukumbari/'),
(2, 'Twitter Mukumbarí', 'twitter', 'https://twitter.com/Mukumbari'),
(3, 'Mukumbarí Sistema Teleférico', 'instagram', 'https://www.instagram.com/mukumbari/'),
(4, 'Mukumbarí Sistema Teleférico de Mérida', 'youtube', 'https://www.youtube.com/user/stmukumbari'),
(5, 'Mukumbarí', 'soundcloud', 'https://soundcloud.com/mukumbar');

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `asunto`, `cuerpo`, `estado`, `fecha_creacion`, `prioridad`, `id_usuario`) VALUES
('0v7Ztxab', 'Prueba 2', '<p>ioyujthdfghjnkm</p>', 'Nuevo', '2017-03-10 14:54:19', 2, 2),
('5uEmWqIJ', 'solicitud de un personal nuevo para informati', '<p>algo&nbsp;</p>', 'Nuevo', '2017-03-23 15:01:04', 2, 2),
('8HSbVPoB', 'Prueba de nuevo ticket', '<p>Nuevo ticket 3</p>\n<ul>\n<li>Item&nbsp;</li>\n<li>item&nbsp;</li>\n<li>item&nbsp;</li>\n</ul>\n<ol>\n<li>Item&nbsp;</li>\n<li>item&nbsp;\n<ol>\n<li>item&nbsp;</li>\n<li>item&nbsp;\n<ol>\n<li>item&nbsp;</li>\n</ol>\n</li>\n<li>item</li>\n</ol>\n</li>\n<li>otem</li>\n</ol>', 'Nuevo', '2017-02-26 19:15:00', 2, 2),
('Aa712nb1', 'Primer ticket', 'Ticket 1', 'Cerrado', '2017-02-25 13:38:00', 1, 2),
('AgG2xCHe', 'Asunto Solicitud de Usuario', '<p>Buenas tardes, me dirijo a usted con la finalidad de Solicitarle la apertura de usuarios en el servicio de INTRANET al siguiente personal de la Coordinacion de Comercializacion.</p>\r\n<p>&nbsp;</p>\r\n<p>Arelys Barboza CI: 18.796.039 TELF: (414) 974-02-55 E-mail: <a href="mailto:arelysjbg@gmail.com">arelysjbg@gmail.com</a></p>\r\n<p>David Perez CI: 12.350.032 TELF: (0416) 501-27-69 E-mail: <a href="mailto:jaziel25k@gmail.com">jaziel25k@gmail.com</a></p>\r\n<p>Jose Valero CI: 14.917.906 TELF:(0424) 728-33-86 E-mail: <a href="mailto:abilio75@gmail.com">abilio75@hotmail.com</a></p>\r\n<p>Lui Guilermo Garcia CI: 7.624.323 TELF: (0146) 708-25-63 (0416) 602-46-53 E-mail: <a href="mailto:gentedespierta.luisguillermo@gmail.com">gentedespierta.luisguillermo@gmail.com</a>&nbsp;</p>\r\n<p>Noryelith Quintana CI: 15.756.881 TELF: (0426) 829-88-30 E-mail: <a href="mailto:noryestm@gmail.com">noryestm@gmail.com</a>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Saludos Cordiales</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Miguel Rangel.</p>', 'Nuevo', '2017-05-05 15:14:26', 3, 11),
('AkmToyB9', 'Prueba', '<p>hola</p>', 'Nuevo', '2017-05-11 11:44:58', 3, 22),
('bNtBXDAp', 'SOLICITUD DE COMPRA DE CINTA MÉTRICA EXTENSIB', '<p>Buenas tardes, por medio de la presentre me dirijo a usted en la oportunidad de solicitarle la compra de una cinta m&eacute;trica extensible, ya que la mayoria de los trabajos se necesita de la misma para la Unidad de Publicidad y Dise&ntilde;o.&nbsp;</p>\r\n<p>Sin mas a que hacer referencia,&nbsp;</p>\r\n<p>Se despide de usted,&nbsp;</p>\r\n<p>Andrea Matheus.</p>', 'Nuevo', '2017-05-08 15:09:34', 3, 20),
('dAOBKvR1', 'Prueba de ticket seleccion', '<p>Prueba de ticket para saber que se estan seleccionando las coordinaciones gerencia y la del usuario actual&nbsp;</p>', 'Nuevo', '2017-03-10 15:14:47', 1, 2),
('Dp8oL0Ef', 'Solicitud de material', '<p>Buenas Tardes de acuerdo a insrucciones del gerente general se deben reemplazar todos los avisos de los ba&ntilde;os del sistma, para ello se hace necsario la adquisicion del siguiente material.</p>\r\n<p>&nbsp;</p>\r\n<p>50 hojas de Vinil autoadhesivo</p>\r\n<p>50 hojas para laminar&nbsp;</p>\r\n<p>Saludos coordiales&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Miguel Rangel</p>', 'Nuevo', '2017-05-04 14:45:11', 3, 11),
('K2nUXRwO', 'Solicitud de Usuarios', '<p>Buenas tardes sirva el presente para solicitar la apertura de usuarios en el servicio de intranet para el siguiente personal adscrito a la coordinacion de comercializacion y mercadeo.</p>\r\n<p>Arelys Barboza CI:18.796.039 TELF: (0414) 974-02-55 E-mail: <a href="mailto:arelysjbg@gmail.com">arelysjbg@gmail.com</a></p>\r\n<p>David Perez CI: 12.350.032 TELF: (0416) 501-27-69 E-mail: <a href="mailto:jaziel25k@gmail.com">jaziel25k@gmail.com</a></p>\r\n<p>Jose Valero CI: 14.917.906 TELF: (0424) 728-33-86 E-mail: <a href="mailto:abilio75@hotmail.com">abilio75@hotmail.com</a></p>\r\n<p>Luis Guillermo Garcia CI: 7.624.323 TELF: (0416) 708-25-63 (0416) 602-46-53 E-mail: <a href="mailto:gentedespierta.luisguillermo@gmail.com">gentedespierta.luisguillermo@gmail.com</a></p>\r\n<p>Noryelith Quintana CI: 15.756.881 TELF: (0416) 829-88-30 E-mail: <a href="mailto:noryestm@gmail.com">noryestm@gmail.com</a></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Saludos Cordiales &nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Miguel Rangel</p>', 'Nuevo', '2017-05-05 14:52:56', 3, 11),
('lY92jHSm', 'Saluditos', '<p>Se prendio esta Mierd@.</p>\r\n<p>&nbsp;</p>\r\n<p>Rosario</p>', 'Nuevo', '2017-05-10 10:15:37', 1, 16),
('nfwRJSgd', 'Prueba 4', '<p>Prueba 4</p>', 'Nuevo', '2017-02-26 19:16:27', 3, 2),
('nTOyVFxJ', 'pruebas con un tickey con imagen', '<p>producto que hay que comporarprueba con una imagen&nbsp;<img class="img-responsive" src="/intranet-m/assets/uploads/technology.png" alt="" width="284" height="284" /></p>', 'Nuevo', '2017-03-24 09:24:11', 2, 2),
('R3phuPLi', 'Un ticket hecho para probar que si se selecci', '<p>fgwthbae</p>', 'Nuevo', '2017-03-15 08:00:04', 2, 2),
('t1OiYwRX', 'Prueba', '<p>Prueba</p>', 'Nuevo', '2017-02-27 11:25:31', 2, 3),
('TrkMoPDA', 'DEVOLUCIÓN MADERA DEPARTAMENTO TALLER.', '<p>Para las fechas de Semana santa, fue prestado por el departamento taller una madera, para realizar los bailes de distraccion para el turismo en la estacion Monta&ntilde;a.</p>\r\n<p>Hasta la fecha no se ah devuelto a este departamento, por tal motivo espero respuesta de este material.</p>\r\n<p>Sin mas a que hacer referencia se despide</p>\r\n<p style="text-align: center;"><strong>Ing: Henry Rondon.</strong></p>\r\n<p style="text-align: center;"><strong>Supervisor del Departamento Taller.</strong></p>', 'Nuevo', '2017-05-10 10:11:02', 1, 22),
('Ttp2S1Po', 'PRUEBA / FALLA', '<p>Los mensajes no pueden ser le&iacute;dos una vez emitidos.</p>', 'Nuevo', '2017-05-11 12:10:10', 3, 16),
('urH9zXDo', 'Prueba de ticker', '<p>sdfsdf</p>', 'Nuevo', '2017-03-10 14:51:18', 2, 2),
('VwDFxNrz', 'Solicitud de compra de exacto', '<p>Se solicita la compra de lo Siguiente:&nbsp;</p>\r\n<p>-Un exacto marca Olfa de hoja ancha.</p>\r\n<p>&nbsp;dicho material se viene solicitando desde hace cuatro meses.</p>', 'Nuevo', '2017-05-11 10:10:59', 3, 11),
('wer431Ma', 'Segundo ticket', 'Ticket 2', 'Nuevo', '2017-02-26 13:38:00', 3, 2),
('wocyYFCs', 'Prueba d eun nuevo ticket', '<p>fgvsfgbrsg</p>', 'Nuevo', '2017-03-10 14:52:54', 1, 2),
('Xk4KH1Qi', 'ventel', '<p><a title="ventel" href="http://www.fitven.gob.ve/images/ventel.png">http://www.fitven.gob.ve/images/ventel.png</a></p>', 'Nuevo', '2017-05-11 14:32:07', 3, 32),
('ZY4EUP5C', 'Solicitud de compra de Tela', '<p>Buenas Tardes siguiendo las instrucciones de la gerencia se solicita la adquicicion de una pancanarta realizada con las siguientes especificaciones&nbsp;</p>\r\n<p>Largo :5 Metros</p>\r\n<p>Alto. 1.5 mteros&nbsp;</p>\r\n<p>Tipo de tela: Forro de Chaqueta</p>\r\n<p>Color Blanco</p>\r\n<p>Debe Llevar impreso el logo Mukumbari segun el manual de imagen&nbsp;</p>\r\n<p>Esperando su colaboracion&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Atentamente</p>\r\n<p>Miguel Rangel</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', 'Nuevo', '2017-04-23 15:21:51', 1, 11);

--
-- Volcado de datos para la tabla `ticket_departamento`
--

INSERT INTO `ticket_departamento` (`id_departamento`, `id_ticket`, `fecha`, `estado`) VALUES
(1, '0v7Ztxab', '2017-03-10 14:54:19', 'Sin leer'),
(1, '5uEmWqIJ', '2017-03-23 15:01:04', 'Sin leer'),
(1, '8HSbVPoB', '2017-02-26 19:15:00', 'Sin leer'),
(1, 'Aa712nb1', '2017-02-26 00:00:00', 'Sin leer'),
(1, 'AgG2xCHe', '2017-05-05 15:14:26', 'Sin leer'),
(1, 'bNtBXDAp', '2017-05-08 15:09:34', 'Sin leer'),
(1, 'dAOBKvR1', '2017-03-10 15:14:47', 'Sin leer'),
(1, 'Dp8oL0Ef', '2017-05-04 14:45:11', 'Sin leer'),
(1, 'nfwRJSgd', '2017-02-26 19:16:27', 'Sin leer'),
(1, 'R3phuPLi', '2017-03-15 08:00:04', 'Sin leer'),
(1, 't1OiYwRX', '2017-02-27 11:25:31', 'Sin leer'),
(1, 'urH9zXDo', '2017-03-10 14:51:18', 'Sin leer'),
(1, 'VwDFxNrz', '2017-05-11 10:10:59', 'Sin leer'),
(1, 'wocyYFCs', '2017-03-10 14:52:54', 'Sin leer'),
(1, 'ZY4EUP5C', '2017-04-23 15:21:51', 'Sin leer'),
(2, '8HSbVPoB', '2017-02-26 19:15:00', 'Sin leer'),
(2, 'Aa712nb1', '2017-02-26 00:00:00', 'Sin leer'),
(2, 'dAOBKvR1', '2017-03-10 15:14:47', 'Sin leer'),
(2, 'R3phuPLi', '2017-03-15 08:00:04', 'Sin leer'),
(2, 'wocyYFCs', '2017-03-10 14:52:54', 'Sin leer'),
(3, '5uEmWqIJ', '2017-03-23 15:01:04', 'Sin leer'),
(3, 'AgG2xCHe', '2017-05-05 15:14:26', 'Sin leer'),
(3, 'K2nUXRwO', '2017-05-05 14:52:56', 'Sin leer'),
(3, 'nTOyVFxJ', '2017-03-24 09:24:11', 'Sin leer'),
(3, 'Ttp2S1Po', '2017-05-11 12:10:10', 'Sin leer'),
(3, 'wocyYFCs', '2017-03-10 14:52:54', 'Sin leer'),
(9, 'bNtBXDAp', '2017-05-08 15:09:34', 'Sin leer'),
(9, 'Dp8oL0Ef', '2017-05-04 14:45:11', 'Sin leer'),
(9, 'VwDFxNrz', '2017-05-11 10:10:59', 'Sin leer'),
(9, 'ZY4EUP5C', '2017-04-23 15:21:51', 'Sin leer'),
(10, '5uEmWqIJ', '2017-03-23 15:01:04', 'Sin leer'),
(11, 'ZY4EUP5C', '2017-04-23 15:21:51', 'Sin leer'),
(12, 'AkmToyB9', '2017-05-11 11:44:58', 'Sin leer'),
(12, 'lY92jHSm', '2017-05-10 10:15:37', 'Sin leer'),
(12, 'TrkMoPDA', '2017-05-10 10:11:02', 'Sin leer'),
(12, 'Ttp2S1Po', '2017-05-11 12:10:10', 'Sin leer'),
(12, 'Xk4KH1Qi', '2017-05-11 14:32:07', 'Sin leer'),
(13, 'AgG2xCHe', '2017-05-05 15:14:26', 'Sin leer'),
(13, 'bNtBXDAp', '2017-05-08 15:09:34', 'Sin leer'),
(13, 'Dp8oL0Ef', '2017-05-04 14:45:11', 'Sin leer'),
(13, 'VwDFxNrz', '2017-05-11 10:10:59', 'Sin leer'),
(13, 'ZY4EUP5C', '2017-04-23 15:21:51', 'Sin leer'),
(14, 'TrkMoPDA', '2017-05-10 10:11:02', 'Sin leer');

--
-- Volcado de datos para la tabla `ticket_historial`
--

INSERT INTO `ticket_historial` (`id_historial`, `fecha`, `accion`, `id_ticket`, `id_usuario`) VALUES
(2, '2017-02-26 09:29:27', '', '8HSbVPoB', 2),
(3, '2017-02-26 22:42:27', 'Responder', '8HSbVPoB', 2),
(4, '2017-02-26 22:46:20', 'Responder', 'nfwRJSgd', 2),
(5, '2017-02-27 08:46:24', 'Responder', '8HSbVPoB', 1),
(6, '2017-02-27 08:46:40', 'Responder', '8HSbVPoB', 1),
(7, '2017-02-27 14:19:28', 'Cerrar', 'Aa712nb1', 2),
(8, '2017-02-27 14:22:37', 'Cerrar', 'Aa712nb1', 2),
(9, '2017-02-27 14:22:46', 'Reabrir', 'Aa712nb1', 2),
(10, '2017-02-27 14:23:23', 'Cerrar', 'Aa712nb1', 2),
(11, '2017-02-27 14:23:52', 'Cerrar', 'Aa712nb1', 2),
(12, '2017-02-27 14:24:40', 'Reabrir', 'Aa712nb1', 2),
(13, '2017-02-27 14:24:46', 'Cerrar', 'Aa712nb1', 2),
(14, '2017-02-27 14:28:41', 'Reabrir', 'Aa712nb1', 2),
(15, '2017-02-27 14:28:47', 'Cerrar', 'Aa712nb1', 2),
(16, '2017-03-23 15:02:22', 'Responder', '5uEmWqIJ', 2),
(17, '2017-04-05 10:25:45', 'Responder', 'nTOyVFxJ', 2),
(18, '2017-05-05 15:25:33', 'Responder', 'AgG2xCHe', 2),
(19, '2017-05-06 15:11:12', 'Responder', 'AgG2xCHe', 2),
(20, '2017-05-08 10:42:13', 'Responder', 'Dp8oL0Ef', 14),
(21, '2017-05-11 11:04:54', 'Responder', 'VwDFxNrz', 14);

--
-- Volcado de datos para la tabla `ticket_prioridad`
--

INSERT INTO `ticket_prioridad` (`id_prioridad`, `nombre`, `color`, `predeterminado`, `estado`) VALUES
(1, 'Media', '#ff7a00', 1, 1),
(2, 'Baja', '#3c4fff', 0, 1),
(3, 'Alta', '#ff0000', 0, 1);

--
-- Volcado de datos para la tabla `ticket_respuesta`
--

INSERT INTO `ticket_respuesta` (`id_respuesta`, `mensaje`, `fecha`, `id_ticket`, `id_usuario`) VALUES
(1, 'aasda', '2017-02-25 00:00:00', '8HSbVPoB', 2),
(2, '<p>Publicando respuesta desde <strong>frontend</strong></p>', '2017-02-26 22:31:27', '8HSbVPoB', 2),
(3, '<p>r</p>', '2017-02-26 22:42:14', '8HSbVPoB', 2),
(4, '<p>r</p>', '2017-02-26 22:42:27', '8HSbVPoB', 2),
(5, '<p>r</p>', '2017-02-26 22:46:20', 'nfwRJSgd', 2),
(6, '<p>Respuesta de vielmaj</p>', '2017-02-27 08:46:24', '8HSbVPoB', 1),
(7, '<p>res</p>', '2017-02-27 08:46:40', '8HSbVPoB', 1),
(8, '<p>ma&ntilde;ana se estara dando respueta</p>', '2017-03-23 15:02:22', '5uEmWqIJ', 2),
(9, '<p>no ntengo la computadora</p>', '2017-04-05 10:25:45', 'nTOyVFxJ', 2),
(10, '<p>ok el dia de ma&ntilde;ana se le estara enviado el usuario y las claves</p>', '2017-05-05 15:25:33', 'AgG2xCHe', 2),
(11, '<p>Noryelith Quintana: usuario Quintanan clave 123456</p>\r\n<p>Jose Valero: usuario&nbsp;Valeroj clave 123789</p>\r\n<p>Arelys Barboza: usuario BarbozaA clave 456789</p>\r\n<p>David Perez: usuario&nbsp;Perezd clave 123456</p>\r\n<p>Lui Guilermo Garcia: usuario&nbsp;Garcial clave 456123</p>', '2017-05-06 15:11:12', 'AgG2xCHe', 2),
(12, '<p>BUENOS DIAS SE SOLICITARA COTIZAR Y SE COMUNICARA</p>', '2017-05-08 10:42:13', 'Dp8oL0Ef', 14),
(15, '<p>BUEN DIA SE CONVERSARA CON EL CORONEL PARA LA COMPRA DE DIHA SOLICITUD</p>', '2017-05-11 11:04:54', 'VwDFxNrz', 14);

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre`, `usuario`, `clave`, `correo`, `telefono`, `foto`, `permisos`, `config`) VALUES
(1, 'Administrador', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', '0410-1234567', 'perfil.jpg', 1, 3),
(2, 'Joeinny Osorio', 'osorioj', 'c4b12d06e212e925f2ecadd642bf7809', 'joeinnyosorio@gmail.com', '041412432124', 'osorioj/joeinnyosorio.jpg', 0, 3),
(3, 'Syra La Cruz', 'slacruz', 'e10adc3949ba59abbe56e057f20f883e', 'correo@corroe.com', '3124325235', '', 0, 0),
(4, 'Marcel Aguilar', 'marcel', '672c6c78ee767372da7054a39532d85d', '', '', '', 0, 0),
(5, 'Richard Goffi', 'RGoffi', 'e10adc3949ba59abbe56e057f20f883e', 'correo@correo.com', '13124234', 'perfil.jpg', 0, 0),
(7, 'Eranyelli', 'Erondon', 'b09b1fee4a8c21d5d41a4be050a3323e', 'eranyelli.olney2501@gmail.com', '04264057648', 'perfil.jpg', 0, 0),
(8, 'Jenifer Rincon', 'JRincon', '636fca93dae5c57a1cacf49c091be6d2', 'jenfer57@hotmail.com', '0416-6146435', 'perfil.jpg', 0, 2),
(9, 'Osman Rodriguez', 'ORodriguez', '3a72180019c973ad5df41f300a09e6f3', 'osmanjafett@hotmail.com', '1234560214', 'perfil.jpg', 0, 0),
(10, 'Jorge Montilla', 'JMontilla', 'b4d519fc7212bdeadcf76034582cf688', 'jorgene2411@gmail.com', '4265745576', 'perfil.jpg', 0, 0),
(11, 'Miguel Ranguel', 'MRanguel', 'a63683648efc14f3a4e2c5718ac6b4b4', 'stmcomercializacion@gmail.com', '04166146436', 'perfil.jpg', 0, 2),
(12, 'Leida Tamara', 'LTamara', '906db43931bf14ec9dba5d3c47d7bbef', 'leidatsalcedo@gmail.com', '04147218809', 'perfil.jpg', 0, 2),
(13, 'Maria Rojas', 'MRojas', 'cab7f5f167a84d4694bb8f66890b403f', 'betaniarojasg@gtmail.com', '04142423625', 'perfil.jpg', 0, 0),
(14, 'Wilmer Rangel', 'WRangel', 'db959dadd78d2872855b5fe034eb76f8', 'wilmerrangel@hotmail.com', '04147200663', 'perfil.jpg', 0, 2),
(15, 'Mendez Alizo', 'RMendez', 'ad3043c65a7f7bbc341120b6cf255071', 'mendezalizor@gmail.com', '0416-6712530', 'perfil.jpg', 0, 0),
(16, 'Gabriela Ayala', 'GAyala', '1e354ce0e710c7f88f1e20d8ef592686', 'aleirbag.ayala@gmail.com', '04263721160', '16649478_10155036890662311_6176835903924399070_n.jpg', 0, 3),
(17, 'Luis Rafael Moreno Machado', 'LMoreno', 'baa6cc901a5345188ed271d180066ba8', 'luisrafaelmormac@gmail.com', '04127560069', 'perfil.jpg', 0, 0),
(18, 'Madelein lantieri', 'Mlantieri', 'a33531674fbc348e3a489ab34931e151', 'gciamukumbari@gmail.com', '04166182497', 'perfil.jpg', 0, 0),
(19, 'Gisela Silva', 'GSilva', 'b84fbaf3be45a9974d3e3c499b05fbd8', 'Gisela@gmail.com', '04166131596', 'perfil.jpg', 0, 2),
(20, 'Andrea Eugenia Matheus Kisis', 'MatehusA', 'e10adc3949ba59abbe56e057f20f883e', 'stmcomercializacion@gmail.com', '04264288066', 'perfil.jpg', 0, 2),
(21, 'Yonater Manzano', 'ymanzano', 'e10adc3949ba59abbe56e057f20f883e', 'Yonaterstm@gmail.com', '02125342125', 'electromecanica.png', 0, 3),
(22, 'Henry Rondon', 'Rondonh', 'e10adc3949ba59abbe56e057f20f883e', 'dptotallerstm@gmail.com', '04140367798', 'pico_espejo.jpg', 0, 3),
(23, 'Esterio Mendez', 'Mendeze', 'd4b3a93821b9d5562311a662df2ae06d', 'esterio.n32@gmail.com', '04163274183', 'perfil.jpg', 0, 2),
(24, 'Wilmer Uzcategui', 'Uzcateguiw', '55587a910882016321201e6ebbc9f595', 'Uzcateguiwil@gmail.com', '04265721593', 'perfil.jpg', 0, 0),
(25, 'Joannery Nuñez', 'Nuñezj', 'e2ff1f739106b27892559d3907165866', 'joita0108@gmail.com', '04268264849', 'perfil.jpg', 0, 0),
(26, 'Arelys Barboza', 'BarbozaA', 'e35cf7b66449df565f93c607d5a81d09', 'arelysjbg@gmail.com', '04149740255', 'perfil.jpg', 0, 0),
(27, 'David Perez', 'Perezd', 'e10adc3949ba59abbe56e057f20f883e', 'jaziel25k@gmail.com', '04165012769', 'perfil.jpg', 0, 0),
(28, 'Jose Valero', 'Valeroj', 'c2e9732267a8f68fffd33f371926b737', 'abilio75@hotmail.com', '04247283386', 'perfil.jpg', 0, 0),
(29, 'Luis Guilermo Garcia', 'Garcial', 'd964173dc44da83eeafa3aebbee9a1a0', 'gentedespierta.luisguillermo@gmail.com', '04166024653', 'perfil.jpg', 0, 0),
(30, 'Noryelith Quintana', 'Quintanan', 'e10adc3949ba59abbe56e057f20f883e', 'noryestm@gmail.com', '04268298830', 'perfil.jpg', 0, 0),
(31, 'Rocio Oriana Reaño Contreras', 'Reañor', 'f0760eb6c6c675597b38dd376fce2f73', 'rociotelevavico@hotmail.com', '04147126328', 'perfil.jpg', 0, 0),
(32, 'Henri Hernandez', 'Hernandezh', 'e10adc3949ba59abbe56e057f20f883e', 'operativostm@gmail.com', '04169708788', 'perfil.jpg', 0, 2),
(33, 'Julio Arias', 'Jarias', '47f1721fb409bac7c46db7c9af9c0df5', 'juliocesar.ariasmarquez@gmail.com', '04166750576', 'antena_azul.png', 0, 3),
(34, 'Betania Rojas', 'Rojasb', 'f270f6496750bdcf71c4a9e7df5538bc', 'rojasmariab@hotmail.com', '04142423625', 'perfil.jpg', 0, 0);

--
-- Volcado de datos para la tabla `usuario_departamento`
--

INSERT INTO `usuario_departamento` (`id_usuario`, `id_departamento`, `estado`) VALUES
(2, 2, 0),
(2, 3, 1),
(3, 1, 0),
(3, 2, 0),
(3, 3, 1),
(4, 2, 0),
(4, 3, 1),
(5, 3, 0),
(5, 10, 1),
(7, 4, 0),
(7, 14, 1),
(8, 4, 1),
(9, 9, 1),
(10, 11, 1),
(11, 13, 1),
(12, 13, 1),
(13, 13, 1),
(14, 9, 1),
(15, 12, 1),
(16, 12, 1),
(17, 1, 1),
(18, 1, 1),
(19, 2, 1),
(20, 13, 1),
(21, 12, 1),
(22, 12, 1),
(23, 12, 1),
(24, 9, 1),
(25, 4, 1),
(26, 13, 1),
(27, 13, 1),
(28, 13, 1),
(29, 13, 1),
(30, 13, 1),
(31, 9, 1),
(32, 12, 1),
(33, 3, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
