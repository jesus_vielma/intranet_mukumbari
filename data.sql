INSERT INTO `categoria` (`id_categoria`, `nombre`, `alias`, `default`) VALUES
(1, 'Noticas', 'noticias', 1),
(2, 'Empleado del mes', 'empleado_del_mes', 0),
(3, 'Estructura Organizacional', 'estructura_organizacional', 0);

INSERT INTO `usuario` (`id_usuario`, `nombre`, `usuario`, `clave`, `correo`, `telefono`, `foto`, `permisos`, `config`) VALUES
(1, 'Administrador', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', '0410-1234567', 'perfil.jpg', 1, 3),
(2, 'Joeinny Osorio', 'osorioj', 'c4b12d06e212e925f2ecadd642bf7809', 'joeinnyosorio@gmail.com', '041412432124', 'osorioj/joeinnyosorio.jpg', 0, 3),
(3, 'Syra La Cruz', 'syra', 'e11cadd3597c9f5b338f9c5158fdc65d', 'correo@corroe.com', '3124325235', '', 0, 0),
(4, 'Marcel Aguilar', 'marcel', '672c6c78ee767372da7054a39532d85d', '', '', '', 0, 0),
(5, 'Richard Goffi', 'richardg', '98301c344dfe17cfaa28bde5acd897bc', 'correo@correo.com', '13124234', 'perfil.jpg', 0, 0);


INSERT INTO `articulo` (`id_articulo`, `titulo`, `fecha`, `cuerpo`, `alias`, `id_categoria`, `id_usuario`) VALUES
(1, 'Estructura Organizacional', '2017-02-28 00:00:00', '<h1 style="text-align: center;">Origanigrama general</h1>
<p><em><strong>Haga clic sobre el el cuadro de la coordinaci&oacute;n que desea ver</strong></em></p>
<p><img class="img-responsive" src="/intranet-m/assets/uploads/estructura/Organigrama-Mukumbari.png" alt="Estructura organizativa" usemap="#imgmap201731516335" width="803" height="503" border="0" /><map id="imgmap201731516335" name="imgmap201731516335"> 
<area alt="Gerencia Mukumbar&iacute; STM" coords="351,4,495,74" shape="rect" target="" />
 
<area alt="Oficina de Atenci&oacute;n al Ciudadano" coords="438,112,580,180" shape="rect" target="" />
 
<area alt="Coordinaci&oacute;n Gesti&oacute;n Administrativa" coords="264,212,406,280" shape="rect" target="" />
 
<area alt="Coordinaci&oacute;n Gesti&oacute;n Humana" coords="440,212,578,282" shape="rect" target="" />
 
<area alt="Coordinaci&oacute;n de Seguridad F&iacute;sica e Industrial " coords="266,308,406,374" shape="rect" target="" />
 
<area alt="Coordinaci&oacute;n Gesti&oacute;n Tecnol&oacute;gica" coords="440,308,578,376" shape="rect" target="" />
 
<area alt="Coordinaci&oacute;n de Operaciones Electromec&aacute;nicas" coords="102,406,298,482" shape="rect" target="" />
 
<area alt="Coordinaci&oacute;n Servicios Turistico" coords="332,403,510,479" shape="rect" target="" />
 
<area alt="Coordinaci&oacute;n de Comercializaci&oacute;n" coords="566,413,739,479" shape="rect" target="" />
 
<area alt="Consultoria Juridica" coords="271,118,413,187" shape="rect" target="" />
</map></p>
<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1"><br />
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header"><button class="close" type="button" data-dismiss="modal">&times;</button>
<h4 id="titulo" class="modal-title"></h4></div>
<div class="modal-body"><img id="imagen" class="img-responsive" border="0" />
<div id="cuerpo"></div>
<div class="modal-footer"><button id="quitarModal" class="btn btn-default" type="button" data-dismiss="modal">Salir</button></div>
</div>
</div>
</div>
</div>', 'estructura_organizacional', 3, 1);

INSERT INTO `departamento` (`id_departamento`, `nombre`, `organigrama`, `descripcion`, `nivel`, `predeterminado`) VALUES
(1, 'Gerencia Mukumbarí STM', 'estructura/Gerencia.png', '<ol>
<li style="text-align: justify;">Planificar, evaluar y hacer seguimiento de la ejecuci&oacute;n de revisiones e inspecciones con el prop&oacute;sito determinar el estado general de las instalaciones en materia de infraestructura mec&aacute;nica, el&eacute;ctrica, electr&oacute;nica e hidr&aacute;ulica, a fin de implementar programas&nbsp;de mantenimiento preventvo o correctvo para el funcionamiento &oacute;ptmo del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>
<li style="text-align: justify;">Planificar, verificar y controlar la correcta aplicaci&oacute;n del sistema de precios y tarifas establecidos para los productos, servicios y&nbsp;experiencias tur&iacute;stcas ofrecidas por VENTEL, C.A., a fin de garantzar el ingreso de recursos financieros que coadyuven a la&nbsp;sustentabilidad de la empresa.</li>
<li style="text-align: justify;">Dirigir, promover y verificar la implementaci&oacute;n de los lineamientos, estrategias y metas del plan de comercializaci&oacute;n dise&ntilde;ado&nbsp;para la promoci&oacute;n y marketng de los productos, servicios y experiencias tur&iacute;stcas ofrecidas por VENTEL, C. A., a los turistas y&nbsp;visitantes del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>
<li style="text-align: justify;">Administrar y controlar la gest&oacute;n de los recursos financieros obtenidos, en coordinaci&oacute;n con la Gerencia de Desarrollo Tur&iacute;stco&nbsp;de VENTEL, C.A., de la comercializaci&oacute;n de las experiencias tur&iacute;stcas, boleter&iacute;a, eventos culturales y gastron&oacute;micos realizados en&nbsp;los espacios del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>
<li style="text-align: justify;">Planificar, controlar y hacer seguimiento del plan de inspecciones a los operadores que prestan servicios tur&iacute;stcos en los espacios f&iacute;sicos administrados por VENTEL, C. A., en el Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>
<li style="text-align: justify;">Administrar y controlar el funcionamiento de los espacios de ventas f&iacute;sicos y virtuales necesarios para el intercambio comercial entre las empresas, comerciantes, visitantes, y turistas que desarrollan actvidades en el Mukumbar&iacute; Sistema Telef&eacute;rico de&nbsp;M&eacute;rida.&nbsp;</li>
<li style="text-align: justify;">Desarrollar proyectos y programas que optmicen el proceso de convenir la adquisici&oacute;n de las experiencias tur&iacute;stcas y el disfrute&nbsp;de los destnos tur&iacute;stcos promocionados por VENTEL, C.A., con el prop&oacute;sito de incrementar progresivamente la afluencia de&nbsp;visitantes y turistas al Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.1. Planificar, controlar y hacer seguimiento de los procesos relacionados con la gest&oacute;n del talento humano en el Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, siguiendo los lineamientos emanados de la Gerencia de Gest&oacute;n Humana de VENTEL, C.A.,y&nbsp;lo establecido en la normatva legal vigente en materia de las relaciones sociales del trabajo.&nbsp;</li>
<li style="text-align: justify;">Evaluar, controlar y hacer seguimiento de la implementaci&oacute;n de los planes, proyectos, normas y procedimientos que&nbsp;contribuyan a la prevenci&oacute;n de accidentes y enfermedades ocupacionales, as&iacute; como a la disminuci&oacute;n de riesgos mec&aacute;nicos&nbsp;o f&iacute;sicos provenientes del medio ambiente laboral.&nbsp;</li>
<li style="text-align: justify;">Evaluar, controlar y hacer seguimiento de la implementaci&oacute;n de planes, proyectos, normas y procedimientos que permitan la detecci&oacute;n y prevenci&oacute;n de riesgos que afecten la integridad f&iacute;sica de los turistas y visitantes en los espacios&nbsp;f&iacute;sicos del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>
<li style="text-align: justify;">Planificar, gestonar y controlar la operaci&oacute;n de los sistemas el&eacute;ctricos, mec&aacute;nicos, de redes, voz y datos que conforman la&nbsp;plataforma tecnol&oacute;gica y operatva del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, a fin de garantzar el desplazamiento&nbsp;seguro y confiable de los turistas y visitantes en los distntos espacios del parque.</li>
</ol>', 'Estratégico', 1),
(2, 'Consultoria Juridica', 'estructura/juridica1.png', '<ol>
<li style="text-align: justify;">Velar por el cumplimiento de la Consttuci&oacute;n, Leyes, Decretos, Reglamentos, Instructvos y otras normatvas legales, relacionadas con las actvidades de la Organizaci&oacute;n y en general con las Leyes de la Rep&uacute;blica Bolivariana de Venezuela.</li>
<li style="text-align: justify;">Asesorar jur&iacute;dicamente a la Gerencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, a solicitud de &eacute;sta, de las diferentes Coordinaciones y&nbsp;dem&aacute;s dependencias, sobre asuntos de car&aacute;cter legal y jur&iacute;dico&nbsp;</li>
<li style="text-align: justify;">Asistr por instrucciones de la Consultor&iacute;a Jur&iacute;dica de VENTEL, C.A. a las reuniones en las cuales se requiera la presencia de la Consultor&iacute;a&nbsp;Jur&iacute;dica, con el fin de unificar los criterios tanto del Ejecutvo Nacional como del Ministro o Ministra, la directva de VENTEL C.A., en cuanto a&nbsp;las leyes en proceso de promulgaci&oacute;n, as&iacute; como de cualquier otro instrumento legal.&nbsp;</li>
<li style="text-align: justify;">Realizar oportunamente, las gestones ante otros organismos p&uacute;blicos, relacionadas con actos administratvos dictados por este ente, as&iacute;&nbsp;como aquellos que en virtud de su contenido, sean inherentes a esta Organizaci&oacute;n.&nbsp;</li>
<li style="text-align: justify;">Representar en todos los asuntos judiciales o extrajudiciales que conciernen a VENTEL, C.A., dentro del Estado Bolivariano de M&eacute;rida.</li>
<li style="text-align: justify;">Elaborar por solicitud de la Gerencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida propuestas de convenios, contratos y cualquier otro&nbsp;documento de car&aacute;cter legal.</li>
</ol>', 'Estretegico', 0),
(3, 'Coordinación Gestión Tecnológica', 'estructura/tecnologica.png', '<ol>
<li style="text-align: justify;">Coordinar, supervisar y hacer seguimiento de la detecci&oacute;n de necesidades y verificaci&oacute;n de requerimientos tecnol&oacute;gicos de las unidades organizatvas que conforman el Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida, a fin de realizar un diagnostco de las&nbsp;demandas de hardware y softwares de la Gerencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>
<li style="text-align: justify;">Supervisar y verificar el funcionamiento adecuado de las redes inform&aacute;tcas y de comunicaciones que ha desarrollado VENTEL,&nbsp;C.A. en los espacios f&iacute;sicos y virtuales, a fin de garantzar la operaci&oacute;n eficiente del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>
<li style="text-align: justify;">Coordinar y supervisar la implementaci&oacute;n de pol&iacute;tcas que garantcen la seguridad e integridad de la informaci&oacute;n que se&nbsp;procesa y almacena en los servidores y estaciones de trabajo de la Gerencia del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>
<li style="text-align: justify;">Programar , coordinar, y supervisar la actualizaci&oacute;n y mantenimiento de los sistemas inform&aacute;tcos, hardwares y softwares que soportan los procesos administratvos y operatvos de las unidades organizatvas que conforman la Gerencia del Mukumbar&iacute;&nbsp;Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>
<li style="text-align: justify;">Dirigir y coordinar el dise&ntilde;o y desarrollo de proyectos para la adopci&oacute;n de soluciones tecnol&oacute;gicas que permitan la optmizaci&oacute;n&nbsp;de los procesos de gest&oacute;n, comercializaci&oacute;n y prestaci&oacute;n de servicios tur&iacute;stcos del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida.</li>
</ol>', 'Apoyo', 0),
(4, 'Oficina de Atención al Ciudadano', 'estructura/atencion-ciudadano.png', '<ol>
<li style="text-align: justify;">Promover la partcipaci&oacute;n ciudadana en las diversas &aacute;reas de inter&eacute;s del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida: social, tur&iacute;stca, cultural,&nbsp;educatva, deportva, ambiental, entre otras.&nbsp;</li>
<li style="text-align: justify;">Apoyar, orientar, recibir y tramitar, las denuncias, quejas, reclamos, sugerencias y petciones formuladas por los ciudadanos al Mukumbar&iacute;&nbsp;Sistema Telef&eacute;rico de M&eacute;rida.&nbsp;</li>
<li style="text-align: justify;">Fomentar la partcipaci&oacute;n de la poblaci&oacute;n con la finalidad de desarrollar iniciatvas que contribuyan en la ejecuci&oacute;n de los programas de turismo como actvidad comunitaria social.&nbsp;</li>
<li style="text-align: justify;">Informar a la ciudadan&iacute;a sobre la utlizaci&oacute;n de los recursos que integran el patrimonio p&uacute;blico del Mukumbar&iacute; Sistema Telef&eacute;rico de M&eacute;rida,&nbsp;as&iacute; como tambi&eacute;n la difusi&oacute;n de valores socialistas a trav&eacute;s de la formaci&oacute;n y la sensibilizaci&oacute;n de los (as) trabajadores (as) de la empresa y la&nbsp;comunidad organizada.</li>
</ol>', 'Estratégico', 0);

INSERT INTO `evento` (`id_evento`, `fecha_inicio`, `fecha_fin`, `titulo`, `descripcion`, `estado`, `tipo`, `id_usuario`) VALUES
(1, '2017-03-13 00:00:00', '2017-03-12 23:59:59', 'Cumpleaños Joeinny', 'svdfergbra', 'Pendiente', 'cumpleanos', 1),
(2, '2017-03-13 12:30:00', '2017-03-13 18:00:00', 'Venta de cosas', 'Cosas', 'Aprobado', 'evento', 1),
(3, '2017-03-12 16:55:00', '2017-03-12 17:55:00', 'Este es un evento creado desde el frontend', '<p>HOLA</p>', '', 'evento', 2),
(4, '2017-03-12 15:48:00', '2017-03-12 15:48:00', 'otro evento del calendario hfgyughj', '<p>kliuytfrdfgh</p>', '', 'cumpleanos', 2),
(5, '2017-03-15 13:45:00', '2017-03-15 23:55:00', 'Cumpleaños de Joenny Osorio aaaa', '<p>f</p>', '', 'cumpleanos', 2);

INSERT INTO `menu` (`id_menu`, `nombre`, `alias`, `posicion`, `logueado`, `estado`, `padre`) VALUES
(1, 'Noticias', 'noticias', 3, 0, 1, 0),
(2, 'Articulo nuevo', 'nuevo_articulo', 7, 1, 1, 0),
(3, 'Eventos', 'eventos', 5, 0, 1, 0),
(4, 'Gestión de solicitudes', 'ges', 6, 1, 1, 0),
(5, 'Nuevo ticket', 'nuevo_ticket', 2, 1, 1, 4),
(6, 'Mis tickets', 'mis_tickets', 1, 1, 1, 4),
(7, 'Inicio', 'inicio', 1, 0, 1, 0),
(8, 'Nuevo evento', 'nuevo_evento', 2, 1, 1, 3),
(9, 'Calendario', 'eventos', 1, 1, 1, 3),
(10, 'Mukumbarí STM', 'mukumbari_stm', 2, 1, 1, 0),
(11, 'Estructura Organizacional', 'estructura_organizacional', 1, 1, 1, 10),
(12, 'Empleado del mes', 'empleado_del_mes', 4, 0, 1, 0);

INSERT INTO `redes_sociales` (`id_red`, `nombre`, `tipo`, `url`) VALUES
(1, 'Sistema Teleférico Mukumbarí', 'facebook', 'https://www.facebook.com/STMukumbari/'),
(2, 'Twitter Mukumbarí', 'twitter', 'https://twitter.com/Mukumbari'),
(3, 'Mukumbarí Sistema Teleférico', 'instagram', 'https://www.instagram.com/mukumbari/'),
(4, 'Mukumbarí Sistema Teleférico de Mérida', 'youtube', 'https://www.youtube.com/user/stmukumbari'),
(5, 'Mukumbarí', 'soundcloud', 'https://soundcloud.com/mukumbar');

INSERT INTO `ticket_prioridad` (`id_prioridad`, `nombre`, `color`, `predeterminado`, `estado`) VALUES
(1, 'Media', '#ff7a00', 1, 1),
(2, 'Baja', '#3c4fff', 0, 1),
(3, 'Alta', '#ff0000', 0, 1);

INSERT INTO `ticket` (`id_ticket`, `asunto`, `cuerpo`, `estado`, `fecha_creacion`, `prioridad`, `id_usuario`) VALUES
('0v7Ztxab', 'Prueba 2', '<p>ioyujthdfghjnkm</p>', 'Nuevo', '2017-03-10 14:54:19', 2, 2),
('8HSbVPoB', 'Prueba de nuevo ticket', '<p>Nuevo ticket 3</p>
<ul>
<li>Item&nbsp;</li>
<li>item&nbsp;</li>
<li>item&nbsp;</li>
</ul>
<ol>
<li>Item&nbsp;</li>
<li>item&nbsp;
<ol>
<li>item&nbsp;</li>
<li>item&nbsp;
<ol>
<li>item&nbsp;</li>
</ol>
</li>
<li>item</li>
</ol>
</li>
<li>otem</li>
</ol>', 'Nuevo', '2017-02-26 19:15:00', 2, 2),
('Aa712nb1', 'Primer ticket', 'Ticket 1', 'Cerrado', '2017-02-25 13:38:00', 1, 2),
('dAOBKvR1', 'Prueba de ticket seleccion', '<p>Prueba de ticket para saber que se estan seleccionando las coordinaciones gerencia y la del usuario actual&nbsp;</p>', 'Nuevo', '2017-03-10 15:14:47', 1, 2),
('nfwRJSgd', 'Prueba 4', '<p>Prueba 4</p>', 'Nuevo', '2017-02-26 19:16:27', 3, 2),
('R3phuPLi', 'Un ticket hecho para probar que si se selecci', '<p>fgwthbae</p>', 'Nuevo', '2017-03-15 08:00:04', 2, 2),
('t1OiYwRX', 'Prueba', '<p>Prueba</p>', 'Nuevo', '2017-02-27 11:25:31', 2, 3),
('urH9zXDo', 'Prueba de ticker', '<p>sdfsdf</p>', 'Nuevo', '2017-03-10 14:51:18', 2, 2),
('wer431Ma', 'Segundo ticket', 'Ticket 2', 'Nuevo', '2017-02-26 13:38:00', 3, 2),
('wocyYFCs', 'Prueba d eun nuevo ticket', '<p>fgvsfgbrsg</p>', 'Nuevo', '2017-03-10 14:52:54', 1, 2);

INSERT INTO `ticket_departamento` (`id_departamento`, `id_ticket`, `fecha`, `estado`) VALUES
(1, '0v7Ztxab', '2017-03-10 14:54:19', 'Sin leer'),
(1, '8HSbVPoB', '2017-02-26 19:15:00', 'Sin leer'),
(1, 'Aa712nb1', '2017-02-26 00:00:00', 'Sin leer'),
(1, 'dAOBKvR1', '2017-03-10 15:14:47', 'Sin leer'),
(1, 'nfwRJSgd', '2017-02-26 19:16:27', 'Sin leer'),
(1, 'R3phuPLi', '2017-03-15 08:00:04', 'Sin leer'),
(1, 't1OiYwRX', '2017-02-27 11:25:31', 'Sin leer'),
(1, 'urH9zXDo', '2017-03-10 14:51:18', 'Sin leer'),
(1, 'wocyYFCs', '2017-03-10 14:52:54', 'Sin leer'),
(2, '8HSbVPoB', '2017-02-26 19:15:00', 'Sin leer'),
(2, 'Aa712nb1', '2017-02-26 00:00:00', 'Sin leer'),
(2, 'dAOBKvR1', '2017-03-10 15:14:47', 'Sin leer'),
(2, 'R3phuPLi', '2017-03-15 08:00:04', 'Sin leer'),
(2, 'wocyYFCs', '2017-03-10 14:52:54', 'Sin leer'),
(3, 'wocyYFCs', '2017-03-10 14:52:54', 'Sin leer');

INSERT INTO `ticket_historial` (`id_historial`, `fecha`, `accion`, `id_ticket`, `id_usuario`) VALUES
(2, '2017-02-26 09:29:27', 'Agregar coordinación involucrada', '8HSbVPoB', 2),
(3, '2017-02-26 22:42:27', 'Responder', '8HSbVPoB', 2),
(4, '2017-02-26 22:46:20', 'Responder', 'nfwRJSgd', 2),
(5, '2017-02-27 08:46:24', 'Responder', '8HSbVPoB', 1),
(6, '2017-02-27 08:46:40', 'Responder', '8HSbVPoB', 1),
(7, '2017-02-27 14:19:28', 'Cerrar', 'Aa712nb1', 2),
(8, '2017-02-27 14:22:37', 'Cerrar', 'Aa712nb1', 2),
(9, '2017-02-27 14:22:46', 'Reabrir', 'Aa712nb1', 2),
(10, '2017-02-27 14:23:23', 'Cerrar', 'Aa712nb1', 2),
(11, '2017-02-27 14:23:52', 'Cerrar', 'Aa712nb1', 2),
(12, '2017-02-27 14:24:40', 'Reabrir', 'Aa712nb1', 2),
(13, '2017-02-27 14:24:46', 'Cerrar', 'Aa712nb1', 2),
(14, '2017-02-27 14:28:41', 'Reabrir', 'Aa712nb1', 2),
(15, '2017-02-27 14:28:47', 'Cerrar', 'Aa712nb1', 2);


INSERT INTO `ticket_respuesta` (`id_respuesta`, `mensaje`, `fecha`, `id_ticket`, `id_usuario`) VALUES
(1, 'aasda', '2017-02-25 00:00:00', '8HSbVPoB', 2),
(2, '<p>Publicando respuesta desde <strong>frontend</strong></p>', '2017-02-26 22:31:27', '8HSbVPoB', 2),
(3, '<p>r</p>', '2017-02-26 22:42:14', '8HSbVPoB', 2),
(4, '<p>r</p>', '2017-02-26 22:42:27', '8HSbVPoB', 2),
(5, '<p>r</p>', '2017-02-26 22:46:20', 'nfwRJSgd', 2),
(6, '<p>Respuesta de vielmaj</p>', '2017-02-27 08:46:24', '8HSbVPoB', 1),
(7, '<p>res</p>', '2017-02-27 08:46:40', '8HSbVPoB', 1);

INSERT INTO `usuario_departamento` (`id_usuario`, `id_departamento`, `estado`) VALUES
(2, 2, 0),
(2, 3, 1),
(3, 1, 1),
(3, 2, 0),
(4, 2, 1),
(5, 3, 0);